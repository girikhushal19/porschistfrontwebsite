import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {
  token:any;email:any;user_type:any;loggedInDetail:any;user_id:any;
  //adminUserLoginCheck = "http://localhost:3001/api/adminUserLoginCheck";

    base_url = "https://www.porschists.com/";
    base_url_node_admin = "https://www.admin.porschists.com/api/";
    base_url_node = "https://www.admin.porschists.com/";
  
    // base_url = "http://localhost:4200/";
    // base_url_node_admin = "http://localhost:3001/api/";
    // base_url_node = "http://localhost:3001/";

  constructor(private _http:HttpClient) { 
        
      }
  userLogin()
  {
    this.token = localStorage.getItem("token");
    /*console.log("service here");
    console.log("service here");
    console.log("this.token"+this.token);*/
    /*if(this.token === "")
    {
      window.location.href = this.base_url+"admin";
    }*/
    return this.token;
  }    
  
  userLoggedInType()
  {
    
    this.user_id = localStorage.getItem("_id");
    // console.log("service here");
    // console.log("this.user_type"+this.user_type);
    // if(this.user_type !== "admin")
    // {
    //   window.location.href = this.base_url+"admin";
    // }
    return this.user_id;
    
  }
}
