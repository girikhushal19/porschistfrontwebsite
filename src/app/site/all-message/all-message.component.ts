import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators, UntypedFormBuilder } from '@angular/forms';
import { LoginauthenticationService } from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { UserchatService } from '../../siteservice/userchat.service';

@Component({
  selector: 'app-all-message',
  templateUrl: './all-message.component.html',
  styleUrls: ['./all-message.component.css']
})
export class AllMessageComponent implements OnInit {
  base_url = ""; base_url_node = ""; base_url_node_only: any; webUserLoginSubmit: any;
  formData: any; formValue: any; apiResponse: any; token: any; user_id: any; user_type: any;
  queryParam: any; webGetUserProfile: any; firstName: any; lastName: any; user_email: any; user_mobileNumber: any; userImage: any; allSingleChatRecord: any;
  gender: any; table_id: any; date_of_birth: any; webGetAllAdSingleChat: any;
  webAllAdChatGroup: any; tableIdError: any;
  address: string = ''; resultAd: any;
  userLatitude: string = '';
  userLongitude: string = '';
  allChatRecord: any;
  myFiles: string[] = [];

  newMessage = '';
  messageList: string[] = [];
  testmsg: any


  defChatid: any



  constructor(private userChatService: UserchatService, private _http: HttpClient, private formBuilder: UntypedFormBuilder, private loginAuthObj: LoginauthenticationService) {
    this.testmsg = []
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node + "webGetFullUserProfile";

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    if (this.token === "" || this.user_id === "" || this.user_id === null || this.token === null) {
      window.location.href = this.base_url;
    }

    this.webAllAdChatGroup = this.base_url_node + "webAllAdChatGroup";
    this.webGetAllAdSingleChat = this.base_url_node + "webGetAllAdSingleChat";
    this.defChatid = ''
    this.allChatRecord = []
  }

  ngOnInit(): void {
    

    this.myFiles = [];

    this.queryParam = { "user_id": this.user_id };
    this._http.post(this.webGetUserProfile, this.queryParam).subscribe((response: any) => {
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if (this.apiResponse.error == false) {
        //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
        this.firstName = this.apiResponse.userRecord.firstName;
        this.lastName = this.apiResponse.userRecord.lastName;
        this.user_email = this.apiResponse.userRecord.email;
        this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
        /*this.address = this.apiResponse.userRecord.address;
        this.gender = this.apiResponse.userRecord.gender;
        this.date_of_birth = this.apiResponse.userRecord.date_of_birth;
        this.userLatitude = this.apiResponse.userRecord.latitude;
        this.userLongitude = this.apiResponse.userRecord.longitude;*/
        //gender //address //latitude //longitude date_of_birth
        if (this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null) {
          this.userImage = this.base_url_node_only + "public/uploads/userProfile/" + this.apiResponse.userRecord.userImage;
        } else {
          this.userImage = "assets/images/logo/user.png";
        }

      }
    });


    this.queryParam = { "user_id": this.user_id };
    this._http.post(this.webAllAdChatGroup, this.queryParam).subscribe((response: any) => {
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if (this.apiResponse.error == false) {
        this.allChatRecord = this.apiResponse.record;
        this.defChatid = this.allChatRecord[0]._id
      //  console.log(this.allChatRecord[0]._id, "chat");
        this. defChatop();


        
      }
    });


    this.userChatService.getNewMessage().subscribe((response: any) => {
      // console.log("component chat response response");
      // console.log(response);
      // console.log(response.error);
      // console.log(response.record);
      if (response.error == false) {
        this.newMessage = '';
        this.allSingleChatRecord = response.record;
      }

      //this.messageList.push(message);
    })







    


  }




  // sendMessage() {
  //   this.userChatService.sendMessage(this.newMessage);
  //   this.newMessage = '';
  // }



  form = new UntypedFormGroup({
    sender_id: new UntypedFormControl('', []),
    table_id: new UntypedFormControl('', []),
    newMessage: new UntypedFormControl('', []),
  });

  get f() {
    return this.form.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  submit() {
    //console.log("here");
    if (this.form.valid) {
      this.formValue = this.form.value;
      //console.log(this.formValue);
      if (this.form.value.table_id == "" || this.form.value.table_id == null || this.form.value.table_id == undefined) {
        //console.log("table id blank");
        this.tableIdError = "Veuillez sélectionner un chat avant d'envoyer le message";
      } if (this.form.value.newMessage == "" || this.form.value.newMessage == null || this.form.value.newMessage == undefined) {
        this.tableIdError = "Veuillez saisir tout message";
      } else {
        this.userChatService.sendMessage(this.formValue);
        //this.newMessage = '';
      }
    } else {
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form);
      // validate all form fields
    }
  }

  openChatModule(chat_id = null) {
   
    //console.log("chat_id "+chat_id);
    this.queryParam = { "chat_id": chat_id,user_id:this.user_id };
    this._http.post(this.webGetAllAdSingleChat, this.queryParam).subscribe((response: any) => {
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if (this.apiResponse.error == false) {
        this.allSingleChatRecord = this.apiResponse.record;
        this.resultAd = this.apiResponse.resultAd;
        //console.log(this.resultAd);
        if (this.allSingleChatRecord) {
          if (this.allSingleChatRecord.length > 0) {
            this.table_id = this.allSingleChatRecord[0]._id;
            let q_param = {"chat_id": chat_id,"user_id":this.user_id};
            this._http.post(this.base_url_node+"updateChatReadStatus",q_param).subscribe((res:any)=>{
              
            })

          }
        }
      }
    });
  }

  defChatop(){

    this.queryParam = { "chat_id": this.defChatid };
   
    this._http.post(this.webGetAllAdSingleChat, this.queryParam).subscribe((response: any) => {
      this.apiResponse = response;
     // console.log(  this.apiResponse , "guess");
      if (this.apiResponse.error == false) {
        this.allSingleChatRecord = this.apiResponse.record;
        this.resultAd = this.apiResponse.resultAd;
        //console.log(this.resultAd);
        if (this.allSingleChatRecord) {
          if (this.allSingleChatRecord.length > 0) {
            this.table_id = this.allSingleChatRecord[0]._id;
            console.log("this.defChatid ", this.defChatid);
            //updateChatReadStatus
            let q_param = {"chat_id": this.defChatid,"user_id":this.user_id};
            this._http.post(this.base_url_node+"updateChatReadStatus",q_param).subscribe((res:any)=>{

            })
          }
        }
      }
    });

  }
}
