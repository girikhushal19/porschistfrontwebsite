import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailProVerificationComponent } from './email-pro-verification.component';

describe('EmailProVerificationComponent', () => {
  let component: EmailProVerificationComponent;
  let fixture: ComponentFixture<EmailProVerificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmailProVerificationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmailProVerificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
