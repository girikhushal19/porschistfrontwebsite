import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder, FormGroup} from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router'

@Component({
  selector: 'app-rating-ad',
  templateUrl: './rating-ad.component.html',
  styleUrls: ['./rating-ad.component.css']
})
export class RatingAdComponent implements OnInit {
  form!:FormGroup
  base_url_node:any;ad_id:any; base_url_node_only:any
  webGetAccessoriesPurchaseSingle:any; webGetAccessoriesPurchaseSingleRes:any; Sdata:any
  webSingleProductCar:any; webSingleProductCarRes:any; image:any
  adID:any; user_id:any; sellerId:any
  myrating:any
  webAddRatingReviews:any; webAddRatingReviewsRes:any; order_id:any
  constructor(private loginAuthObj:LoginauthenticationService,private _http:HttpClient,private router: Router,private actRoute: ActivatedRoute) { 
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.order_id =  this.actRoute.snapshot.params['id'];
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.webGetAccessoriesPurchaseSingle = this.base_url_node+"webGetAccessoriesPurchaseSingle";
    this.webSingleProductCar = this.base_url_node+"webSingleProductCar";
    this.webAddRatingReviews = this.base_url_node+"webAddRatingReviews";
    this.myrating = 0
    
  }

  ngOnInit(): void {
    this.form = new UntypedFormGroup({
      review: new UntypedFormControl('', Validators.required),
    });
    
    const params = {
      "id":this.order_id
    }
    this._http.post(this.webGetAccessoriesPurchaseSingle,params).subscribe(res=>{
      this.webGetAccessoriesPurchaseSingleRes = res
      if(this.webGetAccessoriesPurchaseSingleRes.success){
        this.Sdata = this.webGetAccessoriesPurchaseSingleRes.record[0]
        this.sellerId = this.Sdata.ad_owner_user_id[0]
        this.ad_id = this.Sdata.ad_id[0]
        console.log( this.Sdata)
        
        
      }
    })

    // const imgparams = {
    //   "ad_id":this.adID,
    //   "user_id":this.userId 
    // }
    // console.log( imgparams)
    // this._http.post(this.webSingleProductCar ,imgparams).subscribe(res=>{
    //   this.webSingleProductCarRes  = res
    //   if(this.webSingleProductCarRes.error == false){
    //     // this.image = this.webSingleProductCarRes.record[0].accessoires_image[0].path

    //     // console.log(this.image, "this.image")
    //   }
    // })




  }

  setRating(event:any){

   


    this.myrating =parseInt(event.target.value)
    console.log(event.target.value)
  }
  submit(){
    this.form.value.order_id = this.order_id
    this.form.value.ad_id = this.ad_id
    this.form.value.user_id = this.user_id
    this.form.value.seller_id = this.sellerId
    this.form.value.rating = this.myrating
    console.log( this.form.value)
    
    if(this.form.valid ){
     this._http.post(this.webAddRatingReviews, this.form.value).subscribe(res=>{
      this.webAddRatingReviewsRes = res
      if(this.webAddRatingReviewsRes.success){
        window.location.href='/accessoriesPurchaseList'
      }
     })


    }else{
      
      for (const control of Object.keys(this.form.controls)) {
        this.form.controls[control].markAsTouched();
      }
      return;
    }
  }

}
