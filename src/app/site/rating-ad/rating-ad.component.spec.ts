import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingAdComponent } from './rating-ad.component';

describe('RatingAdComponent', () => {
  let component: RatingAdComponent;
  let fixture: ComponentFixture<RatingAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RatingAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RatingAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
