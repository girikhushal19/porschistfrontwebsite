import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-my-cart',
  templateUrl: './my-cart.component.html',
  styleUrls: ['./my-cart.component.css']
})
export class MyCartComponent implements OnInit {
   base_url:any;base_url_node:any;base_url_node_only:any;user_id:any;
   cartCountURL:any; cartData:any;queryParam:any;
   webRemoveCartItem:any;deleteres:any; record:any; recordLength:Number=0;
   updateCartQtyUrl:any; cartQtyres:any;globalChekArr: any;
  keyValueShipping:any;myCheckoutCheck:any; apiResponseAddress:any;
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.user_id = this.loginAuthObj.userLoggedInId();

    this.cartCountURL = this.base_url_node+"webGetUserCart";
    this.webRemoveCartItem = this.base_url_node+"webRemoveCartItem";
    this.updateCartQtyUrl = this.base_url_node+"updateCartQuantity";

    this.myCheckoutCheck = this.base_url_node+"checkoutShippingCheck";

    this.globalChekArr = [];
    this.keyValueShipping = {};
   }

  ngOnInit(): void {
    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.cartCountURL,this.queryParam).subscribe(res=>{
      this.cartData = res;
      this.record = this.cartData.record;
      this.recordLength = this.record.length;
      console.log("record -------------->>>>>> ", this.record )
    })
  }
  deleteCartItem(cart_id:any){
    console.log(cart_id)
    this.queryParam = {cart_id:cart_id};
    this._http.post(this.webRemoveCartItem,this.queryParam).subscribe((response:any)=>{
      this.deleteres= response
      if(this.deleteres.error == false)
      window.location.reload()
    })
  }


  getShippingCalculation(product_id:any,price:any)
  {
    console.log("product_id ", product_id);
    console.log("price ", price);
    this.keyValueShipping[product_id] = price;
    
    console.log("this.keyValueShipping  ", this.keyValueShipping);
  }
  updateQty(eve:any , id:any){
    console.log(eve.target.value)
    let qytParams= {
      "cart_id":id,
      "quantity":eve.target.value
    }
    console.log(qytParams)
    this._http.post(this.updateCartQtyUrl,qytParams).subscribe(res=>{
      this.cartQtyres =  res
      console.log(res)

      if(this.cartQtyres.error == false){
        window.location.reload()
      }
    })

  }
  myCheckoutPage()
  {
    console.log("checkout ");
    //this.myCheckoutCheck
    this.queryParam = {user_id:this.user_id,keyValueShipping:this.keyValueShipping};
    this._http.post(this.myCheckoutCheck,this.queryParam).subscribe((response:any)=>{
      this.apiResponseAddress = response;
      if(this.apiResponseAddress.error == false)
      {
        setTimeout(() => {
          window.location.href='/my_checkout';
        }, 2000);
      }
      console.log("response " , response);

    })
  }

  // deliveryMethid(id: any, length: any, index: any) {
  //   console.log("hereeeeeeeeeeeeee 254")
  //   console.log(this.globalChekArr)
  //   const arr = []
  //   for (var i = 0; i < length; i++) {
  //     // arr.push("")
  //     this.globalChekArr.push("")

  //   }

  //   this.globalChekArr.splice(index, 1, id);


  // }
}
