import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdSuccessPageComponent } from './ad-success-page.component';

describe('AdSuccessPageComponent', () => {
  let component: AdSuccessPageComponent;
  let fixture: ComponentFixture<AdSuccessPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdSuccessPageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdSuccessPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
