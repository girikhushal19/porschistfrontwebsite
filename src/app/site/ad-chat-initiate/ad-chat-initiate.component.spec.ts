import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdChatInitiateComponent } from './ad-chat-initiate.component';

describe('AdChatInitiateComponent', () => {
  let component: AdChatInitiateComponent;
  let fixture: ComponentFixture<AdChatInitiateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdChatInitiateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdChatInitiateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
