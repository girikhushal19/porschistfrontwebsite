import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ad-chat-initiate',
  templateUrl: './ad-chat-initiate.component.html',
  styleUrls: ['./ad-chat-initiate.component.css']
})
export class AdChatInitiateComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;getWebChatProduct:any;allBanner:any;allCategory:any;
  getCategory:any;getWebHomeCarAd:any;allCarAd:any;allAccessAd:any;webInitiateChatSubmit:any; getWebHomeAccessAd:any;ad_id:any;allCarAdSimilar:any;
  btn_disable:Boolean = false; btn_spring:Boolean = false;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  {
     
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.getWebChatProduct = this.base_url_node+"getWebChatProduct";
    this.webInitiateChatSubmit = this.base_url_node+"webInitiateChatSubmit";

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    this.ad_id = this.actRoute.snapshot.params['id'];
  }

  ngOnInit(): void {
    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          //console.log("userImage "+this.apiResponse.userRecord.userImage);
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
        }
      });
    }

    let queryParam = {ad_id:this.ad_id,user_id:this.user_id}
    this._http.post(this.getWebChatProduct,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log("ad data ", this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCarAd = this.apiResponse.record;
        this.apiResponse = {};
      }
    });
  }

  form = new UntypedFormGroup({
    message: new UntypedFormControl('', [Validators.required]),
    ad_id: new UntypedFormControl('', []),
    sender_id: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  submit(){
    if (this.form.valid)
    {
      this.btn_disable = true;
      this.btn_spring = true;
      console.log("here");
      this.formValue = this.form.value;
      console.log(this.formValue);
      this._http.post(this.webInitiateChatSubmit,this.formValue).subscribe((response:any)=>{
        this.btn_disable = false;
        this.btn_spring = false;
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          this.form.reset();
        }
      });

    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
