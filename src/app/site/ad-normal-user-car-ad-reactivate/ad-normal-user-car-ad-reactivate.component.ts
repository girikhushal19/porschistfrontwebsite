import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ad-normal-user-car-ad-reactivate',
  templateUrl: './ad-normal-user-car-ad-reactivate.component.html',
  styleUrls: ['./ad-normal-user-car-ad-reactivate.component.css']
})
export class AdNormalUserCarAdReactivateComponent implements OnInit {
  base_url = "";base_url_node = "";ad_id:any;base_url_node_only:any;user_id:any;
  token:any;user_type:any;apiResponse:any;apiResponseLast:any;formValue:any;record:any;webGetUserParticularPlanTopSearchList:any;
  allUserParticularPlanTopUrgent:any;
  allUserParticularPlan:any;
  webGetUserParticularPlanTopUrgent:any;
  allUserParticularPlanTopSearch:any;getCarAdForImagePrice:any;allAddPriceImage:any;
  first_plan_price:number;second_plan_price:number;third_plan_price:number;final_plan_price:number;tax_percent:number;queryParam:any; queryParamNew:any;

  errorValueSelcetion:string = "";
  webGetUserParticularPlan:any;
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService,private router: Router,private actRoute: ActivatedRoute){
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    
    this.webGetUserParticularPlanTopSearchList = this.base_url_node+"webGetUserParticularPlanTopSearchList";
    this.getCarAdForImagePrice = this.base_url_node+"getCarAdForImagePrice";
    this.webGetUserParticularPlanTopUrgent = this.base_url_node+"webGetUserParticularPlanTopUrgent";
    // console.log(this.token);
    // console.log(this.user_id);
    // console.log(this.user_type);

    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }

    

    this._http.get(this.base_url_node+"webGetUserParticularPlan",{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlan = this.apiResponse.record;
      }
    });


    this.first_plan_price = 0;this.second_plan_price = 0;this.third_plan_price = 0;
    this.final_plan_price = 0;this.tax_percent = 0;
    this.ad_id =  this.actRoute.snapshot.params['id'];
    console.log("this.ad_id "+this.ad_id);

    this.queryParam = {"ad_id":this.ad_id};
        this._http.post(this.getCarAdForImagePrice,this.queryParam).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            this.allAddPriceImage = this.apiResponse.record;
            console.log("here 674 "+ JSON.stringify(this.allAddPriceImage));
          }
           
        });

        this._http.get(this.base_url_node+"getWebTax",{}).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            this.tax_percent = parseFloat(this.apiResponse.result.attribute_value);
            //console.log("this.tax_percent "+this.tax_percent);
          }
        });

   }

  ngOnInit(): void {
  }


  formSevenStep = new UntypedFormGroup({
    paymentType: new UntypedFormControl('', []),
    ad_id: new UntypedFormControl('', []),
    normalUserAd: new UntypedFormControl('', [Validators.required]),
    //normalUserAdTopUrgent: new UntypedFormControl('', []),
    //normalUserTopSearch: new UntypedFormControl('', []),
  });
  get formSevenStepFun(){
    return this.formSevenStep.controls;
  }
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  onItemChange(event:any)
  {
     this.first_plan_price = 0;
     this.first_plan_price = event;

     this.final_plan_price = this.first_plan_price +this.second_plan_price+this.third_plan_price;
     if(this.allAddPriceImage[0].category == "Porsche Voitures")
     {
      let taxx = this.final_plan_price * this.tax_percent / 100;
      this.final_plan_price = taxx + this.final_plan_price;
     }
    //console.log(event);
  }

  onItemChangeSec(event:any)
  {
    this.second_plan_price = 0;
     this.second_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price+this.third_plan_price;
     if(this.allAddPriceImage[0].category == "Porsche Voitures")
     {
      let taxx = this.final_plan_price * this.tax_percent / 100;
      this.final_plan_price = taxx + this.final_plan_price;
     }
     
    //console.log(event);
  }

  onItemChangeThird(event:any)
  {
    this.third_plan_price = 0;
     this.third_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price+this.third_plan_price;
     if(this.allAddPriceImage[0].category == "Porsche Voitures")
     {
      let taxx = this.final_plan_price * this.tax_percent / 100;
      this.final_plan_price = taxx + this.final_plan_price;
     }
    //  let taxx = this.final_plan_price * this.tax_percent / 100;
    //  this.final_plan_price = taxx + this.final_plan_price;
    //console.log(event);
  }
  next(){
    console.log("inside next");
    if(this.formSevenStep.value.normalUserAd == "" || this.formSevenStep.value.normalUserAd == null || this.formSevenStep.value.normalUserAd == undefined)
    {
      this.errorValueSelcetion = "L'identifiant du plan est requis";
      return;
    }

    if(this.formSevenStep.value.paymentType == "" || this.formSevenStep.value.paymentType == null || this.formSevenStep.value.paymentType == undefined)
    {
      this.errorValueSelcetion = "Le type de paiement est requis";
      return;
    }
    this.errorValueSelcetion = "";
    //let redirect_url = this.base_url_node_only+"api/webCarAdPayment?"+"ad_id="+this.ad_id+"&user_id="+this.user_id+"&top_search_plan_id="+this.formSevenStep.value.normalUserTopSearch+"&paymentType="+this.formSevenStep.value.paymentType;

    this.queryParam = {
      "ad_id":this.ad_id,
      "user_id":this.user_id,
      "basic_main_plan_id":this.formSevenStep.value.normalUserAd,
      "paymentType":this.formSevenStep.value.paymentType,
      
    };
    this._http.post(this.base_url_node+"normalUserCarAdRenewal",this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponseLast = response;
      console.log(this.apiResponseLast);
      if(this.apiResponseLast.error == false)
      {
        
        setTimeout(() => {
          window.location.href = this.apiResponseLast.record;
        }, 2000);
      }
        
    });



  }

}
