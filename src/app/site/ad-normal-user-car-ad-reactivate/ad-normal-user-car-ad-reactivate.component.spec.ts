import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdNormalUserCarAdReactivateComponent } from './ad-normal-user-car-ad-reactivate.component';

describe('AdNormalUserCarAdReactivateComponent', () => {
  let component: AdNormalUserCarAdReactivateComponent;
  let fixture: ComponentFixture<AdNormalUserCarAdReactivateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdNormalUserCarAdReactivateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdNormalUserCarAdReactivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
