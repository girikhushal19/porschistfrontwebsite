import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';

@Component({
  selector: 'app-user-ad-professional',
  templateUrl: './user-ad-professional.component.html',
  styleUrls: ['./user-ad-professional.component.css']
})
export class UserAdProfessionalComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserOwnAd:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;getAllUserOwnAd:any;
  getAllCatURL:any;myCategories:any; userownFilter:any
  searchinput:any; categoryvalue:any; sortvalue:any
  sendcategoryURL:any;
  searchval:any
  activeAdCount:Number = 0;inActiveAdCount:Number = 0;
  date_1:any;date_2:any;
  webGetUserOwnAdExpired:any; webGetUserOwnAdExpiredres:any
  inActiveRecord:any

  webGetUserOwnAdActiveAdCount:any; webGetUserOwnAdInActiveAdCount :any
  webGetUserOwnAdActiveAdCountres:any; webGetUserOwnAdInActiveAdCountres :any

  activeNumber:any; inactiveNumber:any
  selectedIndex:any; selectedIndex2:any; allCategoryList:any;
  cat1:string=""; cat2:string="";
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserOwnAd = this.base_url_node+"webGetUserOwnAd";
    this.webGetUserOwnAdExpired =  this.base_url_node+"webGetUserOwnAdExpired";
    this.getAllCatURL = this.base_url_node+"getCategory";
    this.webGetUserOwnAdActiveAdCount = this.base_url_node+"webGetUserOwnAdActiveAdCount";
    this.webGetUserOwnAdInActiveAdCount = this.base_url_node+"webGetUserOwnAdInActiveAdCount";
    

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    this.searchval = '' 
    this.searchinput = ''
    this.categoryvalue = ''
    this.sortvalue = ''
    this.selectedIndex =0
    this.selectedIndex2 = 0
    //  console.log(this.token);
    //  console.log(this.user_id);
    //console.log("user_type --->>>  ", this.user_type);
    //normal_user
    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }

    this._http.get(this.base_url_node+"getCategoryOnAdPage",{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCategoryList = this.apiResponse.record;
        if(this.allCategoryList)
        {
          if(this.allCategoryList.length > 0)
          {
            this.cat1 = this.allCategoryList[0].first_heading;
          }
          if(this.allCategoryList.length > 1)
          {
            this.cat2 = this.allCategoryList[1].first_heading;
          }
        }
      }
    });
  }

  ngOnInit(): void {
    this.getAllCategories()
    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserOwnAd,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //
      if(this.apiResponse.error == false)
      {
        console.log("getAllUserOwnAd");
        console.log(this.apiResponse);
        this.getAllUserOwnAd = this.apiResponse.record;
      }
    });


    this._http.post(this.webGetUserOwnAdExpired,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.webGetUserOwnAdExpiredres = response;
      //
      if(this.webGetUserOwnAdExpiredres.error == false)
      {
        
        this.inActiveRecord = this.webGetUserOwnAdExpiredres.record;
      }
    });


    
    // this._http.post(this.base_url_node+"webGetUserOwnAdActiveAd",this.queryParam).subscribe((response:any)=>{
    //   //console.log("response of api"+response);
    //   this.apiResponse = response;
    //   //
    //   if(this.apiResponse.error == false)
    //   {
    //     //console.log(this.apiResponse);
    //     this.activeAdCount = this.apiResponse.record;
    //   }
    // });
    // this._http.post(this.base_url_node+"webGetUserOwnAdInActiveAd",this.queryParam).subscribe((response:any)=>{
    //   //console.log("response of api"+response);
    //   this.apiResponse = response;
    //   //
    //   if(this.apiResponse.error == false)
    //   {
    //     //console.log(this.apiResponse);
    //     this.inActiveAdCount = this.apiResponse.record;
    //   }
    // });

    this._http.post(this.webGetUserOwnAdActiveAdCount,this.queryParam).subscribe((response:any)=>{
      //console.log(response, "test");
      this.webGetUserOwnAdActiveAdCountres = response
      this.activeAdCount = this.webGetUserOwnAdActiveAdCountres.record;

      const myNumber = this.webGetUserOwnAdActiveAdCountres.record/10

      this.activeNumber = Array(Math.ceil(myNumber)).fill(0).map((m,n)=>n);
      
    });

    this._http.post(this.webGetUserOwnAdInActiveAdCount,this.queryParam).subscribe((response:any)=>{
    
      this.webGetUserOwnAdInActiveAdCountres = response
      this.inActiveAdCount = this.webGetUserOwnAdInActiveAdCountres.record;

      const myNumber = this.webGetUserOwnAdInActiveAdCountres.record/10

      this.inactiveNumber = Array(Math.ceil(myNumber)).fill(0).map((m,n)=>n);
      
    });



     

  }

  getAllCategories(){
    this._http.post(this.getAllCatURL,Options).subscribe(res=>{
      this.apiResponse = res
      this.myCategories = this.apiResponse.record
    })
  }
  getCatval(eve:any){
   
    this.categoryvalue = eve.target.value
  }
  selectSort(eve:any){
    this.sortvalue =  eve.target.value;
    this.filterRecords();
  }

  filterRecords(){

    this.searchval = this.searchinput.value
    
    this.userownFilter={
      "user_id":this.user_id,
      "search_var":this.searchval,
      "category":this.categoryvalue,
      "sort_var":this.sortvalue ,
      "pageNo":this.selectedIndex
    }

    

    this._http.post(this.webGetUserOwnAd,this.userownFilter).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //
      if(this.apiResponse.error == false)
      {
        //console.log(this.apiResponse);
        this.getAllUserOwnAd = this.apiResponse.record;
      }
    });

  }
  activeInactiveAd(id:any,activation_status:Number)
  {
    //console.log(id);
    console.log(activation_status);
    this.queryParam = {"ad_id":id,activation_status:activation_status};
    this._http.post(this.base_url_node+"updatePausePlayAd",this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //
      console.log("response of api"+this.apiResponse);
      if(this.apiResponse.error == false)
      {
        console.log(this.apiResponse);
        this.userownFilter={
          "user_id":this.user_id,
          "pageNo":0
        }
    
        console.log(this.userownFilter)
    
        this._http.post(this.webGetUserOwnAd,this.userownFilter).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          //
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse);
            this.getAllUserOwnAd = this.apiResponse.record;
          }
        });
      }
    });

  }
  deleteAd(id:any)
  {
    const askPermission =  confirm("Voulez-vous supprimer cette annonce ?")
    if(askPermission){
    //console.log(id);
    this.queryParam = {"ad_id":id};
    this._http.post(this.base_url_node+"deleteAdSubmit",this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //
      console.log("response of api"+this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.location.reload()
        console.log(this.apiResponse);
        this.userownFilter={
          "user_id":this.user_id,
          "pageNo":0
        }
    
        console.log(this.userownFilter)
    
        this._http.post(this.webGetUserOwnAd,this.userownFilter).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          //
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse);
            this.getAllUserOwnAd = this.apiResponse.record;
          }
        });
      }
    });
   }

  }

  activePages(item:any){
    console.log(item)
    this.selectedIndex = item
    this.queryParam = {
      "user_id": this.user_id,
      "pageNo": item
    };

    this._http.post(this.webGetUserOwnAd,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //
      if(this.apiResponse.error == false)
      {
        console.log("getAllUserOwnAd");
        console.log(this.apiResponse);
        this.getAllUserOwnAd = this.apiResponse.record;
      }
    });
  }
  inactivePages(item:any){
    this.selectedIndex2 = item
    this.queryParam = {
      "user_id": this.user_id,
      "pageNo": item
    };
    this._http.post(this.webGetUserOwnAdExpired,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.webGetUserOwnAdExpiredres = response;
      //
      if(this.webGetUserOwnAdExpiredres.error == false)
      {
        
        this.inActiveRecord = this.webGetUserOwnAdExpiredres.record;
      }
    });
  }
  activeCarAd(id:any)
  {
    console.log("heree");
    console.log(id);
    this.queryParam = {
      "ad_id": id,
      "user_id": this.user_id,
      "ad_type": "car"
    };
    this._http.post(this.base_url_node+"proUserReactivate",this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.alert(this.apiResponse.errorMessage);
        window.location.reload();
      }else{
        window.alert(this.apiResponse.errorMessage);
        //window.location.reload();
      }
    });

  }
  activeAccessAd(id:any)
  {
    console.log("heree");
    console.log(id);
    //accessories
    this.queryParam = {
      "ad_id": id,
      "user_id": this.user_id,
      "ad_type": "accessories"
    };
    this._http.post(this.base_url_node+"proUserReactivate",this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.alert(this.apiResponse.errorMessage);
         window.location.reload();
      }else{
        window.alert(this.apiResponse.errorMessage);
        //window.location.reload();
      }
    });
  }
  dayDiffFunction(dateee:any)
  {
    console.log("dateee");
    console.log(dateee);
    this.date_1 = new Date(dateee);
    console.log("date_1");
    console.log(this.date_1);
    this.date_2 = new Date();
    console.log("date_2");
    console.log(this.date_2);
    let diffTime = Math.abs(this.date_2 - this.date_1);
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
    //console.log(diffTime + " milliseconds");
    console.log(diffDays + " days");
    console.log(diffDays );
    return diffDays;
  }
  clickNormalAdReactivate(id:any)
  {
    console.log("ad id ", id);
    this.queryParam = {ad_id:id};
    this._http.post(this.base_url_node+"normalUserReactivate",this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.alert(this.apiResponse.errorMessage);
         window.location.reload();
      }else{
        window.alert(this.apiResponse.errorMessage);
        //window.location.reload();
      }
    });
  }
}
