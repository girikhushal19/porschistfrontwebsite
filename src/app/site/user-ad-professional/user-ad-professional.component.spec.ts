import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAdProfessionalComponent } from './user-ad-professional.component';

describe('UserAdProfessionalComponent', () => {
  let component: UserAdProfessionalComponent;
  let fixture: ComponentFixture<UserAdProfessionalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserAdProfessionalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserAdProfessionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
