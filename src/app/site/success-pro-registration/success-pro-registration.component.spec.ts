import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessProRegistrationComponent } from './success-pro-registration.component';

describe('SuccessProRegistrationComponent', () => {
  let component: SuccessProRegistrationComponent;
  let fixture: ComponentFixture<SuccessProRegistrationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuccessProRegistrationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SuccessProRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
