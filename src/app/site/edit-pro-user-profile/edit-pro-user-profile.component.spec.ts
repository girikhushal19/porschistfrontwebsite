import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProUserProfileComponent } from './edit-pro-user-profile.component';

describe('EditProUserProfileComponent', () => {
  let component: EditProUserProfileComponent;
  let fixture: ComponentFixture<EditProUserProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditProUserProfileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditProUserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
