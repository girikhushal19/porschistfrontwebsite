import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserPurchaseSuccessComponent } from './user-purchase-success.component';

describe('UserPurchaseSuccessComponent', () => {
  let component: UserPurchaseSuccessComponent;
  let fixture: ComponentFixture<UserPurchaseSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserPurchaseSuccessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserPurchaseSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
