import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {
  base_url_node:any
  aboutusURL:any
  apiRes:any
  constructor(private loginAuthObj:LoginauthenticationService,private _http:HttpClient) { 
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.aboutusURL = this.base_url_node+"getPrivacyPolicy";
  }

  ngOnInit(): void {
    this.getAbout()
  }
  getAbout(){
    this._http.get(this.aboutusURL).subscribe(res=>{
      this.apiRes= res
      console.log(this.apiRes)
    })
  }
}


