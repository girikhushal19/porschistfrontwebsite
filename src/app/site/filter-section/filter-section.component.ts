import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder,UntypedFormArray} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-filter-section',
  templateUrl: './filter-section.component.html',
  styleUrls: ['./filter-section.component.css']
})
export class FilterSectionComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;
  showDiv:boolean = false;
  showDivButton:boolean = true;
  tab1Div:boolean = true;
  tab2Div:boolean = false;
  getCategory:any;allCategoryList:any;allModel:any;allModelList:any;allAccessModelList:any;queryParam:any;
  getSubAttribute:any;getColor:any;allColor:any;registration_year:any;apiResponse:any;allChasisType:any;allYear:any;allCarburant:any;allEtatDuVehicle:any;allBoiteDeVitesse:any;
  formValue:any;allSubModelList:any;allTypeDePiece:any;
  cat1:any;cat2:any;categoryTypeForm:any;allWebSubModel:any;getWebProductList:any;
  carModelArray:number [] = [];yearArray:number [] = [];chahisArray:number [] = [];
  colorArray:number [] = [];yearArrayAccess:number [] = [];
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService) {

    

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    // this.token = this.loginAuthObj.userLogin();
    // this.user_id = this.loginAuthObj.userLoggedInId();
    // this.user_type = this.loginAuthObj.userLoggedInType();
    
    // console.log(this.token);
    // console.log(this.user_id);
    // console.log(this.user_type);

    this.getCategory = this.base_url_node+"getCategoryOnAdPage";
    this.allModel = this.base_url_node+"getModel";
    this.getSubAttribute = this.base_url_node+"getSubAttribute";
    this.registration_year = this.base_url_node+"registration_year";
    this.getColor = this.base_url_node+"getColor";
    this.allWebSubModel = this.base_url_node+"allWebSubModel";
    this.getWebProductList = this.base_url_node+"getWebProductList";
    this.categoryTypeForm = "Porsche Voitures";
   }

  ngOnInit(): void {

    this._http.get(this.getCategory,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCategoryList = this.apiResponse.record;
        this.cat1 = this.allCategoryList[0].category;
        this.cat2 = this.allCategoryList[1].category;
      }
      
    });
    this._http.get(this.registration_year,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allYear = this.apiResponse.record;
      }
      
    });

    this.queryParam = {attribute_type:"Porsche Voitures"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allModelList = this.apiResponse.record;
        //console.log(this.allModelList);
      }
    });

    this.queryParam = {attribute_type:"Pièces et accessoires"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allAccessModelList = this.apiResponse.record;
        //console.log(this.allModelList);
      }
    });


    //Type de châssis
    this.queryParam = { "parent_id":"62e3d257ec6f493144a2533a"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allChasisType = this.apiResponse.record;
      }
    });

    //Carburant
    this.queryParam = { "parent_id":"62e3d307ec6f493144a25377"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allCarburant = this.apiResponse.record;
        //console.log("allCarburant"+this.allCarburant);
      }
    });
    //Etat du véhicule
    this.queryParam = { "parent_id":"62e3d38aec6f493144a25390"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allEtatDuVehicle = this.apiResponse.record;
      }
    });
    //Boîte de vitesse
    this.queryParam = { "parent_id":"62e3d3d3ec6f493144a253b7"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allBoiteDeVitesse = this.apiResponse.record;
      }
    });
    //getColor
    this._http.get(this.getColor,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allColor = this.apiResponse.record;
      }
       
    });

    //type_de_piece
    this.queryParam = { "parent_id":"62e4f5a35ed7e4f294719634"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allTypeDePiece = this.apiResponse.record;
        //console.log(this.allModelList);
      }
    });


  }

  onCheckboxChange(e:any) {
    //console.log(e.target.value);
    if (e.target.checked) {
      this.carModelArray.push(e.target.value);
    } else {
      let i: number = 0;
      for( let i = 0; i < this.carModelArray.length; i++){ 
    
        if ( this.carModelArray[i] == e.target.value) { 
          //console.log("iff")
          this.carModelArray.splice(i, 1); 
        }
      }
    }
  }
  onColorChange(e:any) {
    //console.log(e.target.value);
    if (e.target.checked) {
      this.colorArray.push(e.target.value);
    } else {
      let i: number = 0;
      for( let i = 0; i < this.colorArray.length; i++){ 
    
        if ( this.colorArray[i] == e.target.value) { 
          //console.log("iff")
          this.colorArray.splice(i, 1); 
        }
      }
    }
  }

  onYearChange(e:any) {
    //console.log(e.target.value);
    if (e.target.checked) {
      this.yearArray.push(e.target.value);
    } else {
      let i: number = 0;
      for( let i = 0; i < this.yearArray.length; i++){ 
    
        if ( this.yearArray[i] == e.target.value) { 
          //console.log("iff")
          this.yearArray.splice(i, 1); 
        }
      }
    }
  }
  onYearChangeAccess(e:any) {
    //console.log(e.target.value);
    if (e.target.checked) {
      this.yearArrayAccess.push(e.target.value);
    } else {
      let i: number = 0;
      for( let i = 0; i < this.yearArrayAccess.length; i++){ 
    
        if ( this.yearArrayAccess[i] == e.target.value) { 
          //console.log("iff")
          this.yearArrayAccess.splice(i, 1); 
        }
      }
    }
  }
  onchahisChange(e:any) {
    //console.log(e.target.value);
    if (e.target.checked) {
      this.chahisArray.push(e.target.value);
    } else {
      let i: number = 0;
      for( let i = 0; i < this.chahisArray.length; i++){ 
    
        if ( this.chahisArray[i] == e.target.value) { 
          //console.log("iff")
          this.chahisArray.splice(i, 1); 
        }
      }
    }
  }
  accessModelFun(eVal:any)
  {
    this.queryParam = { "parent_id":eVal  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allSubModelList = this.apiResponse.record;
        //console.log("response of api allSubModelList "+this.allSubModelList);
      }
    });
  }
  showSlide()
  {
    console.log("heree");
    this.showDiv = true;
    this.showDivButton = false;
  }
  hideSlide()
  {
    this.showDiv = false;
    this.showDivButton = true;
  }
  categoryRadioBtn(categoryType:string)
  {
    //console.log(categoryType);
    this.categoryTypeForm = categoryType;
    if(categoryType == 'Porsche Voitures')
    {
      this.tab1Div = true;
      this.tab2Div = false;
    }else{
      this.tab1Div = false;
      this.tab2Div = true;
    }
  }

  form = new UntypedFormGroup({
    //carModel: new UntypedFormControl('',[]),
    fuel: new UntypedFormControl('',[]),
    type_of_gearbox: new UntypedFormControl('',[]),
    vehicle_condition: new UntypedFormControl('',[]),
    colorId: new UntypedFormControl('',[]),
    warranty: new UntypedFormControl('', []),
    maintenance_booklet: new UntypedFormControl('', []),
    maintenance_invoice: new UntypedFormControl('', []),
    accidented: new UntypedFormControl('', []),
    original_paint: new UntypedFormControl('', []),
    matching_number: new UntypedFormControl('', []),
    matching_color_paint: new UntypedFormControl('', []),
    matching_color_interior: new UntypedFormControl('', []),
    price_for_pors: new UntypedFormControl('', []),
    deductible_VAT: new UntypedFormControl('', []),
    minPrice: new UntypedFormControl('', []),
    maxPrice: new UntypedFormControl('', []),
    minKilometer: new UntypedFormControl('', []),
    maxKilometer: new UntypedFormControl('', []),
    model_variant: new UntypedFormControl('', []),

    accessModel: new UntypedFormControl('', []),
    subModel: new UntypedFormControl('', []),
    type_de_piece: new UntypedFormControl('', []),
    state: new UntypedFormControl('', []),
    OEM: new UntypedFormControl('', []),
    minAccessPrice: new UntypedFormControl('', []),
    maxAccessPrice: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  

  submit(){
    //console.log("here");
    console.log("here");
    //.log(this.carModelArray);
    //console.log(this.categoryTypeForm);
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      
      this.queryParam = {
        "category":this.categoryTypeForm,
        "carModelArray":this.carModelArray,
        "colorArray":this.colorArray,
        "yearArrayAccess":this.yearArrayAccess,
        "yearArray":this.yearArray,"chahisArray":this.chahisArray,"fuel":this.form.value.fuel,"type_of_gearbox":this.form.value.type_of_gearbox,"vehicle_condition":this.form.value.vehicle_condition,"warranty":this.form.value.warranty,"maintenance_booklet":this.form.value.maintenance_booklet,
        "maintenance_invoice":this.form.value.maintenance_invoice,
        "accidented":this.form.value.accidented,
        "original_paint":this.form.value.original_paint,
        "matching_number":this.form.value.matching_number,
        "matching_color_paint":this.form.value.matching_color_paint,
        "matching_color_interior":this.form.value.matching_color_interior,
        "deductible_VAT":this.form.value.deductible_VAT,
        "minPrice":this.form.value.minPrice,
        "maxPrice":this.form.value.maxPrice,
        "minKilometer":this.form.value.minKilometer,
        "model_variant":this.form.value.model_variant,
        "maxKilometer":this.form.value.maxKilometer,
        "accessModel":this.form.value.accessModel,
        "subModel":this.form.value.subModel,
        "type_de_piece":this.form.value.type_de_piece,
        "state":this.form.value.state,
        "OEM":this.form.value.OEM,
        "minAccessPrice":this.form.value.minAccessPrice,
        "maxAccessPrice":this.form.value.maxAccessPrice,
      };

      console.log(this.queryParam);
      
      this._http.post(this.getWebProductList,this.queryParam).subscribe((response:any)=>{
        this.apiResponse = response;
        console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          ///this.allTypeDePiece = this.apiResponse.record;
          //console.log(this.allModelList);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
