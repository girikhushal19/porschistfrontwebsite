import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdAccessoriesPostProfessionalComponent } from './ad-accessories-post-professional.component';

describe('AdAccessoriesPostProfessionalComponent', () => {
  let component: AdAccessoriesPostProfessionalComponent;
  let fixture: ComponentFixture<AdAccessoriesPostProfessionalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdAccessoriesPostProfessionalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdAccessoriesPostProfessionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
