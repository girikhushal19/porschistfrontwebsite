import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ad-accessories-post-professional',
  templateUrl: './ad-accessories-post-professional.component.html',
  styleUrls: ['./ad-accessories-post-professional.component.css']
})
export class AdAccessoriesPostProfessionalComponent implements OnInit {

 
  userAddress: string = '';webGetUserProfile:any;
  userLatitude: string = '';formDataForMultiFile:any;
  userLongitude: string = '';priceForPors:any;allSubModelList:any;allWebSubModel:any;
  base_url = "";base_url_node = "";ad_id:any;webStep3CarAdName:any;
  addModelsSubmit:any;getSubModel:any;allCategoryList:any;webStep5AccessoriesAdName:any;
  webStep4AccessoriesAdName:any;webStep6AccessoriesAdName:any;
  token:any;user_type:any;apiResponse:any;apiResponseLast:any;formValue:any;record:any;totalPageNumber:any;
  allModel:any;allModelList:any;queryParam:any;numbers:any;allModelCount:any;
  base_url_node_only:any;allCarburant:any;allBoiteDeVitesse:any;webStep1CarAdName:any;
  updateModelStatusApi:any;getSubAttribute:any;getColor:any;allColor:any;
  queryParamNew:any;updateModelStatus:any;numofpage_0:any;apiStringify:any;getCarCategory:any;allCarCategory:any;allYear:any;allEtatDuVehicle:any;titleOfAd:any;
  registration_year:any;allChasisType:any;formData:any;webStep2CarAdName:any;
  personalDetails!: UntypedFormGroup;addAdImageTesting:any;user_email:any;
  addressDetails!: UntypedFormGroup;user_id:any;lastName:any;enterprise:any; user_mobileNumber:any;
  educationalDetails!: UntypedFormGroup;firstName:any;
  personal_step = false;selectedDevice:any;webStep2AccessoriesAdName:any;webStep3AccessoriesAdName:any;webStep6BeforeAccessoriesAdName:any;
  address_step = false;webStep7AccessoriesAdName:any;
  education_step = false;webGetAdTitle:any;
  
  
  step = 1;
  
  webGetUserAccessoriesParticularPlan:any;webGetUserAccessoriesParticularPlanTopUrgent:any;webPaymentAccessAdProfessional:any;allUserParticularPlan:any;allUserParticularPlanTopUrgent:any;
  model_id_on_change:any;
  first_plan_price:number;second_plan_price:number;third_plan_price:number;final_plan_price:number;
  totalImage:number; apiResponse_new:any;
  tempArr: any = { "brands": [] };
  myPiwiReportFiles:string [] = [];
  myServiceReportFiles:string [] = [];

  centerLatitude = 47.034577;
  centerLongitude = 1.156914;
  webGetUserProPlan:any;webGetUserProPlanTopSearch:any;
  getCarAdForImagePrice:any;allAddPriceImage:any;
  allUserParticularPlanTopSearch:any;  apiResponseStepFive:any;
  maxUploadImage:number = 10; isUpload:boolean=false; allUploadedFile:number=0;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService,private router: Router)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    
    // console.log(this.token);
    // console.log(this.user_id);
    // console.log(this.user_type);

    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }
    this.ad_id = this.actRoute.snapshot.params['id'];

    this.webGetUserProPlan = this.base_url_node+"webGetUserProPlan";
    this.getCarAdForImagePrice = this.base_url_node+"getCarAdForImagePrice";
    this.webGetUserProPlanTopSearch = this.base_url_node+"webGetUserProPlanTopSearch";


    this.webGetAdTitle = this.base_url_node+"webGetAdTitle";
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.allModel = this.base_url_node+"getModel";
    this.getSubModel = this.base_url_node+"getSubModel";
    this.registration_year = this.base_url_node+"registration_year";
    this.allWebSubModel = this.base_url_node+"allWebSubModel";
    this.getSubAttribute = this.base_url_node+"getSubAttribute";
    this.webStep2AccessoriesAdName = this.base_url_node+"webStep2AccessoriesAdName";
    this.webStep3AccessoriesAdName = this.base_url_node+"webStep3AccessoriesAdName";
    this.webStep4AccessoriesAdName = this.base_url_node+"webStep4AccessoriesAdName";
    this.webStep5AccessoriesAdName = this.base_url_node+"webStep5AccessoriesAdName";
    this.webStep6AccessoriesAdName = this.base_url_node+"webStep6AccessoriesAdName";
    this.webStep7AccessoriesAdName = this.base_url_node+"webStep7AccessoriesAdName";
    this.webStep6BeforeAccessoriesAdName = this.base_url_node+"webStep6BeforeAccessoriesAdName";

    this.webGetUserAccessoriesParticularPlan = this.base_url_node+"webGetUserAccessoriesParticularPlan";
    this.webGetUserAccessoriesParticularPlanTopUrgent = this.base_url_node+"webGetUserAccessoriesParticularPlanTopUrgent";
    this.webPaymentAccessAdProfessional = this.base_url_node+"webPaymentAccessAdProfessional";

    this.first_plan_price = 0;this.second_plan_price = 0;this.third_plan_price = 0;
    this.final_plan_price = 0;this.totalImage = 0;
  }

  ngOnInit(): void {

    this.queryParam = {plan_type: 'accessories_plan'};
    this._http.post(this.webGetUserProPlan,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlan = this.apiResponse.record;
        console.log("this.allUserParticularPlan "+this.allUserParticularPlan);
      }
    });

    this.queryParam = {plan_type: 'accessories_plan'};
    this._http.post(this.webGetUserProPlanTopSearch,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlanTopSearch = this.apiResponse.record;
        console.log("this.allUserParticularPlanTopSearch "+this.allUserParticularPlanTopSearch);
      }
    });

    


    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
        this.firstName = this.apiResponse.userRecord.firstName;
        this.lastName = this.apiResponse.userRecord.lastName;
        this.enterprise = this.apiResponse.userRecord.enterprise;
        this.user_email = this.apiResponse.userRecord.email;
        this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
      }
       
    });
    //titleOfAd
    this.queryParam = {"ad_id":this.ad_id};
    this._http.post(this.webGetAdTitle,this.queryParam).subscribe((response:any)=>{
      //console.log("image response "+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log(this.apiResponse.record.accessoires_image);
        this.titleOfAd = this.apiResponse.record.ad_name;
        //this.allYear = this.apiResponse.record;
        this.totalImage = this.apiResponse.record.accessoires_image.length;
        if(this.totalImage > 1)
        {
          this.totalImage = this.totalImage - 1;
        }
        console.log("totalImage "+this.totalImage);
      }
    });

    this._http.get(this.registration_year,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allYear = this.apiResponse.record;
      }
    });

    this.queryParam = {attribute_type:"Pièces et accessoires"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allModelList = this.apiResponse.record;
      }
    });

    this.queryParam = { "parent_id":"62e4f5a35ed7e4f294719634"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allChasisType = this.apiResponse.record;
        //console.log(this.allModelList);
      }
    });

   

  }


  abc = new UntypedFormGroup({
    ad_id: new UntypedFormControl('', []),
    model_id: new UntypedFormControl('', [Validators.required]),
    subModelId: new UntypedFormControl('', [Validators.required]),
    registration_year: new UntypedFormControl('', [Validators.required]),
    registration_month: new UntypedFormControl('', [Validators.required]),
    type_de_piece: new UntypedFormControl('', [Validators.required]),
    state: new UntypedFormControl('', [Validators.required]),
    OEM: new UntypedFormControl('', [Validators.required]),
  });
  get abcc(){
    return this.abc.controls;
  }

  
  formSecondStep = new UntypedFormGroup({
    
  });
  get formSecondStepFun(){
    return this.formSecondStep.controls;
  }

  formThirdStep = new UntypedFormGroup({
    //title: new UntypedFormControl('', [Validators.required]),
    description: new UntypedFormControl('', [Validators.required])
  });

  get formThirdStepFun(){
    return this.formThirdStep.controls;
  }
  formBeforeFourthStep = new UntypedFormGroup({
    
    price: new UntypedFormControl('', [Validators.required]),
    pro_price: new UntypedFormControl('', [])
  });

  get formBeforeFourthStepFun(){
    return this.formBeforeFourthStep.controls;
  }
  formFourthStep = new UntypedFormGroup({
    file: new UntypedFormControl('', [Validators.required]),

  });
  get formFourthStepFun(){
    return this.formFourthStep.controls;
  }
  
  formFifthStep = new UntypedFormGroup({
    address: new UntypedFormControl('', [Validators.required])
  });
  get formFifthStepFun(){
    return this.formFifthStep.controls;
  }
  formBeforeFifthStep = new UntypedFormGroup({
    shipping_type_1: new UntypedFormControl('', []),
    shipping_type_2: new UntypedFormControl('', []),
    shipping_type_3: new UntypedFormControl('', []),
    shipping_type_4: new UntypedFormControl('', []),

    shipping_type_1_text: new UntypedFormControl('Livraison standard', []),
    shipping_type_2_text: new UntypedFormControl('Livraison express', []),
    shipping_type_3_text: new UntypedFormControl('Livraison gratuite', []),
    shipping_type_4_text: new UntypedFormControl('', []),

    shipping_price_1: new UntypedFormControl('', []),
    shipping_price_2: new UntypedFormControl('', []),
    //shipping_price_3: new UntypedFormControl('', []),
    shipping_price_4: new UntypedFormControl('', [])

  });
  get formBeforeFifthStepFun(){
    return this.formBeforeFifthStep.controls;
  }

  formSixStep = new UntypedFormGroup({
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    enterprise: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', []),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    hideNumber: new UntypedFormControl('', [])
  });
  get formSixStepFun(){
    return this.formSixStep.controls;
  }
  //formSevenStep
  formSevenStep = new UntypedFormGroup({
    
    paymentType: new UntypedFormControl('', []),
    ad_id: new UntypedFormControl('', []),
    user_id: new UntypedFormControl('', []),
    proUserAd: new UntypedFormControl('', []),
    proUserAdTopSearch: new UntypedFormControl('', []),


  });
  get formSevenStepFun(){
    return this.formSevenStep.controls;
  }
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onChangeModel(event:any){
    this.model_id_on_change=event;
    /*console.log("hello");
    console.log(this.selectedDeviceR);
    console.log("category");
    console.log(this.form.value.category);*/
    //allSubCatListList
    this.queryParam = { "parent_id":this.model_id_on_change  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      console.log(this.model_id_on_change);
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allSubModelList = this.apiResponse.record;
      }
    });
  }
  next()
  {
    if(this.step==1)
    {
      //console.log("hereeee");
      //this.personal_step = true;
      if (this.abc.invalid)
      {
        this.validateAllFormFields(this.abc); 
        return;
      }else{
        //console.log("here");
        this._http.post(this.webStep2AccessoriesAdName,this.abc.value).subscribe((response:any)=>{
          this.apiResponse = response;
          console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });        
      }
    }
    else if(this.step==2)
    {
      if (this.formThirdStep.invalid)
      {
        this.validateAllFormFields(this.formThirdStep);  return;
      }else{
        let adData = { "description":this.formThirdStep.value.description,"ad_id":this.ad_id};
        this._http.post(this.webStep3AccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });
        //this.step++;

      }
    }
    else if(this.step==3)
    {
      if (this.formBeforeFourthStep.invalid)
      {
        this.validateAllFormFields(this.formBeforeFourthStep);  return;
      }else{
        //webStep4AccessoriesAdName
        //formBeforeFourthStep
        let adData = {"price":this.formBeforeFourthStep.value.price,"pro_price":this.formBeforeFourthStep.value.pro_price,"ad_id":this.ad_id};
        this._http.post(this.webStep4AccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });

      }
      //this.step++;
    }
    else if(this.step==4)
    {
      this.formData = new FormData();
      this.formData.append('ad_id', this.ad_id);
      for (var i = 0; i < this.myFiles.length; i++)
      { 
        this.formData.append("exterior_image", this.myFiles[i]);
      }
      
      this._http.post(this.webStep5AccessoriesAdName,this.formData).subscribe((response:any)=>{
        this.apiResponse = response;
        //console.log("formData "+this.formData);
        if(this.apiResponse.error == false)
        {
          //console.log(this.apiResponse.lastInsertId);
          //this.ad_id = this.apiResponse.lastInsertId;
          //console.log("ad_id "+this.ad_id);
          this.step++;
        }
      });
      //this.step++;
    }
    else if(this.step==5)
    {
      if (this.formBeforeFifthStep.invalid)
      {
        this.validateAllFormFields(this.formBeforeFifthStep);  return;
      }else{
        //formBeforeFifthStep
        let adData = {"shipping_price_1":this.formBeforeFifthStep.value.shipping_price_1,
        "shipping_price_2":this.formBeforeFifthStep.value.shipping_price_2,
        "shipping_price_4":this.formBeforeFifthStep.value.shipping_price_4,
        "shipping_type_1":this.formBeforeFifthStep.value.shipping_type_1,
        "shipping_type_2":this.formBeforeFifthStep.value.shipping_type_2,
        "shipping_type_3":this.formBeforeFifthStep.value.shipping_type_3,
        "shipping_type_4":this.formBeforeFifthStep.value.shipping_type_4,
        "shipping_type_1_text":this.formBeforeFifthStep.value.shipping_type_1_text,
        "shipping_type_2_text":this.formBeforeFifthStep.value.shipping_type_2_text,
        "shipping_type_3_text":this.formBeforeFifthStep.value.shipping_type_3_text,
        "shipping_type_4_text":this.formBeforeFifthStep.value.shipping_type_4_text,
        "ad_id":this.ad_id};
        this._http.post(this.webStep6BeforeAccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponseStepFive = response;
          
          if(this.apiResponseStepFive.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
        });
      }
    }
    else if(this.step==6)
    {
      //this.address_step = true;
      this.queryParam = {"ad_id":this.ad_id};
      this._http.post(this.getCarAdForImagePrice,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          this.allAddPriceImage = this.apiResponse.record;
        }
          
      });

      if (this.formFifthStep.invalid)
      {
        this.validateAllFormFields(this.formFifthStep);  return;
      }else{
        let adData = {"address": this.userAddress,"latitude":this.userLatitude,"longitude":this.userLongitude,"ad_id":this.ad_id};
        this._http.post(this.webStep6AccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
        });
      }
      //this.step++;
    }
    else if(this.step==7)
    {
      //this.address_step = true;  formSixStep
      if (this.formSixStep.invalid)
      {
        this.validateAllFormFields(this.formSixStep);  return;
      }else{
        let adData = {'mobileNumber':this.formSixStep.value.mobileNumber,'hideNumber':this.formSixStep.value.hideNumber,"ad_id":this.ad_id,"enterprise":this.formSixStep.value.enterprise};
        this._http.post(this.webStep7AccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponse_new = response;
          //console.log("formData "+this.formData);
          if(this.apiResponse_new.error == false)
          {
            //console.log(this.apiResponse_new.lastInsertId);
            //this.ad_id = this.apiResponse_new.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
            //adSuccess
            // setTimeout(() => {
            //   //window.location.reload();
            //   window.location.href = this.base_url+"adSuccess";

            // }, 2000);

          }
        });
      }
      //this.step++;
    }
    else if(this.step == 8)
    {
      if (this.formSevenStep.invalid)
      {
        this.validateAllFormFields(this.formSevenStep);  return;
      }else{
        
        //console.log(this.formSevenStep.value);
        this._http.post(this.webPaymentAccessAdProfessional,this.formSevenStep.value).subscribe((response:any)=>{
          this.apiResponseLast = response;
          //console.log("formData "+this.formData);
          if(this.apiResponseLast.error == false)
          {
            
            if(this.apiResponseLast.price == 0)
            {
              var redirect_url = this.base_url+"adSuccess";
            }else{
              if(this.formSevenStep.value.paymentType == "Paypal")
              {
                var redirect_url = this.base_url_node+"getProUsrCarAdPaymentPaypal?id="+this.apiResponseLast.lastInsertId+"&price="+this.apiResponseLast.price;
              }else{
                var redirect_url = this.base_url_node+"getProUsrCarAdPaymentStripe?id="+this.apiResponseLast.lastInsertId+"&price="+this.apiResponseLast.price;
              }
            }
            //return false;
            //var redirect_url = this.apiResponseLast.redirect_url_web;
            //
            //this.ad_id = this.apiResponseLast.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            //this.step++;

              setTimeout(() => {
                window.location.href = redirect_url;
              }, 2000);

          }
        });
      }
    }
  }

  previous()
  {
    this.step--
   
  }
  images : string[] = [];
  myFiles:string [] = [];
  onFileChangeNew(event:any)
  {
    //this.myFiles = [];
    if (event.target.files && event.target.files[0])
    {
      this.allUploadedFile = this.myFiles.length + event.target.files.length;
        console.log("this.allUploadedFile ",this.allUploadedFile);

      if(this.maxUploadImage >= this.allUploadedFile)
      {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++)
        {
          var reader = new FileReader();
          reader.onload = (event:any) => {
            // Push Base64 string
            this.images.push(event.target.result); 
            //this.patchValues();
          }
          reader.readAsDataURL(event.target.files[i]);

          this.myFiles.push(event.target.files[i]);

        }
      }
    }
  }
  removeImage(url:any,imgNumber:any){
    //console.log(this.images,url);
    //console.log(this.myFiles);
    this.images = this.images.filter(img => (img != url));
    //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
    ///this.patchValues();
    //console.log(imgNumber);
    this.myFiles.splice(imgNumber,1);
      


  }

  handleAddressChange(address: any) {
    
    this.userAddress = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);
    console.log("this.userAddress");
    console.log(this.userAddress);
    this.centerLatitude = parseFloat(address.geometry.location.lat());
    this.centerLongitude = parseFloat(address.geometry.location.lng());
    console.log(this.centerLatitude);
    console.log(this.centerLongitude);
    this.marker = {
      position: { lat: this.centerLatitude, lng: this.centerLongitude },
    }

  }
  onItemChange(event:any)
  {
     this.first_plan_price = 0;
     this.first_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price;
    //console.log(event);
  }
  onItemChangeSec(event:any)
  {
    this.second_plan_price = 0;
     this.second_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price;
    //console.log(event);
  }  

  mapOptions: google.maps.MapOptions = {
    center: { lat: this.centerLatitude, lng: this.centerLongitude },
    zoom : 4
  }
  marker = {
    position: { lat: this.centerLatitude, lng: this.centerLongitude },
  }
  

}
