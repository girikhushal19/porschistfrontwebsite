import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdNormalUserAcesAdReactivateComponent } from './ad-normal-user-aces-ad-reactivate.component';

describe('AdNormalUserAcesAdReactivateComponent', () => {
  let component: AdNormalUserAcesAdReactivateComponent;
  let fixture: ComponentFixture<AdNormalUserAcesAdReactivateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdNormalUserAcesAdReactivateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdNormalUserAcesAdReactivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
