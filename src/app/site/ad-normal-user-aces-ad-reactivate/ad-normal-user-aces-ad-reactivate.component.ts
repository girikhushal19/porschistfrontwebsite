import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ad-normal-user-aces-ad-reactivate',
  templateUrl: './ad-normal-user-aces-ad-reactivate.component.html',
  styleUrls: ['./ad-normal-user-aces-ad-reactivate.component.css']
})
export class AdNormalUserAcesAdReactivateComponent implements OnInit {
  userAddress: string = '';webGetUserProfile:any;
  userLatitude: string = '';formDataForMultiFile:any;
  userLongitude: string = '';priceForPors:any;allSubModelList:any;allWebSubModel:any;
  base_url = "";base_url_node = "";ad_id:any;webStep3CarAdName:any;
  addModelsSubmit:any;getSubModel:any;allCategoryList:any;webAcesPhotoPack:any;
  webStep4AccessoriesAdName:any;webStep6AccessoriesAdName:any;
  token:any;user_type:any;apiResponse:any;apiResponseLast:any;formValue:any;record:any;totalPageNumber:any;apiResponse2:any;
  allModel:any;allModelList:any;queryParam:any;numbers:any;allModelCount:any;
  base_url_node_only:any;allCarburant:any;allBoiteDeVitesse:any;webStep1CarAdName:any;
  updateModelStatusApi:any;getSubAttribute:any;getColor:any;allColor:any;
  queryParamNew:any;updateModelStatus:any;numofpage_0:any;apiStringify:any;getCarCategory:any;allCarCategory:any;allYear:any;allEtatDuVehicle:any;titleOfAd:any;
  registration_year:any;allChasisType:any;formData:any;webStep2CarAdName:any;
  personalDetails!: UntypedFormGroup;addAdImageTesting:any;user_email:any;
  addressDetails!: UntypedFormGroup;user_id:any;lastName:any;user_mobileNumber:any;
  educationalDetails!: UntypedFormGroup;firstName:any;
  personal_step = false;selectedDevice:any;webStep2AccessoriesAdName:any;webStep3AccessoriesAdName:any;webStep6BeforeAccessoriesAdName:any;
  address_step = false;webStep7AccessoriesAdName:any;
  education_step = false;webGetAdTitle:any;
  step = 1;webGetUserAccessoriesParticularPlan:any;webGetUserAccessoriesParticularPlanTopUrgent:any;webPaymentAccessParticularRenewal:any;allUserParticularPlan:any;allUserParticularPlanTopUrgent:any;
  model_id_on_change:any;
  first_plan_price:number;second_plan_price:number;third_plan_price:number;final_plan_price:number;
  totalImage:number;
  tempArr: any = { "brands": [] };
  myPiwiReportFiles:string [] = [];
  myServiceReportFiles:string [] = [];
  webGetUserParticularPlanTopSearchListAccess:any;
  centerLatitude = 47.034577;
  centerLongitude = 1.156914;
  getCarAdForImagePrice:any;allAddPriceImage:any;
  allUserParticularPlanTopSearch:any;remainingUpload:number=7;fileError:string="";
  additional_photo_check:Boolean=false;  category:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService,private router: Router) {

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    
    // console.log(this.token);
    // console.log(this.user_id);
    // console.log(this.user_type);

    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }
    this.ad_id = this.actRoute.snapshot.params['id'];

    
    this.webAcesPhotoPack = this.base_url_node+"webAcesPhotoPack";
    this.first_plan_price = 0;this.second_plan_price = 0;this.third_plan_price = 0;
    this.final_plan_price = 0;this.totalImage = 0;this.remainingUpload=7;

    
    this.webPaymentAccessParticularRenewal = this.base_url_node+"webPaymentAccessParticularRenewal";
    this.webGetAdTitle = this.base_url_node+"webGetAdTitle";
    this.webGetUserAccessoriesParticularPlan = this.base_url_node+"webGetUserAccessoriesParticularPlan";

    this.queryParam = {user_type:"normal_user"};
    this._http.post(this.webGetUserAccessoriesParticularPlan,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlan = this.apiResponse.record;
        console.log(" this.allUserParticularPlan");
        console.log(this.allUserParticularPlan);
      }
    });


    this.getCarAdForImagePrice = this.base_url_node+"getCarAdForImagePrice";
    this.queryParam = {"ad_id":this.ad_id};
    this._http.post(this.getCarAdForImagePrice,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allAddPriceImage = this.apiResponse.record;
        if(this.allAddPriceImage)
        {
          if(this.allAddPriceImage.length > 0)
          {
            this.category = this.allAddPriceImage[0].category;
          }
        }
        console.log("this.category ", this.category);
      }
        
    });


  }

  ngOnInit(): void {

    this.queryParam = {"ad_id":this.ad_id};
    this._http.post(this.webGetAdTitle,this.queryParam).subscribe((response:any)=>{
      //console.log("image response "+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log(this.apiResponse.record.accessoires_image_new);
        this.titleOfAd = this.apiResponse.record.ad_name;
        //this.allYear = this.apiResponse.record;
        var old_img = this.apiResponse.record.accessoires_image.length;
        this.remainingUpload = this.remainingUpload - old_img;
        this.totalImage = this.apiResponse.record.accessoires_image_new.length;
        if(this.totalImage > 1)
        {
          this.totalImage = this.totalImage - 1;
          
        }
        console.log("totalImage "+this.totalImage);
        console.log("remainingUpload "+this.remainingUpload);
        //this.remainingUpload
      }
    });

  }

  images : string[] = [];
  myFiles:string [] = [];
  onFileChangeNew(event:any)
  {
    //this.myFiles = [];
    
    this.fileError = "";
    if (event.target.files && event.target.files[0])
    {
      var filesAmount = event.target.files.length;
      console.log(this.myFiles)
      console.log("this.myFiles.length")
      console.log(this.myFiles.length)
      console.log("event.target.files.length")
      console.log(event.target.files.length)
      

      if(filesAmount > this.remainingUpload)
      {
        this.fileError = "Vous pouvez télécharger 3 photos gratuitement ou choisir le pack 7 photos supplémentaires pour seulement 9,11 € TTC";
         
      }else{
        let new_length = event.target.files.length + this.myFiles.length;
        if(new_length > this.remainingUpload)
        {
          this.fileError = "Vous pouvez télécharger 3 photos gratuitement ou choisir le pack 7 photos supplémentaires pour seulement 9,11 € TTC";
        }else{
          for (let i = 0; i < filesAmount; i++)
          {
            var reader = new FileReader();
            reader.onload = (event:any) => {
              // Push Base64 string
              this.images.push(event.target.result); 
              //this.patchValues();
            }
            reader.readAsDataURL(event.target.files[i]);

            this.myFiles.push(event.target.files[i]);

          }
        }
        
      }
      
    }
  }
  removeImage(url:any,imgNumber:any){
    //console.log(this.images,url);
    //console.log(this.myFiles);
    this.images = this.images.filter(img => (img != url));
    //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
    ///this.patchValues();
    //console.log(imgNumber);
    this.myFiles.splice(imgNumber,1);
      


  }
  formFourthStep = new UntypedFormGroup({
    ad_id: new UntypedFormControl('', []),
    file: new UntypedFormControl('', [Validators.required]),
    additional_photo_check: new UntypedFormControl('', [Validators.required]),
    paymentType: new UntypedFormControl('', [Validators.required]),
  });
  get formFourthStepFun(){
    return this.formFourthStep.controls;
  }
  formSevenStep = new UntypedFormGroup({
    paymentType: new UntypedFormControl('', [Validators.required]),
    ad_id: new UntypedFormControl('', []),
    normalUserAd: new UntypedFormControl('', [Validators.required]), 
  });
  get formSevenStepFun(){
    return this.formSevenStep.controls;
  }
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  next()
  {
    if(this.step==1)
    {
      if (this.formFourthStep.invalid)
      {
        this.validateAllFormFields(this.formFourthStep);  return;
      }else{
        this.formData = new FormData();
        this.formData.append('ad_id', this.ad_id);
        for (var i = 0; i < this.myFiles.length; i++)
        { 
          this.formData.append("accessoires_image_new", this.myFiles[i]);
        }
        
        this._http.post(this.webAcesPhotoPack,this.formData).subscribe((response:any)=>{
          this.apiResponse2 = response;
          //console.log("formData "+this.formData);
          if(this.apiResponse2.error == false)
          {
            //console.log(this.apiResponse2.lastInsertId);
            //this.ad_id = this.apiResponse2.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            //this.step++;

             
            //return;
            if(this.category == "Pièces et accessoires")
            {
              console.log("Pièces et accessoires");
              //this.formSevenStep.value.ad_id = this.ad_id;
              //this.formSevenStep.value.ad_id = this.ad_id;
              this._http.post(this.base_url_node+"webPaymentAccessoriesParticular",this.formFourthStep.value).subscribe((response:any)=>{
                this.apiResponseLast = response;
                //console.log("formData "+this.formData);
                if(this.apiResponseLast.error == false)
                {
                  console.log(this.apiResponseLast);
                  console.log("price ", this.apiResponseLast.price);
                  
                  //return ;
                  
                    if(this.formFourthStep.value.paymentType == "" || this.formFourthStep.value.paymentType == null)
                    {
                      var redirect_url = this.base_url_node_only+"api/webAccessoriesAdPayment?"+"ad_id="+this.ad_id+"&user_id="+this.user_id+"&basic_main_plan_id="+''+"&top_urgent_plan_id="+''+"&paymentType="+'Stripe'+"&price="+this.apiResponseLast.price+"&normalUserAdTopSearch="+''+"&which_week="+''+"&additional_photo_check="+this.additional_photo_check;
                    }else{
                      var redirect_url = this.base_url_node_only+"api/webAccessoriesAdPayment?"+"ad_id="+this.ad_id+"&user_id="+this.user_id+"&basic_main_plan_id="+''+"&top_urgent_plan_id="+''+"&paymentType="+this.formFourthStep.value.paymentType+"&price="+this.apiResponseLast.price+"&normalUserAdTopSearch="+''+"&which_week="+''+"&additional_photo_check="+this.additional_photo_check;
                    }
                  
                  //console.log(this.apiResponseLast.lastInsertId);
                  //this.ad_id = this.apiResponseLast.lastInsertId;
                  //console.log("ad_id "+this.ad_id);
                  //this.step++;
      
                   setTimeout(() => {
                     window.location.href = redirect_url;
                   }, 2000);
      
                }
              });


            }else{
              console.log("Car");
              
              this.formFourthStep.value.user_id = this.user_id;
              this._http.post(this.base_url_node+"webPaymentStepCarAdName",this.formFourthStep.value).subscribe((response:any)=>{
                this.apiResponseLast = response;
                //console.log("formData "+this.formData);
                if(this.apiResponseLast.error == false)
                {
                  var redirect_url = this.base_url_node_only+"api/webCarAdPayment?"+"ad_id="+this.ad_id+"&user_id="+this.user_id+"&basic_main_plan_id="+''+"&top_urgent_plan_id="+''+"&top_search_plan_id="+''+"&paymentType="+this.formFourthStep.value.paymentType+"&price="+this.apiResponseLast.price+"&additional_photo_check="+this.additional_photo_check;

                  console.log(redirect_url);
                  //return false;
                  setTimeout(() => {
                  window.location.href = redirect_url;
                  }, 2000);
                }
              });
            }

          }
        });
      }
    }else{
      console.log("ad_id "+this.ad_id);
      console.log("step "+this.step);
      if (this.formSevenStep.invalid)
      {
        this.validateAllFormFields(this.formSevenStep);  return;
      }else{

        


        this._http.post(this.webPaymentAccessParticularRenewal,this.formSevenStep.value).subscribe((response:any)=>{
          this.apiResponseLast = response;
          //console.log("formData "+this.formData);
          if(this.apiResponseLast.error == false)
          {
            //console.log(this.apiResponseLast);
            console.log(this.formSevenStep.value.paymentType);
            //return false;
            if(this.formSevenStep.value.paymentType == "" || this.formSevenStep.value.paymentType == null)
            {
              var redirect_url = this.base_url_node_only+"api/webAccessoriesAdPayment?"+"ad_id="+this.ad_id+"&user_id="+this.user_id+"&basic_main_plan_id="+this.formSevenStep.value.normalUserAdt+"&paymentType="+'Stripe'+"&price="+this.apiResponseLast.price;
            }else{
              var redirect_url = this.base_url_node_only+"api/webAccessAdPaymentRenewal?"+"ad_id="+this.ad_id+"&user_id="+this.user_id+"&basic_main_plan_id="+this.formSevenStep.value.normalUserAd+"&paymentType="+this.formSevenStep.value.paymentType+"&price="+this.apiResponseLast.price;
            }
            console.log(redirect_url);
            setTimeout(() => {
              window.location.href = redirect_url;
            }, 2000);

            
          }
        });
      }
      
    }

  
    
  }

    onItemChange(event:any)
  {
     this.first_plan_price = 0;
     
     this.first_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price +this.third_plan_price;
    //console.log(event);
  }

  
  addiotional_photo_7()
  {
    this.additional_photo_check=true;
  }


}
