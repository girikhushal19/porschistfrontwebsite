import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdCarPostParticularComponent } from './ad-car-post-particular.component';

describe('AdCarPostParticularComponent', () => {
  let component: AdCarPostParticularComponent;
  let fixture: ComponentFixture<AdCarPostParticularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdCarPostParticularComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdCarPostParticularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
