import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ad-car-post-particular',
  templateUrl: './ad-car-post-particular.component.html',
  styleUrls: ['./ad-car-post-particular.component.css']
})
export class AdCarPostParticularComponent implements OnInit {

 
  userAddress: string = '';webGetUserProfile:any;
  userLatitude: string = '';formDataForMultiFile:any;
  userLongitude: string = '';priceForPors:any;
  base_url = "";base_url_node = "";ad_id:any;webStep3CarAdName:any;
  addModelsSubmit:any;getCategory:any;allCategoryList:any;webStep4CarAdName:any;
  webStepBefore4CarAdName:any;webStep5CarAdName:any;
  token:any;user_type:any;apiResponse:any;apiResponseLast:any;formValue:any;record:any;totalPageNumber:any;
  allModel:any;allModelList:any;queryParam:any;numbers:any;allModelCount:any;
  base_url_node_only:any;allCarburant:any;allBoiteDeVitesse:any;webStep1CarAdName:any;
  updateModelStatusApi:any;getSubAttribute:any;getColor:any;allColor:any;
  queryParamNew:any;updateModelStatus:any;numofpage_0:any;apiStringify:any;getCarCategory:any;allCarCategory:any;allYear:any;allEtatDuVehicle:any;titleOfAd:any;
  registration_year:any;allChasisType:any;formData:any;webStep2CarAdName:any;
  personalDetails!: UntypedFormGroup;addAdImageTesting:any;user_email:any;
  addressDetails!: UntypedFormGroup;user_id:any;lastName:any;user_mobileNumber:any;
  educationalDetails!: UntypedFormGroup;firstName:any;
  personal_step = false;selectedDevice:any;webPaymentStepCarAdName:any;
  address_step = false;webStep6CarAdName:any;
  education_step = false;webGetUserParticularPlan:any;webGetUserParticularPlanTopSearchList:any;

  step = 1;

  allUserParticularPlan:any;allUserParticularPlanTopUrgent:any;
  model_id_on_change:any;webGetUserParticularPlanTopUrgent:any;allUserParticularPlanTopSearch:any;
  first_plan_price:number;second_plan_price:number;third_plan_price:number;final_plan_price:number;tax_percent:number;

  totalImage:number;
  maxUploadImage:number = 3; isUpload:boolean=false; allUploadedFile:number=0;
  additional_photo_check:Boolean=false; additional_photo_price:number=0;


  tempArr: any = { "brands": [] };
  myPiwiReportFiles:string [] = [];
  myServiceReportFiles:string [] = [];
  allSubModelList:any;allWebSubModel:any; allWebVersion:any;
  centerLatitude = 47.034577;
  centerLongitude = 1.156914;  optionsArray:any [] = [];
  getCarAdForImagePrice:any;allAddPriceImage:any; apiResponse_New:any; allOptions:any;allVersions:any;
  google_zip_code:any;google_city:string="";google_country:string="";colr_id:string="";allExtClrName:any;
  allIntClrName:any;allIntClr:any; uploadImgLength:number=0;
  secondStepDisable:Boolean = false; secondStepDisable_loading:Boolean = false;
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService,private router: Router,private actRoute: ActivatedRoute)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    
    // console.log(this.token);
    // console.log(this.user_id);
    // console.log(this.user_type);

    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }
    
    this.getCarAdForImagePrice = this.base_url_node+"getCarAdForImagePrice";
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.getCategory = this.base_url_node+"getCategoryOnAdPage";
    this.allModel = this.base_url_node+"getModel";
    this.getSubAttribute = this.base_url_node+"getSubAttribute";
    this.registration_year = this.base_url_node+"registration_year";
    this.getColor = this.base_url_node+"getColor";
    this.addAdImageTesting = this.base_url_node+"addAdImageTesting";
    this.webStep1CarAdName = this.base_url_node+"webStep1CarAdName";
    this.webStep2CarAdName = this.base_url_node+"webStep2CarAdName";
    this.webStep3CarAdName = this.base_url_node+"webStep3CarAdName";
    this.webStepBefore4CarAdName = this.base_url_node+"webStepBefore4CarAdName";
    this.webStep4CarAdName = this.base_url_node+"webStep4CarAdName";
    this.webStep5CarAdName = this.base_url_node+"webStep5CarAdName";
    this.webStep6CarAdName = this.base_url_node+"webStep6CarAdName";
    //console.log("here");
    this.webGetUserParticularPlan = this.base_url_node+"webGetUserParticularPlan";
    this.webGetUserParticularPlanTopSearchList = this.base_url_node+"webGetUserParticularPlanTopSearchList";
    this.webGetUserParticularPlanTopUrgent = this.base_url_node+"webGetUserParticularPlanTopUrgent";
    this.webPaymentStepCarAdName = this.base_url_node+"webPaymentStepCarAdName";
    this.allWebSubModel = this.base_url_node+"allWebSubModel";
    this.allWebVersion = this.base_url_node+"allWebVersion";

    this.first_plan_price = 0;this.second_plan_price = 0;this.third_plan_price = 0;
    this.final_plan_price = 0;this.tax_percent = 0;

    this.ad_id =  this.actRoute.snapshot.params['id'];
    console.log("this.ad_id "+this.ad_id);

    this._http.get(this.base_url_node+"allOptions",{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allOptions = this.apiResponse.record;
        //console.log("allOptions ",this.allOptions);
      }
    });

    this._http.get(this.base_url_node+"getColorInterior",{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allIntClr = this.apiResponse.record;
        //console.log("allOptions ",this.allOptions);
      }
    });

    // this._http.get(this.base_url_node+"allVersions",{}).subscribe((response:any)=>{
    //   //console.log("response of api"+response);
    //   this.apiResponse = response;
    //   //console.log(this.apiResponse);
    //   if(this.apiResponse.error == false)
    //   {
    //     this.allVersions = this.apiResponse.record;
    //     //console.log("allVersions ",this.allVersions);
    //   }
    // });
    
    this.first_plan_price = 0;this.second_plan_price = 0;this.third_plan_price = 0;
    this.final_plan_price = 0;this.totalImage = 0;this.uploadImgLength = 0;
  }
  colorChange(event:any)
  {
    console.log(event.target.value);
    this.colr_id = event.target.value;
    this._http.get(this.base_url_node+"getExtColorNameById/"+this.colr_id).subscribe((result:any)=>{
      //
      //console.log("result ", result);//
      this.allExtClrName = [];
      if(result.error == false)
      {
        this.allExtClrName = result.record;
      }
    })
  }
  
  colorChangeInt(event:any)
  {
    console.log(event.target.value);
    this.colr_id = event.target.value;
    this._http.get(this.base_url_node+"getIntColorNameById/"+this.colr_id).subscribe((result:any)=>{
      //
      //console.log("result ", result);//
      this.allIntClrName = [];
      if(result.error == false)
      {
        this.allIntClrName = result.record;
      }
    })
  }
  ngOnInit(): void {

    this._http.get(this.base_url_node+"getWebTax",{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.tax_percent = parseFloat(this.apiResponse.result.attribute_value);
        //console.log("this.tax_percent "+this.tax_percent);
      }
    });

    this._http.get(this.webGetUserParticularPlan,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlan = this.apiResponse.record;
      }
    });
    let cc = {"plan_type":"Car"};
    this._http.post(this.webGetUserParticularPlanTopUrgent,cc).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlanTopUrgent = this.apiResponse.record;
      }
    });
    
    this._http.get(this.webGetUserParticularPlanTopSearchList,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlanTopSearch = this.apiResponse.record;
        //console.log(this.allUserParticularPlanTopSearch);
      }
    });
    this._http.get(this.getCategory,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCategoryList = this.apiResponse.record;
      }
    });
    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
        this.firstName = this.apiResponse.userRecord.firstName;
        this.lastName = this.apiResponse.userRecord.lastName;
        this.user_email = this.apiResponse.userRecord.email;
        this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
        
      }
    });
    this._http.get(this.registration_year,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allYear = this.apiResponse.record;
      }
    });

    this.queryParam = {attribute_type:"Porsche Voitures"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);


      console.log("this.allModel    <<<<<<<-------->>>>>> ", this.allModel);


      this.apiResponse = response;
      
      if(this.apiResponse.error == false)
      {
        this.allModelList = this.apiResponse.record;
        //console.log(this.allModelList);

        
        console.log("this.allModelList    <<<<<<<-------->>>>>> ", this.allModelList);


      }
      
    });


    //Type de châssis
      this.queryParam = { "parent_id":"62e3d257ec6f493144a2533a"};
      this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          this.allChasisType = this.apiResponse.record;
          //console.log(this.allModelList);
        }
      });

      //Carburant
      this.queryParam = { "parent_id":"62e3d307ec6f493144a25377"};
      this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          this.allCarburant = this.apiResponse.record;
          //console.log("allCarburant"+this.allCarburant);
        }
      });
      //Etat du véhicule
      this.queryParam = { "parent_id":"62e3d38aec6f493144a25390"};
      this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          this.allEtatDuVehicle = this.apiResponse.record;
          //console.log(this.allModelList);
        }
      });
      //Boîte de vitesse
      this.queryParam = { "parent_id":"62e3d3d3ec6f493144a253b7"};
      this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          this.allBoiteDeVitesse = this.apiResponse.record;
          //console.log(this.allModelList);
        }
      });
      //getColor
      this._http.get(this.getColor,{}).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          this.allColor = this.apiResponse.record;
        }
        
      });

    /*this.personalDetails = this.formBuilder.group({
      name: new UntypedFormControl('', []),
      email: new UntypedFormControl('', [Validators.required]),
      phone: new UntypedFormControl('', [Validators.required])
    });
    this.addressDetails = this.formBuilder.group({
        city: new UntypedFormControl('', [Validators.required]),
        address: new UntypedFormControl('', [Validators.required]),
        pincode: new UntypedFormControl('', [Validators.required])
    });
    this.educationalDetails = this.formBuilder.group({
        highest_qualification: new UntypedFormControl('', [Validators.required]),
        university: new UntypedFormControl('', [Validators.required]),
        total_marks: new UntypedFormControl('', [Validators.required])
    });*/

    this.myPiwiReportFiles = [];
  }
  onOptionsCheckboxChange(e:any) {
    //console.log(e.target.value);
    if (e.target.checked) {
      this.optionsArray.push(e.target.value);
    } else {
      let i: number = 0;
      for( let i = 0; i < this.optionsArray.length; i++){ 
    
        if ( this.optionsArray[i] == e.target.value) { 
          //console.log("iff")
          this.optionsArray.splice(i, 1); 
        }
      }
    }
    console.log("optionsArray " , this.optionsArray)
  }
  onChangeModel(event:any){
    this.model_id_on_change=event;
    /*console.log("hello");
    console.log(this.selectedDeviceR);
    console.log("category");
    console.log(this.form.value.category);*/
    //allSubCatListList
    this.queryParam = { "parent_id":this.model_id_on_change  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      //console.log(this.model_id_on_change);
      
      console.log("response of api"+response);

      this.apiResponse = response;
      this.allVersions = [];
      //this.allVersions = this.apiResponse.all_version;
      if(this.apiResponse.error == false)
      {
        this.allSubModelList = this.apiResponse.record;
      }
    });
  }
  onChangeModelGeneration(event:any){
    let generation =event;
    /*console.log("hello");
    console.log(this.selectedDeviceR);
    console.log("category");
    console.log(this.form.value.category);*/
    //allSubCatListList
    this.queryParam = { "parent_id":this.model_id_on_change,"generation":generation  };
    this._http.post(this.allWebVersion,this.queryParam).subscribe((response:any)=>{
      //console.log(this.model_id_on_change);
      
      console.log("response of api"+response);

      this.apiResponse = response;
      this.allVersions = this.apiResponse.all_version;
       
    });
  }
  numSequence(n: number): Array<number> {
    return Array(n);
  }


  handleAddressChange(address: any) {
    this.userAddress = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);

    this.centerLatitude = parseFloat(address.geometry.location.lat());
    this.centerLongitude = parseFloat(address.geometry.location.lng());
    //console.log(this.centerLatitude);
    //console.log(this.centerLongitude);
    this.marker = {
      position: { lat: this.centerLatitude, lng: this.centerLongitude },
    }


    if(address)
    {
      if(address.address_components)
      {
        address.address_components.forEach((val:any)=>{
          if(val)
          {
            if(val.types)
            {
              if(val.types.length > 0)
              {
                for(let x=0; x<val.types.length; x++)
                {
                  console.log(val.types[x]);
                  if(val.types[x] == "postal_code")
                  {
                    this.google_zip_code = val.long_name;
                  }
                  if(val.types[x] == "country")
                  {
                    this.google_country = val.long_name;
                  }
                  if(val.types[x] == "locality")
                  {
                    this.google_city = val.long_name;
                  }
                }
              }
            }
          }
        })
      }
    }

  }
  onFileChange(event:any)
  {
    //this.myPiwiReportFiles = [];
    for (var i = 0; i < event.target.files.length; i++)
    { 
      this.myPiwiReportFiles.push(event.target.files[i]);
    }
  }

  abc = new UntypedFormGroup({
    
    title: new UntypedFormControl('', [Validators.required]),
    category: new UntypedFormControl('', [Validators.required])
  });
  get abcc(){
    return this.abc.controls;
  }
 
  

  
  formSecondStep = new UntypedFormGroup({
    optionVal: new UntypedFormControl('', [ ]),
    model_id: new UntypedFormControl('', [Validators.required]),
    subModelId: new UntypedFormControl('', [Validators.required]),
    model_variant: new UntypedFormControl('', [Validators.required]),
    registration_year: new UntypedFormControl('', [Validators.required]),
    registration_month: new UntypedFormControl('', [Validators.required]),
    type_of_chassis: new UntypedFormControl('', [Validators.required]),
    fuel: new UntypedFormControl('', [Validators.required]),
    vehicle_condition: new UntypedFormControl('', []),
    type_of_gearbox: new UntypedFormControl('', [Validators.required]),
    color_interior_name: new UntypedFormControl('', [ ]),
    color_interior: new UntypedFormControl('', [Validators.required]),
    color_exterieur: new UntypedFormControl('', [Validators.required]),
    color_exterieur_name: new UntypedFormControl('', []),
    pors_warranty: new UntypedFormControl('', []),
    warranty: new UntypedFormControl('', []),
    report_piwi: new UntypedFormControl('', [ ]),
    warranty_month: new UntypedFormControl('', []),
    pors_warranty_month: new UntypedFormControl('', []),
    maintenance_booklet: new UntypedFormControl('', [ ]),
    maintenance_invoice: new UntypedFormControl('', [ ]),
    //accidented: new UntypedFormControl('', [ ]),
    original_paint: new UntypedFormControl('', []),
    matching_number: new UntypedFormControl('', [ ]),
    matching_color_paint: new UntypedFormControl('', [ ]),
    //matching_color_interior: new UntypedFormControl('', [Validators.required]),
    //price_for_pors: new UntypedFormControl('', [Validators.required]),
    //deductible_VAT: new UntypedFormControl('', [Validators.required]),
    number_of_owners: new UntypedFormControl('', [Validators.required]),
    //cylinder_capacity: new UntypedFormControl('', [Validators.required]),
    mileage_kilometer: new UntypedFormControl('', [Validators.required]),
    engine_operation_hour: new UntypedFormControl('', []),
    engine_operation_hour_image: new UntypedFormControl('', []),
    //piwi_report_date: new UntypedFormControl('', []),
    
    porte: new UntypedFormControl('', []),
    ailes: new UntypedFormControl('', []),
    capot: new UntypedFormControl('', []),
    toit: new UntypedFormControl('', []),

    porte_ar_gauche: new UntypedFormControl('', []),

    body_porte: new UntypedFormControl('', []),
    body_ailes: new UntypedFormControl('', []),
    body_capot: new UntypedFormControl('', []),
    body_toit: new UntypedFormControl('', []),
    body_hayon: new UntypedFormControl('', []),
    body_ar_dorite: new UntypedFormControl('', []),

    // piwi_report_date1: new UntypedFormControl('', []),
    // piwi_report_image1: new UntypedFormControl('', []),
    // piwi_report_date2: new UntypedFormControl('', []),
    // piwi_report_image2: new UntypedFormControl('', []),
    // piwi_report_date3: new UntypedFormControl('', []),
    // piwi_report_image3: new UntypedFormControl('', []),
    // piwi_report_date4: new UntypedFormControl('', []),
    // piwi_report_image4: new UntypedFormControl('', []),
    // piwi_report_date5: new UntypedFormControl('', []),
    // piwi_report_image5: new UntypedFormControl('', []),
    // piwi_report_date6: new UntypedFormControl('', []),
    // piwi_report_image6: new UntypedFormControl('', []),


    service_date1: new UntypedFormControl('', []),
    service_km1: new UntypedFormControl('', []),
    service_file1: new UntypedFormControl('', []),

    service_date2: new UntypedFormControl('', []),
    service_km2: new UntypedFormControl('', []),
    service_file2: new UntypedFormControl('', []),

    service_date3: new UntypedFormControl('', []),
    service_km3: new UntypedFormControl('', []),
    service_file3: new UntypedFormControl('', []),

    service_date4: new UntypedFormControl('', []),
    service_km4: new UntypedFormControl('', []),
    service_file4: new UntypedFormControl('', []),

    service_date5: new UntypedFormControl('', []),
    service_km5: new UntypedFormControl('', []),
    service_file5: new UntypedFormControl('', []),

    service_date6: new UntypedFormControl('', []),
    service_km6: new UntypedFormControl('', []),
    service_file6: new UntypedFormControl('', []),

    service_date7: new UntypedFormControl('', []),
    service_km7: new UntypedFormControl('', []),
    service_file7: new UntypedFormControl('', []),

    service_date8: new UntypedFormControl('', []),
    service_km8: new UntypedFormControl('', []),
    service_file8: new UntypedFormControl('', []),

    service_date9: new UntypedFormControl('', []),
    service_km9: new UntypedFormControl('', []),
    service_file9: new UntypedFormControl('', []),

    service_date10: new UntypedFormControl('', []),
    service_km10: new UntypedFormControl('', []),
    service_file10: new UntypedFormControl('', []),


    report_piwi_checkbox_1: new UntypedFormControl('', []),
    report_piwi_checkbox_2: new UntypedFormControl('', []),
    report_piwi_checkbox_3: new UntypedFormControl('', []),
  });

  get formSecondStepFun(){
    return this.formSecondStep.controls;
  }

  formThirdStep = new UntypedFormGroup({
    
    //title: new UntypedFormControl('', [Validators.required]),
    description: new UntypedFormControl('', [Validators.required])
  });

  get formThirdStepFun(){
    return this.formThirdStep.controls;
  }

  formBeforeFourthStep = new UntypedFormGroup({
    
    price: new UntypedFormControl('', [Validators.required]),
    //pro_price: new UntypedFormControl('', [])
  });

  get formBeforeFourthStepFun(){
    return this.formBeforeFourthStep.controls;
  }
  formFourthStep = new UntypedFormGroup({
    file: new UntypedFormControl('', [Validators.required]),
    addiotional_photo_7: new UntypedFormControl('', [])

  });
  addiotional_photo_7()
  {
    this.isUpload = false;
    this.maxUploadImage = 10;
    this.additional_photo_check=true;
    this.additional_photo_price = 9.11;
    this.final_plan_price = this.additional_photo_price;
  }
  get formFourthStepFun(){
    return this.formFourthStep.controls;
  }
  formFifthStep = new UntypedFormGroup({
    address: new UntypedFormControl('', [Validators.required]),
    zip_code: new UntypedFormControl('', [Validators.required]),
    city: new UntypedFormControl('', [Validators.required]),
    country: new UntypedFormControl('', [Validators.required]),
  });
  get formFifthStepFun(){
    return this.formFifthStep.controls;
  }
  formSixStep = new UntypedFormGroup({
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', []),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    hideNumber: new UntypedFormControl('', [])
  });
  get formSixStepFun(){
    return this.formSixStep.controls;
  }
  //this.allUserParticularPlan
  
  formSevenStep = new UntypedFormGroup({
    paymentType: new UntypedFormControl('', []),
    ad_id: new UntypedFormControl('', []),
    normalUserAd: new UntypedFormControl('', []),
    normalUserAdTopUrgent: new UntypedFormControl('', []),
    normalUserTopSearch: new UntypedFormControl('', []),
    which_week: new UntypedFormControl('', []),
    user_id: new UntypedFormControl('', []),
    additional_photo_check: new UntypedFormControl(this.additional_photo_check, []),
  });
  get formSevenStepFun(){
    return this.formSevenStep.controls;
  }
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  next(){

    if(this.step==1)
    {
      //console.log("hereeee");
      //this.personal_step = true;
      if (this.abc.invalid)
      {
        this.validateAllFormFields(this.abc); 
        return;
      }else{
        //console.log("here");
        this.titleOfAd = this.abc.value.title;
        let adData = {"category":this.abc.value.category,"ad_name":this.abc.value.title,"user_id":this.user_id,"user_type":this.user_type};
        console.log(this.abc.value.category);

        
        

        this._http.post(this.webStep1CarAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            this.ad_id = this.apiResponse.lastInsertId;
            if(this.abc.value.category == "Pièces Et Accessoires"  || this.abc.value.category == "Pièces et accessoires")
            {
              console.log(this.user_type);
              if(this.user_type == "normal_user")
              {
                this.router.navigate(['/', 'adAccessoriesParticular',this.ad_id]);
              }else{
                this.router.navigate(['/', 'adAccessories',this.ad_id]);
              }
              
            }
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });
        //this.step++;
      }
    }
    else if(this.step==2)
      {
        //this.step++;
        //this.address_step = true;
        this.titleOfAd = this.abc.value.title;
        //console.log("titleOfAd  "+this.titleOfAd);
        if (this.formSecondStep.invalid)
        {  
          this.validateAllFormFields(this.formSecondStep); 
          return;
        }else{
          this.secondStepDisable = true; 
          this.secondStepDisable_loading = true;

          //console.log("here");
          //console.log(this.ad_id);
          //console.log(this.formSecondStep.value.model_id);
          this.formData = new FormData(); 
          this.formData.append('ad_id', this.ad_id);
          this.formData.append('model_id', this.formSecondStep.value.model_id);
          this.formData.append('subModelId', this.formSecondStep.value.subModelId);
          
          this.formData.append('model_variant', this.formSecondStep.value.model_variant);
          this.formData.append('registration_year', this.formSecondStep.value.registration_year);
          this.formData.append('registration_month', this.formSecondStep.value.registration_month);
          this.formData.append('type_of_chassis', this.formSecondStep.value.type_of_chassis);
          this.formData.append('fuel', this.formSecondStep.value.fuel);
          this.formData.append('vehicle_condition', this.formSecondStep.value.vehicle_condition);
          this.formData.append('type_of_gearbox', this.formSecondStep.value.type_of_gearbox);
          this.formData.append('color_interior_name', this.formSecondStep.value.color_interior_name);
          this.formData.append('color_interior', this.formSecondStep.value.color_interior);
          
          this.formData.append('color_exterieur', this.formSecondStep.value.color_exterieur);
          this.formData.append('color_exterieur_name', this.formSecondStep.value.color_exterieur_name);
          //color_exterieur  ,, color_exterieur_name

          this.formData.append('pors_warranty', this.formSecondStep.value.pors_warranty);
          this.formData.append('pors_warranty_month', this.formSecondStep.value.pors_warranty_month);
          this.formData.append('warranty', this.formSecondStep.value.warranty);
          this.formData.append('warranty_month', this.formSecondStep.value.warranty_month);
          this.formData.append('maintenance_booklet', this.formSecondStep.value.maintenance_booklet);
          this.formData.append('maintenance_invoice', this.formSecondStep.value.maintenance_invoice);
          //this.formData.append('accidented', this.formSecondStep.value.accidented);
          this.formData.append('original_paint', this.formSecondStep.value.original_paint);
          this.formData.append('matching_number', this.formSecondStep.value.matching_number);
          this.formData.append('matching_color_paint', this.formSecondStep.value.matching_color_paint);
          this.formData.append('matching_color_interior', this.formSecondStep.value.matching_color_interior);
          this.formData.append('price_for_pors', this.formSecondStep.value.price_for_pors);
          this.formData.append('deductible_VAT', this.formSecondStep.value.deductible_VAT);
          this.formData.append('number_of_owners', this.formSecondStep.value.number_of_owners);
          this.formData.append('cylinder_capacity', this.formSecondStep.value.cylinder_capacity);
          this.formData.append('mileage_kilometer', this.formSecondStep.value.mileage_kilometer);

          this.formData.append('engine_operation_hour', this.formSecondStep.value.engine_operation_hour);
          
          this.formData.append('porte', this.formSecondStep.value.porte);
          this.formData.append('ailes', this.formSecondStep.value.ailes);
          this.formData.append('capot', this.formSecondStep.value.capot);
          this.formData.append('toit', this.formSecondStep.value.toit);
          this.formData.append('body_ar_dorite', this.formSecondStep.value.body_ar_dorite);
          this.formData.append('porte_ar_gauche', this.formSecondStep.value.porte_ar_gauche);
          
          
          this.formData.append('body_porte', this.formSecondStep.value.body_porte);
          this.formData.append('body_ailes', this.formSecondStep.value.body_ailes);
          this.formData.append('body_capot', this.formSecondStep.value.body_capot);
          this.formData.append('body_toit', this.formSecondStep.value.body_toit);
          this.formData.append('body_hayon', this.formSecondStep.value.body_hayon);
          
          // this.formData.append('piwi_report_date1', this.formSecondStep.value.piwi_report_date1);
          // this.formData.append('piwi_report_date2', this.formSecondStep.value.piwi_report_date2);
          // this.formData.append('piwi_report_date3', this.formSecondStep.value.piwi_report_date3);
          // this.formData.append('piwi_report_date4', this.formSecondStep.value.piwi_report_date4);
          // this.formData.append('piwi_report_date5', this.formSecondStep.value.piwi_report_date5);
          // this.formData.append('piwi_report_date6', this.formSecondStep.value.piwi_report_date6);

          
          this.formData.append('service_date1', this.formSecondStep.value.service_date1);
          this.formData.append('service_km1', this.formSecondStep.value.service_km1);
          this.formData.append('service_date2', this.formSecondStep.value.service_date2);
          this.formData.append('service_km2', this.formSecondStep.value.service_km2);
          this.formData.append('service_date3', this.formSecondStep.value.service_date3);
          this.formData.append('service_km3', this.formSecondStep.value.service_km3);
          this.formData.append('service_date4', this.formSecondStep.value.service_date4);
          this.formData.append('service_km4', this.formSecondStep.value.service_km4);
          this.formData.append('service_date5', this.formSecondStep.value.service_date5);
          this.formData.append('service_km5', this.formSecondStep.value.service_km5);
          this.formData.append('service_date6', this.formSecondStep.value.service_date6);
          this.formData.append('service_km6', this.formSecondStep.value.service_km6);

          this.formData.append('service_date7', this.formSecondStep.value.service_date7);
          this.formData.append('service_km7', this.formSecondStep.value.service_km7);

          this.formData.append('service_date8', this.formSecondStep.value.service_date8);
          this.formData.append('service_km8', this.formSecondStep.value.service_km8);

          this.formData.append('service_date9', this.formSecondStep.value.service_date9);
          this.formData.append('service_km9', this.formSecondStep.value.service_km9);

          this.formData.append('service_date10', this.formSecondStep.value.service_date10);
          this.formData.append('service_km10', this.formSecondStep.value.service_km10);
          this.formData.append('optionsArray', this.optionsArray);
          
           

          this.formData.append('report_piwi_checkbox_1', this.formSecondStep.value.report_piwi_checkbox_1);
          this.formData.append('report_piwi_checkbox_2', this.formSecondStep.value.report_piwi_checkbox_2);
          this.formData.append('report_piwi_checkbox_3', this.formSecondStep.value.report_piwi_checkbox_3);

          for (var i = 0; i < this.myPiwiReportFiles.length; i++)
          { 
            this.formData.append("piwi_report_image", this.myPiwiReportFiles[i]);
          }
          this.myServiceReportFiles = [];
          if(this.images_1)
          {
            if(this.images_1.length > 0)
            {
              this.myServiceReportFiles.push(this.images_1[0]);
            }
          }
          if(this.images_2)
          {
            if(this.images_2.length > 0)
            {
              this.myServiceReportFiles.push(this.images_2[0]);
            }
          }
           
          if(this.images_3)
          {
            if(this.images_3.length > 0)
            {
              this.myServiceReportFiles.push(this.images_3[0]);
            }
          }
          if(this.images_4)
          {
            if(this.images_4.length > 0)
            {
              this.myServiceReportFiles.push(this.images_4[0]);
            }
          }
          if(this.images_5)
          {
            if(this.images_5.length > 0)
            {
              this.myServiceReportFiles.push(this.images_5[0]);
            }
          }
          if(this.images_6)
          {
            if(this.images_6.length > 0)
            {
              this.myServiceReportFiles.push(this.images_6[0]);
            }
          }
          if(this.images_7)
          {
            if(this.images_7.length > 0)
            {
              this.myServiceReportFiles.push(this.images_7[0]);
            }
          }
          if(this.images_8)
          {
            if(this.images_8.length > 0)
            {
              this.myServiceReportFiles.push(this.images_8[0]);
            }
          }
          if(this.images_9)
          {
            if(this.images_9.length > 0)
            {
              this.myServiceReportFiles.push(this.images_9[0]);
            }
          }
          if(this.images_10)
          {
            if(this.images_10.length > 0)
            {
              this.myServiceReportFiles.push(this.images_10[0]);
            }
          }
           
          for (var i = 0; i < this.myServiceReportFiles.length; i++)
          { 
            this.formData.append("mileage_maintanance_report_image", this.myServiceReportFiles[i]);
          }
          //images_1
        for (var i = 0; i < this.myFiles_e_o_h_i.length; i++)
        { 
          this.formData.append("engine_operation_hour_image", this.myFiles_e_o_h_i[i]);
        }
        
          this._http.post(this.webStep2CarAdName,this.formData).subscribe((response:any)=>{
            console.log("response of api"+response);
            this.apiResponse = response;

            this.secondStepDisable = false; 
            this.secondStepDisable_loading = false;

            //console.log(this.apiResponse);
            if(this.apiResponse.error == false)
            {
              

              //console.log(this.apiResponse.lastInsertId);
              //this.ad_id = this.apiResponse.lastInsertId;
              //console.log("ad_id "+this.ad_id);
              this.step++;
            }
          });
          //this.step++;
        }
        
      }
    else if(this.step==3)
    {
      this.priceForPors = this.formSecondStep.value.price_for_pors;
      //console.log("priceForPors "+this.priceForPors);
      //console.log("myPiwiReportFiles "+ JSON.stringify(this.myPiwiReportFiles));
      // this.formData = new FormData(); 
      // for (var i = 0; i < this.myPiwiReportFiles.length; i++)
      // { 
      //   this.formData.append("file", this.myPiwiReportFiles[i]);
      // } 
      // this._http.post(this.addAdImageTesting,this.formData).subscribe((response:any)=>{
      //   console.log("response of api"+response);
      // });
      //this.address_step = true;
      if (this.formThirdStep.invalid)
      {
        this.validateAllFormFields(this.formThirdStep);  return;
      }else{
        let adData = {"description":this.formThirdStep.value.description,"ad_id":this.ad_id};
        this._http.post(this.webStep3CarAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });
        //this.step++;

      }
      //this.step++;

    }
    else if(this.step==4)
    {
      if (this.formBeforeFourthStep.invalid)
      {
        this.validateAllFormFields(this.formBeforeFourthStep);  return;
      }else{
        //webStepBefore4CarAdName
        //formBeforeFourthStep
        let adData = {"price":this.formBeforeFourthStep.value.price,"ad_id":this.ad_id};
        this._http.post(this.webStepBefore4CarAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });

      }
      //this.step++;
    }
    else if(this.step==5)
    {
      this.formData = new FormData();
      this.formData.append('ad_id', this.ad_id);
      for (var i = 0; i < this.myFiles.length; i++)
      { 
        this.formData.append("exterior_image", this.myFiles[i]);
      }
      
      this._http.post(this.webStep4CarAdName,this.formData).subscribe((response:any)=>{
        this.apiResponse = response;
        //console.log("formData "+this.formData);
        if(this.apiResponse.error == false)
        {
          //console.log(this.apiResponse.lastInsertId);
          //this.ad_id = this.apiResponse.lastInsertId;
          //console.log("ad_id "+this.ad_id);
          this.step++;
        }
      });
      //this.step++;
    }
    else if(this.step==6)
    {
      //this.address_step = true;
      if (this.formFifthStep.invalid)
      {
        this.validateAllFormFields(this.formFifthStep);  return;
      }else{
        
        this.queryParam = {"ad_id":this.ad_id};
        this._http.post(this.getCarAdForImagePrice,this.queryParam).subscribe((response:any)=>{
          //console.log("response of api"+response);
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            this.allAddPriceImage = this.apiResponse.record;
            console.log("here 674 "+this.allAddPriceImage);
          }
           
        });


        

        let adData = {"address": this.userAddress,"latitude":this.userLatitude,"longitude":this.userLongitude,"ad_id":this.ad_id,"zip_code":this.formFifthStep.value.zip_code,"city":this.formFifthStep.value.city,"country":this.formFifthStep.value.country};
        this._http.post(this.webStep5CarAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
        });


      }
      //this.step++;
    }
    else if(this.step==7)
    {
      //this.address_step = true;  formSixStep
      if (this.formSixStep.invalid)
      {
        this.validateAllFormFields(this.formSixStep);  return;
      }else{
        let adData = {'mobileNumber':this.formSixStep.value.mobileNumber,'hideNumber':this.formSixStep.value.hideNumber,"ad_id":this.ad_id};
        this._http.post(this.webStep6CarAdName,adData).subscribe((response:any)=>{
          this.apiResponse_New = response;
          //console.log("formData "+this.formData);
          if(this.apiResponse_New.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
            //setTimeout(() => {
              //window.location.reload();
              //window.location.href = this.base_url+"adSuccess";
              
            //}, 2000);

          }
        });
      }
      //this.step++;
    }else if(this.step==8)
    {
      if (this.formSevenStep.invalid)
      {
        this.validateAllFormFields(this.formSevenStep);  return;
      }else{

        console.log("this.additional_photo_check ", this.additional_photo_check);
        //console.log(this.formSevenStep.value);
        this.formSevenStep.value.additional_photo_check = this.additional_photo_check;

        //console.log(this.formSevenStep.value);
        this._http.post(this.webPaymentStepCarAdName,this.formSevenStep.value).subscribe((response:any)=>{
          this.apiResponseLast = response;
          //console.log("formData "+this.formData);
          if(this.apiResponseLast.error == false)
          {
            console.log(this.apiResponseLast);
            //return false;

            if(this.apiResponseLast.price == 0)
            {
              var redirect_url = this.base_url+"adSuccess"
            }else{
              var redirect_url = this.base_url_node_only+"api/webCarAdPayment?"+"ad_id="+this.ad_id+"&user_id="+this.user_id+"&basic_main_plan_id="+this.formSevenStep.value.normalUserAd+"&top_urgent_plan_id="+this.formSevenStep.value.normalUserAdTopUrgent+"&top_search_plan_id="+this.formSevenStep.value.normalUserTopSearch+"&paymentType="+this.formSevenStep.value.paymentType+"&price="+this.apiResponseLast.price+"&additional_photo_check="+this.additional_photo_check;
            }
            
            //return false;
            //console.log(this.apiResponseLast.lastInsertId);
            //this.ad_id = this.apiResponseLast.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            //this.step++;
            console.log(redirect_url);
            //return false;
            setTimeout(() => {
             window.location.href = redirect_url;
            }, 2000);

          }
        });
      }
    }
  }

  previous()
  {
    this.step--
    

    if(this.step==2)
    {
       console.log("step 2 only");
       this.queryParam = { "parent_id":"62e3d257ec6f493144a2533a"};
       this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
         this.apiResponse = response;
         if(this.apiResponse.error == false)
         {
           this.allChasisType = this.apiResponse.record;
           console.log("ffffffffffffffffffff--------------------------fffffffffffffffffffffff");
         }
       });
    }


    

    /*if(this.step==1){
      this.address_step = false;
    }
    if(this.step==2){
      this.education_step = false;
    }*/
   
  }
  // files: File[] = [];
  submit(){
    //console.log("hereeeeeeeee");
    if(this.step==3)
    {
      //console.log("iffffff");
      this.education_step = true;
      //if (this.educationalDetails.invalid) { return }
      alert("Well done!!")
    }else{
      //console.log("elseeeeeee");
    }

  }
  myFiles_e_o_h_i:string [] = [];
    onFileChange_e_o_h_i(event:any)
    {
      if (event.target.files && event.target.files[0])
      {
        this.myFiles_e_o_h_i.push(event.target.files[0]);
      }
    }
  images : string[] = [];
  

  myFiles:string [] = [];
  onFileChangeNew(event:any)
  {
    //console.log("maxUploadImage ", this.maxUploadImage);
    //console.log("event.target.files.length ", event.target.files.length);
    //console.log("this.myFiles.length ", this.myFiles.length);
    this.allUploadedFile = this.myFiles.length + event.target.files.length;
    console.log("this.allUploadedFile ",this.allUploadedFile);
    //isUpload
    //this.myFiles = [];
    if (event.target.files && event.target.files[0])
    {
      //console.log("this.myFiles.length ", this.myFiles.length);
      //
      // if(numOfFiles + files.length > 4){
      //   alert("You can only upload at most 4 files!");
      //   return;
      // }
      // numOfFiles += files.length;

      if(this.maxUploadImage >= this.allUploadedFile)
      {
        this.isUpload = false;
        
          this.isUpload = false;
          var filesAmount = event.target.files.length;
          for (let i = 0; i < filesAmount; i++)
          {
            var reader = new FileReader();
            reader.onload = (event:any) => {
              // Push Base64 string
              this.images.push(event.target.result); 
              //this.patchValues();
            }
            reader.readAsDataURL(event.target.files[i]);

            this.myFiles.push(event.target.files[i]);
            
          }
      }else{
        this.isUpload = true;
      }
        
       
      
    }

    

  }
  removeImage(url:any,imgNumber:any){
    //console.log(this.images,url);
    //console.log(this.myFiles);
    this.images = this.images.filter(img => (img != url));
    //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
    ///this.patchValues();
    //console.log(imgNumber);
    this.myFiles.splice(imgNumber,1);
    
    this.allUploadedFile--;

  }

  images_for_service_1 : string[] = []; images_1 :any[] = []; extension_1:string="";
  images_for_service_2 : string[] = []; images_2 :any[] = []; extension_2:string="";
  images_for_service_3 : string[] = []; images_3 :any[] = []; extension_3:string="";
  images_for_service_4 : string[] = []; images_4 :any[] = []; extension_4:string="";
  images_for_service_5 : string[] = []; images_5 :any[] = []; extension_5:string="";
  images_for_service_6 : string[] = []; images_6 :any[] = []; extension_6:string="";
  images_for_service_7 : string[] = []; images_7 :any[] = []; extension_7:string="";
  images_for_service_8 : string[] = []; images_8 :any[] = []; extension_8:string="";
  images_for_service_9 : string[] = []; images_9 :any[] = []; extension_9:string="";
  images_for_service_10 : string[] = []; images_10 :any[] = []; extension_10:string="";

  onFileChangeTwo(event:any)
  {
    this.images_for_service_1 = []; this.images_1 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_1.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_1.push(event.target.files[i]);
      this.extension_1 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_1 = this.extension_1.toLowerCase();
      console.log("this.type extension_1  ", this.extension_1);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_1 ", this.images_1);
  }
  onFileChangeTwo_2(event:any)
  {
    this.images_for_service_2 = []; this.images_2 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_2.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_2.push(event.target.files[i]);
      this.extension_2 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_2 = this.extension_2.toLowerCase();
      console.log("this.type extension_2  ", this.extension_2);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_2 ", this.images_2);
  }
  onFileChangeTwo_3(event:any)
  {
    this.images_for_service_3 = []; this.images_3 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_3.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_3.push(event.target.files[i]);
      this.extension_3 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_3 = this.extension_3.toLowerCase();
      console.log("this.type extension_3  ", this.extension_3);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_3 ", this.images_3);
  }
  onFileChangeTwo_4(event:any)
  {
    this.images_for_service_4 = []; this.images_4 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_4.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_4.push(event.target.files[i]);
      this.extension_4 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_4 = this.extension_4.toLowerCase();
      console.log("this.type extension_4  ", this.extension_4);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_4 ", this.images_4);
  }
  onFileChangeTwo_5(event:any)
  {
    this.images_for_service_5 = []; this.images_5 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_5.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_5.push(event.target.files[i]);
      this.extension_5 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_5 = this.extension_5.toLowerCase();
      console.log("this.type extension_5  ", this.extension_5);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_5 ", this.images_5);
  }
  onFileChangeTwo_6(event:any)
  {
    this.images_for_service_6 = []; this.images_6 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_6.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_6.push(event.target.files[i]);
      this.extension_6 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_6 = this.extension_6.toLowerCase();
      console.log("this.type extension_6  ", this.extension_6);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_6 ", this.images_6);
  }
  onFileChangeTwo_7(event:any)
  {
    this.images_for_service_7 = []; this.images_7 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_7.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_7.push(event.target.files[i]);
      this.extension_7 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_7 = this.extension_7.toLowerCase();
      console.log("this.type extension_7  ", this.extension_7);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_7 ", this.images_7);
  }
  onFileChangeTwo_8(event:any)
  {
    this.images_for_service_8 = []; this.images_8 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_8.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_8.push(event.target.files[i]);
      this.extension_8 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_8 = this.extension_8.toLowerCase();
      console.log("this.type extension_8  ", this.extension_8);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_8 ", this.images_8);
  }
  onFileChangeTwo_9(event:any)
  {
    this.images_for_service_9 = []; this.images_9 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_9.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_9.push(event.target.files[i]);
      this.extension_9 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_9 = this.extension_9.toLowerCase();
      console.log("this.type extension_9  ", this.extension_9);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_9 ", this.images_9);
  }
  onFileChangeTwo_10(event:any)
  {
    this.images_for_service_10 = []; this.images_10 = [];
    for (var i = 0; i < event.target.files.length; i++)
    {
      var reader = new FileReader();
      reader.onload = (event:any) => {
        this.images_for_service_10.push(event.target.result);
      }
      reader.readAsDataURL(event.target.files[i]);
      this.images_10.push(event.target.files[i]);
      this.extension_10 = event.target.files[i].name.split(/[. ]+/).pop();
      this.extension_10 = this.extension_10.toLowerCase();
      console.log("this.type extension_10  ", this.extension_10);
    }
    //console.log("this.myServiceReportFiles ", this.myServiceReportFiles);
    console.log("this.images_10 ", this.images_10);
  }
  onItemChange(event:any)
  {
     this.first_plan_price = 0;
     this.first_plan_price = event;

     this.final_plan_price = this.first_plan_price +this.second_plan_price+this.third_plan_price;
     let taxx = this.final_plan_price * this.tax_percent / 100;
      
     this.final_plan_price = taxx + this.final_plan_price;
     this.final_plan_price = this.final_plan_price + this.additional_photo_price;
     this.final_plan_price.toFixed(2);
     // console.log("additional_photo_check " , this.additional_photo_check);
    // if(this.additional_photo_check == true)
    // {

    // }
    //console.log(event);
  }
  onItemChangeSec(event:any)
  {
    this.second_plan_price = 0;
     this.second_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price+this.third_plan_price;
     let taxx = this.final_plan_price * this.tax_percent / 100;
      
     this.final_plan_price = taxx + this.final_plan_price;
     this.final_plan_price = this.final_plan_price + this.additional_photo_price;
     this.final_plan_price.toFixed(2);
    //console.log(event);
  }
  onItemChangeThird(event:any)
  {
    this.third_plan_price = 0;
     this.third_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price+this.third_plan_price;
     let taxx = this.final_plan_price * this.tax_percent / 100;
      
     this.final_plan_price = taxx + this.final_plan_price;
     this.final_plan_price = this.final_plan_price + this.additional_photo_price;
     this.final_plan_price.toFixed(2);
    //console.log(event);
  }

  mapOptions: google.maps.MapOptions = {
    center: { lat: this.centerLatitude, lng: this.centerLongitude },
    zoom : 4
  }
  marker = {
    position: { lat: this.centerLatitude, lng: this.centerLongitude },
  }

}
