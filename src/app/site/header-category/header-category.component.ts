import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-header-category',
  templateUrl: './header-category.component.html',
  styleUrls: ['./header-category.component.css']
})
export class HeaderCategoryComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;webGetSubModel:any;allBanner:any;allModel:any;
  webGetModel:any;getCategory:any;allCategoryList:any;allSubModel:any;
  category1:any;category2:any;
  category1Heading:any;category2Heading:any;
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.webGetSubModel = this.base_url_node+"webGetSubModel";
    this.webGetModel = this.base_url_node+"webGetSubModel2";
    this.getCategory = this.base_url_node+"getCategoryOnAdPage";
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
  }

  ngOnInit(): void {

    this._http.get(this.getCategory,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCategoryList = this.apiResponse.record;
        //console.log(this.allCategoryList);
        if(this.allCategoryList.length > 0)
        {
          this.category1 = this.allCategoryList[0].category;
          this.category1Heading = this.allCategoryList[0].first_heading;
        }
        if(this.allCategoryList.length > 1)
        {
          this.category2 = this.allCategoryList[1].category;
          this.category2Heading = this.allCategoryList[1].first_heading;
        }
      }
       
    });


    //Porsche Voitures
    let objVal = {attribute_type:"Porsche Voitures"};
    this._http.post(this.webGetModel,objVal).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allModel = this.apiResponse.record;
        console.log(this.allModel,  "my modal");
      }
    });

    let objVal2 = {attribute_type:"Pièces et accessoires"};
    this._http.post(this.webGetSubModel,objVal2).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allSubModel = this.apiResponse.record;
        console.log(this.allSubModel, "my test");
      }
    });

  }

}
