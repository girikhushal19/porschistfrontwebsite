import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdAccessoriesPostParticularComponent } from './ad-accessories-post-particular.component';

describe('AdAccessoriesPostParticularComponent', () => {
  let component: AdAccessoriesPostParticularComponent;
  let fixture: ComponentFixture<AdAccessoriesPostParticularComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdAccessoriesPostParticularComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdAccessoriesPostParticularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
