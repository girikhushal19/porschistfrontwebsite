import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdNormalUserTopSearchComponent } from './ad-normal-user-top-search.component';

describe('AdNormalUserTopSearchComponent', () => {
  let component: AdNormalUserTopSearchComponent;
  let fixture: ComponentFixture<AdNormalUserTopSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdNormalUserTopSearchComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdNormalUserTopSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
