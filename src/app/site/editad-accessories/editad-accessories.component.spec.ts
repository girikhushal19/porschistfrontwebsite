import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditadAccessoriesComponent } from './editad-accessories.component';

describe('EditadAccessoriesComponent', () => {
  let component: EditadAccessoriesComponent;
  let fixture: ComponentFixture<EditadAccessoriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditadAccessoriesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditadAccessoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
