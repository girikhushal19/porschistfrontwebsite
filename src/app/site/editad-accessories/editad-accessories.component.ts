import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-editad-accessories',
  templateUrl: './editad-accessories.component.html',
  styleUrls: ['./editad-accessories.component.css']
})
export class EditadAccessoriesComponent implements OnInit {

 
  userAddress: string = '';webGetUserProfile:any;
  userLatitude: string = '';formDataForMultiFile:any;
  userLongitude: string = '';priceForPors:any;allSubModelList:any;allWebSubModel:any;
  base_url = "";base_url_node = "";ad_id:any;webStep3CarAdName:any;
  addModelsSubmit:any;getSubModel:any;allCategoryList:any;webStep5AccessoriesAdNameNormal:any;
  webStep4AccessoriesAdName:any;webStep6AccessoriesAdName:any;
  token:any;user_type:any;apiResponse:any;apiResponseLast:any;formValue:any;record:any;totalPageNumber:any; apiResponse_new:any;
  allModel:any;allModelList:any;queryParam:any;numbers:any;allModelCount:any;
  base_url_node_only:any;allCarburant:any;allBoiteDeVitesse:any;webStep1CarAdName:any;
  updateModelStatusApi:any;getSubAttribute:any;getColor:any;allColor:any;
  queryParamNew:any;updateModelStatus:any;numofpage_0:any;apiStringify:any;getCarCategory:any;allCarCategory:any;allYear:any;allEtatDuVehicle:any;titleOfAd:any;
  registration_year:any;allChasisType:any;formData:any;webStep2CarAdName:any;
  personalDetails!: UntypedFormGroup;addAdImageTesting:any;user_email:any;
  addressDetails!: UntypedFormGroup;user_id:any;lastName:any;user_mobileNumber:any;
  educationalDetails!: UntypedFormGroup;firstName:any;
  personal_step = false;selectedDevice:any;webStep2AccessoriesAdName:any;webStep3AccessoriesAdName:any;webStep6BeforeAccessoriesAdName:any;
  address_step = false;webStep7AccessoriesAdName:any;
  education_step = false;webGetAdTitle:any;
  
  step = 1;
  
  webGetUserAccessoriesParticularPlan:any;webGetUserAccessoriesParticularPlanTopUrgent:any;webPaymentAccessoriesParticular:any;allUserParticularPlan:any;allUserParticularPlanTopUrgent:any;
  model_id_on_change:any;
  first_plan_price:number;second_plan_price:number;third_plan_price:number;final_plan_price:number;
  totalImage:number;
  maxUploadImage:number = 3; isUpload:boolean=false; allUploadedFile:number=0;
  additional_photo_check:Boolean=false; additional_photo_price:number=0;
  tempArr: any = { "brands": [] };
  myPiwiReportFiles:string [] = [];
  myServiceReportFiles:string [] = [];
  webGetUserParticularPlanTopSearchListAccess:any;
  centerLatitude = 47.034577;
  centerLongitude = 1.156914;
  getCarAdForImagePrice:any;allAddPriceImage:any; apiResponseStepFive:any;
  allUserParticularPlanTopSearch:any;uploadImgLength:number=0;
  edit_id:any;old_title:any;old_modelId:any;old_model_variant:any;old_type_of_chassis:any;old_fuel:any;old_vehicle_condition:any;old_type_of_gearbox:any;old_colorId:any;old_registration_year:any;old_warranty:any;old_warranty_month:any;old_maintenance_booklet:any;old_maintenance_invoice:any;old_accidented:any;old_original_paint:any;old_matching_number:any;old_matching_color_paint :any;old_matching_color_interior:any;
  old_price_for_pors:any;webStep1CarEditAdName:any;
  old_price:any;old_location:any;webRemoveAccessOldImage:any;
  old_pro_price:any;old_shipping_price:any;
  old_shipping_price_1:number=0;old_shipping_price_2:number=0;old_shipping_price_3:number=0;
  old_shipping_price_4:number=0;
  old_shipping_type_1:boolean=false;
  old_shipping_type_4_text:string="";
  old_delivery_price:any;old_vat_tax:any;old_deductible_VAT:any;old_number_of_owners:any;old_cylinder_capacity:any;old_mileage_kilometer:any;old_description:any;old_address:any;old_contact_number:any;old_engine_operation_hour:any;
  old_subModelId:any;old_type_de_piece:any;old_state:any;old_OEM:any;
  webGetSingleAd:any;singleAd:any;
 

  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService,private router: Router)
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
     
    // console.log(this.token);
    // console.log(this.user_id);
    // console.log(this.user_type);

    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }
    this.edit_id = this.ad_id = this.actRoute.snapshot.params['id'];

    this.webGetAdTitle = this.base_url_node+"webGetAdTitle";
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.allModel = this.base_url_node+"getModel";
    this.getSubModel = this.base_url_node+"getSubModel";
    this.registration_year = this.base_url_node+"registration_year";
    this.allWebSubModel = this.base_url_node+"allWebSubModel";
    this.getSubAttribute = this.base_url_node+"getSubAttribute";
    this.webStep2AccessoriesAdName = this.base_url_node+"webStep2AccessoriesAdName";
    this.webStep3AccessoriesAdName = this.base_url_node+"webStep3AccessoriesAdName";
    this.webStep4AccessoriesAdName = this.base_url_node+"webStep4AccessoriesAdName";
    this.webStep5AccessoriesAdNameNormal = this.base_url_node+"webStep5AccessoriesAdNameNormal";
    this.webStep6AccessoriesAdName = this.base_url_node+"webStep6AccessoriesAdName";
    this.webStep7AccessoriesAdName = this.base_url_node+"webStep7AccessoriesAdName";
    this.webStep6BeforeAccessoriesAdName = this.base_url_node+"webStep6BeforeAccessoriesAdName";

    this.webGetUserAccessoriesParticularPlan = this.base_url_node+"webGetUserAccessoriesParticularPlan";
    this.webStep1CarEditAdName = this.base_url_node+"webStep1CarEditAdName";

    this.webGetSingleAd =  this.base_url_node+"webGetSingleAd";


    this.webGetUserAccessoriesParticularPlanTopUrgent = this.base_url_node+"webGetUserParticularPlanTopUrgent";
    this.webPaymentAccessoriesParticular = this.base_url_node+"webPaymentAccessoriesParticular";

    this.webGetUserParticularPlanTopSearchListAccess = this.base_url_node+"webGetUserParticularPlanTopSearchListAccess";

    this.getCarAdForImagePrice = this.base_url_node+"getCarAdForImagePrice";

    this.first_plan_price = 0;this.second_plan_price = 0;this.third_plan_price = 0;
    this.final_plan_price = 0;this.totalImage = 0;this.uploadImgLength = 0;
  }

  ngOnInit(): void {

    this.queryParam = {ad_id:this.edit_id};
    this._http.post(this.webGetSingleAd,this.queryParam).subscribe((response:any)=>{
      console.log("response of api"+response);
      //return;
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
         
        //old_modelId:any;old_model_variant:any;old_type_of_chassis:any;old_fuel:any;old_vehicle_condition:any;old_type_of_gearbox:any;old_colorId:any;old_registration_year:any;old_warranty:any;old_warranty_month:any;old_price_for_pors:any;old_price:any;old_pro_price:any;old_delivery_price:any;old_vat_tax:any;old_deductible_VAT:any;old_number_of_owners:any;old_cylinder_capacity:any;old_mileage_kilometer:any;old_description:any;old_address:any;old_contact_number:any;old_engine_operation_hour:any;

        this.abc = new UntypedFormGroup({
          ad_id: new UntypedFormControl( this.apiResponse.record.ad_id, []),
          model_id: new UntypedFormControl(this.apiResponse.record.model_id, [Validators.required]),
          subModelId: new UntypedFormControl(this.apiResponse.record.subModelId, [Validators.required]),
          registration_year: new UntypedFormControl(this.apiResponse.record.registration_year, [Validators.required]),
          type_de_piece: new UntypedFormControl(this.apiResponse.record.type_de_piece, [Validators.required]),
          state: new UntypedFormControl(this.apiResponse.record.state, [Validators.required]),
          OEM: new UntypedFormControl(this.apiResponse.record.OEM, [Validators.required]),
        });


        this.singleAd = this.apiResponse.record;
        console.log("this.singleAd ", this.singleAd);


        this.old_title = this.singleAd.ad_name;
        this.old_modelId = this.singleAd.modelId;
        this.old_subModelId = this.singleAd.subModelId;
        // this.old_model_variant = this.singleAd.model_variant;
        // this.old_type_of_chassis = this.singleAd.type_of_chassis;
        // this.old_fuel = this.singleAd.fuel;
        // this.old_vehicle_condition = this.singleAd.vehicle_condition;
        // this.old_type_of_gearbox = this.singleAd.type_of_gearbox;
        // this.old_colorId = this.singleAd.colorId;
         this.old_registration_year = this.singleAd.registration_year;
        // this.old_number_of_owners = this.singleAd.number_of_owners;
        // this.old_cylinder_capacity = this.singleAd.cylinder_capacity;
        // this.old_mileage_kilometer = this.singleAd.mileage_kilometer;
        // this.old_engine_operation_hour = this.singleAd.engine_operation_hour;
        // this.old_warranty = this.singleAd.warranty;
        
        this.old_shipping_price =  this.singleAd.shipping_price;

        //shipping_price_1 ,  shipping_price_2 ,  shipping_price_4


          this.old_shipping_type_4_text = this.singleAd.shipping_type_4_text;
          //console.log("this.old_shipping_price ",this.old_shipping_price);
          if(this.singleAd.shipping_price_1 >= 0)
          {
            this.old_shipping_price_1 = this.singleAd.shipping_price_1;
          }
          if(this.singleAd.shipping_price_2 >= 0)
          {
            this.old_shipping_price_2 = this.singleAd.shipping_price_2;
          }
          if(this.singleAd.shipping_price_4 >= 0)
          {
            this.old_shipping_price_4 = this.singleAd.shipping_price_4;
          }
          
        
        this.old_price = this.singleAd.price;
        this.old_pro_price = this.singleAd.pro_price;
        this.old_delivery_price = this.singleAd.delivery_price;
        this.old_description = this.singleAd.description;
        this.old_address = this.singleAd.address;
        console.log("this.old_address ", this.old_address);
        this.old_contact_number = this.singleAd.contact_number;
        this.old_type_de_piece = this.singleAd.type_de_piece;
        this.old_state = this.singleAd.state;
        this.old_OEM = this.singleAd.OEM;
        this.old_location = this.singleAd.location;
        //this.old_exterior_image = this.singleAd.accessoires_image;
        if(this.old_location.coordinates)
        {
          this.userLatitude = this.old_location.coordinates[0];
          this.userLongitude = this.old_location.coordinates[1];

          this.centerLatitude = parseFloat(this.old_location.coordinates[0]);
            this.centerLongitude = parseFloat(this.old_location.coordinates[1]);
            //console.log(this.centerLatitude);
           // console.log(this.centerLongitude);
            this.marker = {
              position: { lat: this.centerLatitude, lng: this.centerLongitude },
            }

          //console.log(this.userLatitude);
          //console.log(this.userLongitude);
        }
      }
      
    });

    this._http.get(this.webGetUserParticularPlanTopSearchListAccess,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlanTopSearch = this.apiResponse.record;
        console.log("this.allUserParticularPlanTopSearch");
        console.log(this.allUserParticularPlanTopSearch);
      }
    });


    this.queryParam = {user_type:"normal_user"};
    this._http.post(this.webGetUserAccessoriesParticularPlan,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlan = this.apiResponse.record;
      }
    });
    this.queryParam = {user_type:"normal_user"};
    let cc = {"plan_type":"Accessoires"};
    this._http.post(this.webGetUserAccessoriesParticularPlanTopUrgent,cc).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlanTopUrgent = this.apiResponse.record;
      }
    });


    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
        this.firstName = this.apiResponse.userRecord.firstName;
        this.lastName = this.apiResponse.userRecord.lastName;
        this.user_email = this.apiResponse.userRecord.email;
        this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
      }
       
    });
    //titleOfAd
    this.queryParam = {"ad_id":this.ad_id};
    this._http.post(this.webGetAdTitle,this.queryParam).subscribe((response:any)=>{
      //console.log("image response "+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log(this.apiResponse.record.accessoires_image);
        this.titleOfAd = this.apiResponse.record.ad_name;
        //this.allYear = this.apiResponse.record;
        this.totalImage = this.apiResponse.record.accessoires_image.length;
        if(this.totalImage > 1)
        {
          this.totalImage = this.totalImage - 1;
        }
        console.log("totalImage "+this.totalImage);
      }
    });

    this._http.get(this.registration_year,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allYear = this.apiResponse.record;
      }
    });

    this.queryParam = {attribute_type:"Pièces et accessoires"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allModelList = this.apiResponse.record;
      }
    });

    this.queryParam = { "parent_id":"62e4f5a35ed7e4f294719634"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allChasisType = this.apiResponse.record;
        //console.log(this.allModelList);
      }
    });

   
    
  }

  abcd = new UntypedFormGroup({
    
    title: new UntypedFormControl('', [Validators.required]),
    //category: new UntypedFormControl('', [Validators.required])
  });
  get abccd(){
    return this.abcd.controls;
  }

  abc = new UntypedFormGroup({
    ad_id: new UntypedFormControl('', []),
    model_id: new UntypedFormControl('', [Validators.required]),
    subModelId: new UntypedFormControl('', [Validators.required]),
    registration_year: new UntypedFormControl('', [Validators.required]),
    //registration_month: new UntypedFormControl('', [Validators.required]),
    type_de_piece: new UntypedFormControl('', [Validators.required]), 
    state: new UntypedFormControl('', [Validators.required]),
    OEM: new UntypedFormControl('', [Validators.required]),
  });
  get abcc(){
    return this.abc.controls;
  }

  
  formSecondStep = new UntypedFormGroup({
    
  });
  get formSecondStepFun(){
    return this.formSecondStep.controls;
  }

  formThirdStep = new UntypedFormGroup({
    //title: new UntypedFormControl('', [Validators.required]),
    description: new UntypedFormControl('', [Validators.required])
  });

  get formThirdStepFun(){
    return this.formThirdStep.controls;
  }
  formBeforeFourthStep = new UntypedFormGroup({
    
    price: new UntypedFormControl('', [Validators.required]),
    //pro_price: new UntypedFormControl('', [])
  });

  get formBeforeFourthStepFun(){
    return this.formBeforeFourthStep.controls;
  }
  formFourthStep = new UntypedFormGroup({
    file: new UntypedFormControl('', [Validators.required]),
    addiotional_photo_7: new UntypedFormControl('', [])
  });
  addiotional_photo_7()
  {
    this.isUpload = false;
    this.maxUploadImage = 10;
    this.additional_photo_check=true;
    this.additional_photo_price = 9.11;
    this.final_plan_price = this.additional_photo_price;
  }
  get formFourthStepFun(){
    return this.formFourthStep.controls;
  }
  
  formFifthStep = new UntypedFormGroup({
    address: new UntypedFormControl('', [Validators.required])
  });
  get formFifthStepFun(){
    return this.formFifthStep.controls;
  }
  formBeforeFifthStep = new UntypedFormGroup({
    shipping_type_1: new UntypedFormControl('', []),
    shipping_type_2: new UntypedFormControl('', []),
    shipping_type_3: new UntypedFormControl('', []),
    shipping_type_4: new UntypedFormControl('', []),
    shipping_type_5: new UntypedFormControl('', []),

    shipping_type_1_text: new UntypedFormControl('Livraison standard', []),
    shipping_type_2_text: new UntypedFormControl('Livraison express', []),
    shipping_type_3_text: new UntypedFormControl('Livraison gratuite', []),
    shipping_type_4_text: new UntypedFormControl('', []),
    shipping_type_5_text: new UntypedFormControl('Pas de livraison', []),

    shipping_price_1: new UntypedFormControl('', []),
    shipping_price_2: new UntypedFormControl('', []),
    //shipping_price_3: new UntypedFormControl('', []),
    shipping_price_4: new UntypedFormControl('', []),
    shipping_price_5: new UntypedFormControl('0', [])
  });
  get formBeforeFifthStepFun(){
    return this.formBeforeFifthStep.controls;
  }

  formSixStep = new UntypedFormGroup({
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', []),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    hideNumber: new UntypedFormControl('', [])
  });
  get formSixStepFun(){
    return this.formSixStep.controls;
  }
  //formSevenStep
  formSevenStep = new UntypedFormGroup({
    paymentType: new UntypedFormControl('', []),
    ad_id: new UntypedFormControl('', []),
    normalUserAd: new UntypedFormControl('', []),
    normalUserAdTopUrgent: new UntypedFormControl('', []),
    normalUserAdTopSearch: new UntypedFormControl('', []),
    which_week: new UntypedFormControl('', []),
    user_id: new UntypedFormControl('', []),
    additional_photo_check: new UntypedFormControl(this.additional_photo_check, []),
  });
  get formSevenStepFun(){
    return this.formSevenStep.controls;
  }
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  onChangeModel(event:any){
    this.model_id_on_change=event;
    /*console.log("hello");
    console.log(this.selectedDeviceR);
    console.log("category");
    console.log(this.form.value.category);*/
    //allSubCatListList
    this.queryParam = { "parent_id":this.model_id_on_change  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      console.log(this.model_id_on_change);
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allSubModelList = this.apiResponse.record;
      }
    });
  }
  next()
  {
    if(this.step==1)
    {
      console.log("hereeee 451");
      //this.personal_step = true;
      //if (this.abcd.invalid)
      if (this.abcd.invalid)
      {
        this.validateAllFormFields(this.abcd); 
        return;
      }else{
        console.log("here 459");
        this.titleOfAd = this.abcd.value.title;
        let adData = {"ad_name":this.abcd.value.title,"ad_id":this.ad_id};
        //console.log(this.abcd.value.category);
        this._http.post(this.webStep1CarEditAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            this.ad_id = this.apiResponse.lastInsertId;
            // if(this.abcd.value.category == "Pièces et accessoires")
            // {
            //   this.router.navigate(['/', 'adAccessories',this.ad_id]);
            // }
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });       
      }
    }

    else if(this.step==2)
    {
      //console.log("hereeee");
      //this.personal_step = true;
      if (this.abc.invalid)
      {
        this.validateAllFormFields(this.abc); 
        return;
      }else{
        //console.log("here");
        this._http.post(this.webStep2AccessoriesAdName,this.abc.value).subscribe((response:any)=>{
          this.apiResponse = response;
          console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });        
      }
    }
    else if(this.step==3)
    {
      if (this.formThirdStep.invalid)
      {
        this.validateAllFormFields(this.formThirdStep);  return;
      }else{
        let adData = {"description":this.formThirdStep.value.description,"ad_id":this.ad_id};
        this._http.post(this.webStep3AccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });
        //this.step++;

      }
    }
    else if(this.step==4)
    {
      if (this.formBeforeFourthStep.invalid)
      {
        this.validateAllFormFields(this.formBeforeFourthStep);  return;
      }else{
        //webStep4AccessoriesAdName
        //formBeforeFourthStep
        let adData = {"price":this.formBeforeFourthStep.value.price,"pro_price":this.formBeforeFourthStep.value.pro_price,"ad_id":this.ad_id};
        this._http.post(this.webStep4AccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          //console.log(this.apiResponse);
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
          
        });

      }
      //this.step++;
    }
    else if(this.step==5)
    {
      console.log("this.formBeforeFifthStep.value.shipping_type_1 ", this.formBeforeFifthStep.value.shipping_type_1);
      if (this.formBeforeFifthStep.invalid)
      {
        console.log("form errror");
        this.validateAllFormFields(this.formBeforeFifthStep);  return;
      }else{
        //formBeforeFifthStep 
        //"shipping_price_3":this.formBeforeFifthStep.value.shipping_price_3,

        let adData = {"shipping_price_1":this.formBeforeFifthStep.value.shipping_price_1,
        "shipping_price_2":this.formBeforeFifthStep.value.shipping_price_2,
        "shipping_price_4":this.formBeforeFifthStep.value.shipping_price_4,
        "shipping_price_5":0,
        "shipping_type_1":this.formBeforeFifthStep.value.shipping_type_1,
        "shipping_type_2":this.formBeforeFifthStep.value.shipping_type_2,
        "shipping_type_3":this.formBeforeFifthStep.value.shipping_type_3,
        "shipping_type_4":this.formBeforeFifthStep.value.shipping_type_4,
        "shipping_type_5":this.formBeforeFifthStep.value.shipping_type_5,
        "shipping_type_1_text":this.formBeforeFifthStep.value.shipping_type_1_text,
        "shipping_type_2_text":this.formBeforeFifthStep.value.shipping_type_2_text,
        "shipping_type_3_text":this.formBeforeFifthStep.value.shipping_type_3_text,
        "shipping_type_4_text":this.formBeforeFifthStep.value.shipping_type_4_text,
        "shipping_type_5_text":this.formBeforeFifthStep.value.shipping_type_5_text,
        "ad_id":this.ad_id};
        this._http.post(this.webStep6BeforeAccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponseStepFive = response;
          
          if(this.apiResponseStepFive.error == false)
          {
            console.log(this.apiResponseStepFive);
            //this.ad_id = this.apiResponseStepFive.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
        });
      }
    }
    else if(this.step==6)
    {
      this.queryParam = {"ad_id":this.ad_id};
      this._http.post(this.getCarAdForImagePrice,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          this.allAddPriceImage = this.apiResponse.record;
        }
          
      });
      
      //this.address_step = true;
      if (this.formFifthStep.invalid)
      {
        this.validateAllFormFields(this.formFifthStep);  return;
      }else{
        if(!this.userAddress)
        {
          this.userAddress = this.old_address;
        }
        let adData = {"address": this.userAddress,"latitude":this.userLatitude,"longitude":this.userLongitude,"ad_id":this.ad_id};
        this._http.post(this.webStep6AccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponse = response;
          
          if(this.apiResponse.error == false)
          {
            //console.log(this.apiResponse.lastInsertId);
            //this.ad_id = this.apiResponse.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            this.step++;
          }
        });
      }
      //this.step++;
    }
    else if(this.step==7)
    {
      //this.address_step = true;  formSixStep
      if (this.formSixStep.invalid)
      {
        this.validateAllFormFields(this.formSixStep);  return;
      }else{
        let adData = {'mobileNumber':this.formSixStep.value.mobileNumber,'hideNumber':this.formSixStep.value.hideNumber,"ad_id":this.ad_id};
        this._http.post(this.webStep7AccessoriesAdName,adData).subscribe((response:any)=>{
          this.apiResponse_new = response;
          //console.log("formData "+this.formData);
          if(this.apiResponse_new.error == false)
          {
            //console.log(this.apiResponse_new.lastInsertId);
            //this.ad_id = this.apiResponse_new.lastInsertId;
            //console.log("ad_id "+this.ad_id);
            //this.step++;
            //adSuccess
            setTimeout(() => {
              //window.location.reload();
              window.location.href = this.base_url+"adProfessionalUser";

            }, 2000);

          }
        });
      }
      //this.step++;
    }
    
  }

  previous()
  {
    this.step--
   
  }
  images : string[] = [];
  myFiles:string [] = [];
  
  

  onFileChangeNew(event:any)
  {
    //console.log("maxUploadImage ", this.maxUploadImage);
    //console.log("event.target.files.length ", event.target.files.length);
    //console.log("this.myFiles.length ", this.myFiles.length);
    this.allUploadedFile = this.myFiles.length + event.target.files.length;
    console.log("this.allUploadedFile ",this.allUploadedFile);
    //isUpload
    //this.myFiles = [];
    if (event.target.files && event.target.files[0])
    {
      //console.log("this.myFiles.length ", this.myFiles.length);
      //
      // if(numOfFiles + files.length > 4){
      //   alert("You can only upload at most 4 files!");
      //   return;
      // }
      // numOfFiles += files.length;

      if(this.maxUploadImage >= this.allUploadedFile)
      {
        this.isUpload = false;
        
          this.isUpload = false;
          var filesAmount = event.target.files.length;
          for (let i = 0; i < filesAmount; i++)
          {
            var reader = new FileReader();
            reader.onload = (event:any) => {
              // Push Base64 string
              this.images.push(event.target.result); 
              //this.patchValues();
            }
            reader.readAsDataURL(event.target.files[i]);

            this.myFiles.push(event.target.files[i]);
            
          }
      }else{
        this.isUpload = true;
      }
        
       
      
    }

    

  }
  removeImage(url:any,imgNumber:any){
    //console.log(this.images,url);
    //console.log(this.myFiles);
    this.images = this.images.filter(img => (img != url));
    //this.myFiles.splice(this.myFiles.indexOf(imgNumber), 1);
    ///this.patchValues();
    //console.log(imgNumber);
    this.myFiles.splice(imgNumber,1);
    
    this.allUploadedFile--;

  }


  handleAddressChange(address: any) {
    this.userAddress = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);

    this.centerLatitude = parseFloat(address.geometry.location.lat());
    this.centerLongitude = parseFloat(address.geometry.location.lng());
    console.log(this.centerLatitude);
    console.log(this.centerLongitude);
    this.marker = {
      position: { lat: this.centerLatitude, lng: this.centerLongitude },
    }

  }
  onItemChange(event:any)
  {
     this.first_plan_price = 0;
     
     this.first_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price +this.third_plan_price;

     this.final_plan_price = this.final_plan_price + this.additional_photo_price;
    // console.log("additional_photo_check " , this.additional_photo_check);
    // if(this.additional_photo_check == true)
    // {

    // }
  }
  onItemChangeSec(event:any)
  {
    this.second_plan_price = 0;
     this.second_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price +this.third_plan_price;
    //console.log(event);
    this.final_plan_price = this.final_plan_price + this.additional_photo_price;
  }  
  onItemChangeThird(event:any)
  {
    this.third_plan_price = 0;
     this.third_plan_price = event;
     this.final_plan_price = this.first_plan_price +this.second_plan_price+this.third_plan_price;
     this.final_plan_price = this.final_plan_price + this.additional_photo_price;
    //console.log(event);
  } 
  mapOptions: google.maps.MapOptions = {
    center: { lat: this.centerLatitude, lng: this.centerLongitude },
    zoom : 4
  }
  marker = {
    position: { lat: this.centerLatitude, lng: this.centerLongitude },
  }
  

}
