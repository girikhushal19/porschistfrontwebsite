import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-view-myorder',
  templateUrl: './view-myorder.component.html',
  styleUrls: ['./view-myorder.component.css']
})
export class ViewMyorderComponent implements OnInit {
  base_url: string;
  base_url_node: string;
  base_url_node_only: string;
  token: any;
  user_id: any;
  user_type: any;
  viewOrderURL:any
  order_id:any
  apiRes:any
  orderDetail:any

  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    this.order_id = this.actRoute.snapshot.params['id']
    this.viewOrderURL = this.base_url_node+"webGetOwnSingleSales/";

  }

  ngOnInit(): void {
    console.log(this.viewOrderURL+this.order_id)
    this._http.get(this.viewOrderURL+this.order_id).subscribe(res=>{
     this.apiRes =  res
     this.orderDetail = this.apiRes.record[0]
     console.log( this.orderDetail)
    })
  }

}
