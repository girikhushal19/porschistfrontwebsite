import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdProUserAccessSubscriptionReactivateComponent } from './ad-pro-user-access-subscription-reactivate.component';

describe('AdProUserAccessSubscriptionReactivateComponent', () => {
  let component: AdProUserAccessSubscriptionReactivateComponent;
  let fixture: ComponentFixture<AdProUserAccessSubscriptionReactivateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdProUserAccessSubscriptionReactivateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdProUserAccessSubscriptionReactivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
