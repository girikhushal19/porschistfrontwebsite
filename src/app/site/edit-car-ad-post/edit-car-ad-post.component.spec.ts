import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCarAdPostComponent } from './edit-car-ad-post.component';

describe('EditCarAdPostComponent', () => {
  let component: EditCarAdPostComponent;
  let fixture: ComponentFixture<EditCarAdPostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditCarAdPostComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditCarAdPostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
