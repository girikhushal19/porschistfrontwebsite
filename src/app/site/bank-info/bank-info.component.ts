import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-bank-info',
  templateUrl: './bank-info.component.html',
  styleUrls: ['./bank-info.component.css']
})
export class BankInfoComponent implements OnInit {
  base_url:any;base_url_node:any;base_url_node_only:any
  token:any;user_id:any;user_type:any
  allBankURL:any
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    this.allBankURL = this.base_url_node+"getAllBankInformation/";
   }

  ngOnInit(): void {
    this._http.get(this.allBankURL+ this.user_id).subscribe(res=>{
      console.log(res, "banks")
    })
  }

}
