import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  base_url_node:any
  aboutusURL:any;webUserLoginSubmit:any;
  apiRes:any;
  formData:any;formValue:any;apiResponse:any;
  constructor(private loginAuthObj:LoginauthenticationService,private _http:HttpClient) { 
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.aboutusURL = this.base_url_node+"getcontactUs";
    this.webUserLoginSubmit = this.base_url_node+"webContactSubmit";
  }

  ngOnInit(): void {
    this.getAbout()
  }

  form = new UntypedFormGroup({
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    nom: new UntypedFormControl('', [Validators.required]),
    prenom: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required ]),
    enterprise_name: new UntypedFormControl('', []),
    message: new UntypedFormControl('', [Validators.required]),
    subject: new UntypedFormControl('', [Validators.required]),
    mobile: new UntypedFormControl('', [Validators.required]),

  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }


  getAbout(){
    this._http.get(this.aboutusURL).subscribe(res=>{
      this.apiRes= res;
      console.log("this.apiRes ",this.apiRes);
    })
  }
  submit(){
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      this._http.post(this.webUserLoginSubmit,this.formValue).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          setTimeout(()=>{
            window.location.reload();
          },2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
