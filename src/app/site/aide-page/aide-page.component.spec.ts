import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AidePageComponent } from './aide-page.component';

describe('AidePageComponent', () => {
  let component: AidePageComponent;
  let fixture: ComponentFixture<AidePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AidePageComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AidePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
