import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-aide-page',
  templateUrl: './aide-page.component.html',
  styleUrls: ['./aide-page.component.css']
})
export class AidePageComponent implements OnInit {
  base_url_node:any
  aboutusURL:any
  apiRes:any
  constructor(private loginAuthObj:LoginauthenticationService,private _http:HttpClient) { 
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.aboutusURL = this.base_url_node+"getUserFaq";
  }

  ngOnInit(): void {
    this.getAbout()
  }
  getAbout(){
    this._http.get(this.aboutusURL).subscribe(res=>{
      this.apiRes= res
      console.log(this.apiRes)
    })
  }

}
