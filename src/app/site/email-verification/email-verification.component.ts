import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})
export class EmailVerificationComponent implements OnInit {

 

  base_url = "";base_url_node = "";base_url_node_only:any;webEmailOTPVerification:any;
  formData:any;formValue:any;apiResponse:any;user_id:any;queryParam:any;webResendOTP:any;
  webGetUserProfile:any;firstName:any;lastName:any;user_email:any;enteredOtp:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webEmailOTPVerification = this.base_url_node+"webEmailOTPVerification";

    this.user_id = this.actRoute.snapshot.params['id'];
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.webResendOTP = this.base_url_node+"webResendOTP";
  }

  ngOnInit(): void {

    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
        this.firstName = this.apiResponse.userRecord.firstName;
        this.lastName = this.apiResponse.userRecord.lastName;
        this.user_email = this.apiResponse.userRecord.email;
      }
       
    });


  }

  form = new UntypedFormGroup({
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    user_id: new UntypedFormControl('', []),
    first: new UntypedFormControl('', []),
    second: new UntypedFormControl('',[]),
    third: new UntypedFormControl('',[]),
    fourth: new UntypedFormControl('',[]),
    fifth: new UntypedFormControl('',[]),
    sixth: new UntypedFormControl('',[]),
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  submit(){
    //console.log("here");
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      let valuess = {"otp":this.enteredOtp,"user_id":this.user_id}
      this._http.post(this.webEmailOTPVerification,valuess).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          setTimeout(() => {
            window.location.href = this.base_url+"login";
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
  onOtpChange(event:any){
    //console.log(event);
    this.enteredOtp = event;
  }
  resendOtp()
  {
    //console.log("here");
    let valuess = {"user_id":this.user_id}
    this._http.post(this.webResendOTP,valuess).subscribe((response:any)=>{
    this.apiResponse = response;
    console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      }
    });
  }
}
