import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { GoogleMapsModule } from '@angular/google-maps'
 
import { NgxDropzoneModule } from 'ngx-dropzone';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";
import { NgOtpInputModule } from  'ng-otp-input';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { NgImageSliderModule } from 'ng-image-slider';
 

import { SiteRoutingModule } from './site-routing.module';
import { AdCarPostProfessionalComponent } from './ad-car-post-professional/ad-car-post-professional.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MobileHeaderComponent } from './mobile-header/mobile-header.component';
import { LoginComponent } from './login/login.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { AdAccessoriesPostProfessionalComponent } from './ad-accessories-post-professional/ad-accessories-post-professional.component';
import { AdSuccessPageComponent } from './ad-success-page/ad-success-page.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { HomeComponent } from './home/home.component';
import { LogoutComponent } from './logout/logout.component';
import { ResendVerificationEmailComponent } from './resend-verification-email/resend-verification-email.component';
import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
import { ViewUserProfileComponent } from './view-user-profile/view-user-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserAdProfessionalComponent } from './user-ad-professional/user-ad-professional.component';
import { HeaderForAdComponent } from './header-for-ad/header-for-ad.component';
import { HeaderCategoryComponent } from './header-category/header-category.component';
import { EditCarAdPostComponent } from './edit-car-ad-post/edit-car-ad-post.component';
import { EditadAccessoriesComponent } from './editad-accessories/editad-accessories.component';
import { AdCarPostParticularComponent } from './ad-car-post-particular/ad-car-post-particular.component';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FilterSectionComponent } from './filter-section/filter-section.component';
import { AccessoriesProductDetailComponent } from './accessories-product-detail/accessories-product-detail.component';
import { AdChatInitiateComponent } from './ad-chat-initiate/ad-chat-initiate.component';
import { AllMessageComponent } from './all-message/all-message.component';
import { GetCartComponent } from './get-cart/get-cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { UserPurchaseSuccessComponent } from './user-purchase-success/user-purchase-success.component';
import { AccessoriesPurchaseListComponent } from './accessories-purchase-list/accessories-purchase-list.component';
import { AdAccessoriesPostParticularComponent } from './ad-accessories-post-particular/ad-accessories-post-particular.component';
import { MyFavoriteComponent } from './my-favorite/my-favorite.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NotificationsComponent } from './notifications/notifications.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AidePageComponent } from './aide-page/aide-page.component';
import { MyCartComponent } from './my-cart/my-cart.component';
import { MyCheckoutComponent } from './my-checkout/my-checkout.component';
import { ViewMyorderComponent } from './view-myorder/view-myorder.component';
import { BankInfoComponent } from './bank-info/bank-info.component';
import { SellerOwnAdComponent } from './seller-own-ad/seller-own-ad.component';
import { AdNormalUserTopSearchComponent } from './ad-normal-user-top-search/ad-normal-user-top-search.component';
import { AdNormalUserTopUrgentComponent } from './ad-normal-user-top-urgent/ad-normal-user-top-urgent.component';
import { AdNormalUserCarAdReactivateComponent } from './ad-normal-user-car-ad-reactivate/ad-normal-user-car-ad-reactivate.component';
import { SellerProfileComponent } from './seller-profile/seller-profile.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { AdNormalUserAcesAdReactivateComponent } from './ad-normal-user-aces-ad-reactivate/ad-normal-user-aces-ad-reactivate.component';
import { AdProUserSubscriptionReactivateComponent } from './ad-pro-user-subscription-reactivate/ad-pro-user-subscription-reactivate.component';
import { ProPlanRenewalSuccessComponent } from './pro-plan-renewal-success/pro-plan-renewal-success.component';
import { AdProUserAccessSubscriptionReactivateComponent } from './ad-pro-user-access-subscription-reactivate/ad-pro-user-access-subscription-reactivate.component';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ViewProUserProfileComponent } from './view-pro-user-profile/view-pro-user-profile.component';
import { EditProUserProfileComponent } from './edit-pro-user-profile/edit-pro-user-profile.component';
import { ReportAdComponent } from './report-ad/report-ad.component';
import { RatingAdComponent } from './rating-ad/rating-ad.component';
import { SuccessRegistrationComponent } from './success-registration/success-registration.component';
import { ReviresComponent } from './revires/revires.component';
import { ProfessionalRegistrationComponent } from './professional-registration/professional-registration.component';
import { SuccessProRegistrationComponent } from './success-pro-registration/success-pro-registration.component';
import { EmailProVerificationComponent } from './email-pro-verification/email-pro-verification.component';
import { SetNewPasswordComponent } from './set-new-password/set-new-password.component'; 
 
 


@NgModule({
  declarations: [
    AdCarPostProfessionalComponent,
    HeaderComponent,
    FooterComponent,
    MobileHeaderComponent,
    LoginComponent,
    UserRegistrationComponent,
    AdAccessoriesPostProfessionalComponent,
    AdSuccessPageComponent,
    EmailVerificationComponent,
    UserProfileComponent,
    MyProfileComponent,
    HomeComponent,
    LogoutComponent,
    ResendVerificationEmailComponent,
    EditUserProfileComponent,
    ViewUserProfileComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    UserAdProfessionalComponent,
    HeaderForAdComponent,
    HeaderCategoryComponent,
    EditCarAdPostComponent,
    EditadAccessoriesComponent,
    AdCarPostParticularComponent,
    ProductListComponent,
    ProductDetailComponent,
    FilterSectionComponent,
    AccessoriesProductDetailComponent,
    AdChatInitiateComponent,
    AllMessageComponent,
    GetCartComponent,
    CheckoutComponent,
    UserPurchaseSuccessComponent,
    AccessoriesPurchaseListComponent,
    AdAccessoriesPostParticularComponent,
    MyFavoriteComponent,
    NotificationsComponent,
    TermsConditionComponent,
    AboutUsComponent,
    ContactUsComponent,
    PrivacyPolicyComponent,
    AidePageComponent,
    MyCartComponent,
    MyCheckoutComponent,
    ViewMyorderComponent,
    BankInfoComponent,
    SellerOwnAdComponent,
    AdNormalUserTopSearchComponent,
    AdNormalUserTopUrgentComponent,
    AdNormalUserCarAdReactivateComponent,
    SellerProfileComponent,
    AdNormalUserAcesAdReactivateComponent,
    AdProUserSubscriptionReactivateComponent,
    ProPlanRenewalSuccessComponent,
    AdProUserAccessSubscriptionReactivateComponent,
    ViewProUserProfileComponent,
    EditProUserProfileComponent,
    ReportAdComponent,
    RatingAdComponent,
    SuccessRegistrationComponent,
    ReviresComponent,
    ProfessionalRegistrationComponent,
    SuccessProRegistrationComponent,
    EmailProVerificationComponent,
    SetNewPasswordComponent
  ],
  imports: [
    //DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxDropzoneModule,
    CommonModule,
    SiteRoutingModule,
    GooglePlaceModule,
    NgOtpInputModule,
    SlickCarouselModule,
     
  
    GoogleMapsModule,
    NgImageSliderModule,
    AutocompleteLibModule,
    NgxPaginationModule, 
    OwlDateTimeModule, 
    OwlNativeDateTimeModule,
    BrowserModule,
        BrowserAnimationsModule,
         
  ]
})
export class SiteModule { }
