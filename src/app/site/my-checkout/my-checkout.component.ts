import { Component, OnInit } from '@angular/core';
import { LoginauthenticationService } from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-my-checkout',
  templateUrl: './my-checkout.component.html',
  styleUrls: ['./my-checkout.component.css']
})
export class MyCheckoutComponent implements OnInit {
  base_url: any; base_url_node: any; base_url_node_only: any; user_id: any
  cartCountURL: any; cartData: any; queryParam: any
  userURL: any; userData: any; token: any
  userqueryParam: any
  webCheckout:any;webCheckoutparam:any;apiResponse:any
  radioButtonval:any;webAddDeliveryAddress:any;
  deliveryAddress_id:any; deliveryErr:any
  shippingErr:any
  address: string = '';
    userLatitude: string = '';
    userLongitude: string = '';
    apiResponseAddress:any;formValue:any;
    firstName: string = ''; lastName: string = '';  mobileNumber: string = '';
    webGetDeliveryAddress:any; allDeliveryAddress:any
    extraPrizes:any
    webSubmitCartApi:any; apiResponse2:any
    shippingoff:any
    item_owner_user_type:string="";
  constructor(private _http: HttpClient, private loginAuthObj: LoginauthenticationService) {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.token = this.loginAuthObj.userLogin();

    this.cartCountURL = this.base_url_node + "webGetUserCart";
    this.userURL = this.base_url_node + "getUserProfile";
    this.webCheckout = this.base_url_node+"webCheckout";
    this.webAddDeliveryAddress = this.base_url_node+"webAddDeliveryAddress";
    this.webGetDeliveryAddress = this.base_url_node+"webGetDeliveryAddress";
    this.webSubmitCartApi = this.base_url_node+"webSubmitCart";

    this.deliveryErr = true
    this.shippingErr = true
    this.shippingoff = true
    this.radioButtonval = 2
    this.deliveryAddress_id = ''

  }

  ngOnInit(): void {
    this.queryParam = { "user_id": this.user_id };
    this._http.post(this.cartCountURL, this.queryParam).subscribe(res => {
      //this.cartData = res
      this.apiResponse = res;
      console.log("this.apiResponse ", this.apiResponse);
      this.extraPrizes = this.apiResponse
      this.cartData = this.apiResponse.record;
      this.item_owner_user_type = this.apiResponse.user_type;
      console.log("this.item_owner_user_type ", this.item_owner_user_type);
    })

    this.userqueryParam = {
      "user_id": this.user_id,
      "token": this.token
    };
   

    this._http.post(this.userURL, this.userqueryParam).subscribe(res => {
      this.apiResponse = res;
      this.userData = this.apiResponse.userRecord;
     
      this.firstName = this.userData.firstName;
      this.lastName = this.userData.lastName;
      this.mobileNumber = this.userData.mobileNumber;
    })

   
    this._http.post(this.webGetDeliveryAddress,this.queryParam).subscribe((response:any)=>{
      if(response.error == false)
      {
        this.allDeliveryAddress = response.record;
     //   console.log(this.allDeliveryAddress )
      }
    });



    

  }

  checkout(){
    this.webCheckoutparam = {
      "user_id": this.user_id,
      "self_pickup":this.radioButtonval
    }

   // console.log(this.webCheckoutparam)

    this._http.post(this.webCheckout, this.webCheckoutparam).subscribe((response:any)=>{
      this.apiResponse = response;
    //  console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          
          setTimeout(() => {
            var redirect_url_sdf = this.base_url_node_only+"api/webaccessoiresPurchase?paymentId="+this.apiResponse.order_id;
          //  console.log(redirect_url_sdf);
            window.location.href = redirect_url_sdf;
           // window.location.href = this.base_url+"checkout";
            
          }, 2000);
        }
      });
    
  }
  sendPayment(){

    const AddressParams = {
      "self_pickup":this.radioButtonval ,
      "user_id":this.user_id,
      "address_id":this.deliveryAddress_id
    }

    console.log(AddressParams)
   

   if(this.deliveryAddress_id == '' && AddressParams.self_pickup == 2){
      this.deliveryErr = false
    }

    else{

      this._http.post( this.webSubmitCartApi,AddressParams).subscribe(res=>{
        this.apiResponse2 = res
        if(this.apiResponse2.error == false)
        {
          setTimeout(() => {
            window.location.href='/checkout';
          }, 2000);
        }
      })
    }
    
  }
  getAddress(eve:any){
    this.deliveryAddress_id = eve
    this.deliveryErr = true
  }
  getDeliveryPriceFun(eve:any){
    console.log("priceeee ", eve.target.value);
  }
  getValue(eve:any){
    console.log(eve.target.value)
    if(eve.target.value == '1'){
      this.shippingoff = true
      this.deliveryAddress_id = ''
    
    }else{
      this.shippingoff = false
    }
    this.radioButtonval = eve.target.value
    this.shippingErr = true
  }

  get f(){
    return this.formAddress.controls;
  }
  
  formAddress = new UntypedFormGroup({
    user_id: new UntypedFormControl('', []),
    address: new UntypedFormControl('', [Validators.required]),
    latitude: new UntypedFormControl('', []),
    longitude: new UntypedFormControl('', []),
  });
  
  get formAddressFun(){
    return this.formAddress.controls;
  }
  handleAddressChange(address: any) {
    this.address = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);
  }
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }
  submitAddress(){
    //console.log("hereeeeeeeeeeeee");
    if (this.formAddress.valid)
    {
      this.formValue = this.formAddress.value;
      this.formValue.address =  this.address

      
      //let valuess = {"otp":this.enteredOtp,"user_id":this.user_id}
      this._http.post(this.webAddDeliveryAddress,this.formValue).subscribe((response:any)=>{
      this.apiResponseAddress = response;
      
        if(this.apiResponseAddress.error == false)
        {
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.formAddress); 
      // validate all form fields
    }
  }

}
