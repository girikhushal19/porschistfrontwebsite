import { Component, OnInit,HostListener } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import {  Router,ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;getBannerHomePage:any;allBanner:any;allCategory:any;
  counterNum:number;getCategory:any;getWebHomeCarAd:any;allAccessAd:any;adUserFavAdd:any; getWebHomeAccessAd:any;getWebProductList:any;
  allColorInterior:any;
  showDiv:boolean = false;
  showDivButton:boolean = true;
  tab1Div:boolean = true;
  tab2Div:boolean = false;
  allCategoryList:any;allModel:any;allModelList:any;allAccessModelList:any;
  getSubAttribute:any;getColor:any;allColor:any;registration_year:any;allChasisType:any;allYear:any;allCarburant:any;allEtatDuVehicle:any;allBoiteDeVitesse:any;allYesNo:any;
  allSubModelList:any;allTypeDePiece:any;
  cat1:any;cat2:any;categoryTypeForm:any;allWebSubModel:any;
  carModelArray:any;yearArray:any;  chahisArray:any;
  colorArray:number [] = [];yearArrayAccess:number [] = [];
  allCarAd:any []= [];  yearArray_2:Number = 0;
  allCarAdNew:any []= []; accessRegYear_2:string=""; accessRegYear:string="";
  category:string="";fuel:string="";maintenance_invoice:string="";colorArrayString:string="";yearArrayString:string="";chahisArrayString:string="";type_of_gearbox:string="";vehicle_condition:string="";warranty:string="";maintenance_booklet:string="";accidented:string="";
  original_paint:string="";matching_number:string="";matching_color_paint:string="";matching_color_interior:string="";deductible_VAT:string="";minPrice:string="";maxPrice:string="";minKilometer:string="";maxKilometer:string="";model_variant:string="";
  accessModel:string="";subModel:string="";yearArrayAccessString:string="";type_de_piece:string="";state:string="";OEM:string="";minAccessPrice:string="";maxAccessPrice:string="";
  yesNoArray:any [] = [];
  old_carModelArray:number [] = [];old_yearArray:string [] = [];old_chahisArray:number [] = [];
  old_colorArray:number [] = [];old_yearArrayAccess:number [] = [];
  colorId:string=""; colorInteriorId:string=""; 
  carModelString:string = "";allYesNoApi:any;
  userAddress: string = ''; userLatitude: number = 0; userLongitude: number = 0;ad_name:string=""; api_response_var:number = 0;
  totalPageNumber:number=1;pageStart:number=0;totalAdcount:number=0;pageNo:number=1;
  sorting_var:string="";
  allVersions:any;
  
  filterArray:any [] = [];
  filterArrayAccess:any [] = [];
  radioStatus:boolean = false;

  zip_code:any;  country:any;   report_piwi:any;    city:any;   vendeurType:any;   color_name:any;   onlineSince:any;   nearByKm:any;   carVersion:any;   pors_warranty:any;
  allSubModelListCar:any; 

  model_heading:string="Modèle";  radio_model_heading:boolean = false;
  model_variant_heading:string="Génération";  radio_model_variant:boolean = false;
  carVersion_heading:string="Version";  radio_carVersion:boolean = false;
  chahis_heading:string="Type de châssis";  radio_chahis:boolean = false;
  type_of_gearbox_heading:string="Boîte de vitesse";  radio_type_of_gearbox:boolean = false;
  fuel_heading:string="Carburant";  radio_fuel:boolean = false;
  color_exterior_heading:string="Couleur extérieur";  radio_color_exterior:boolean = false;
  color_interior_heading:string="Couleur intérieur";  radio_color_interior:boolean = false;
  pors_warranty_heading:string="Garantie Porsche Approved";  radio_pors_warranty:boolean = false;
  warranty_heading:string="Garantie";  radio_warranty:boolean = false;
  maintenance_booklet_heading:string="Carnet d’entretien";  radio_maintenance_booklet:boolean = false;
  maintenance_invoice_heading:string="Facture d’entretien";  radio_maintenance_invoice:boolean = false;
  report_piwi_heading:string="Rapport piwis";  radio_report_piwi:boolean = false;
  matching_number_heading:string="Matching numbers";  radio_matching_number:boolean = false;
  matching_color_paint_heading:string="Matching colors";  radio_matching_color_paint:boolean = false;
  original_paint_heading:string="Peinture d’origine"; radio_original_paint:boolean = false;
  onlineSince_heading:string="Annonce en ligne depuis"; radio_onlineSince:boolean = false;
  vendeurType_heading:string="Vendeur"; radio_vendeurType:boolean = false;

  onYearChange_heading:string="Année";  radio_onYearChange:boolean = false;
  onYearChange_2_heading:string="";  radio_onYearChange_2:boolean = false;
  minKilometer_heading:string="kilométrage";  radio_minKilometer:boolean = false;
  maxKilometer_heading:string="";  radio_maxKilometer:boolean = false;

  accessModel_heading:string="Modèle";  radio_accessModel:boolean = false;
  subModel_heading:string="Modèle";  radio_subModel:boolean = false;

  accessRegYear_heading:string="Année";  radio_accessRegYear:boolean = false;
  accessRegYear_2_heading:string="";  radio_accessRegYear_2:boolean = false;
  type_de_piece_heading:string="Type de pièce";  radio_type_de_piece:boolean = false;
  state_heading:string="État";  radio_state:boolean = false;
  OEM_heading:string="Pièce d'origine Porsche (OEM)";  radio_OEM:boolean = false;

  zip_code_heading:any="Code postal";  radio_zip_code:boolean = false;
  city_heading:any="Ville";  radio_city:boolean = false;
  country_heading:any="Pays";  radio_country:boolean = false;

  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService,private route: ActivatedRoute,private router: Router)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";

    //this.getBannerHomePage = this.base_url_node+"getBannerHomePage";
    //this.getCategory = this.base_url_node+"getCategory";
    //this.getWebHomeCarAd = this.base_url_node+"getWebHomeCarAd";
    //this.getWebHomeAccessAd = this.base_url_node+"getWebHomeAccessAd";

    this.adUserFavAdd = this.base_url_node+"adUserFavAdd";
    this.getWebProductList = this.base_url_node+"getWebProductList";
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    this.counterNum = 0;

    this.getCategory = this.base_url_node+"getCategoryOnAdPage";
    this.allModel = this.base_url_node+"getModel";
    this.getSubAttribute = this.base_url_node+"getSubAttribute";
    this.registration_year = this.base_url_node+"registration_year";
    this.getColor = this.base_url_node+"getColor";
    this.allWebSubModel = this.base_url_node+"allWebSubModel";
    this.allYesNoApi = this.base_url_node+"allYesNoApi";
    this.categoryTypeForm = "Porsche Voitures";
    //console.log("categoryTypeForm 96  "+this.categoryTypeForm);
    this.totalPageNumber = 1;this.pageStart = 0;


    // this._http.get(this.base_url_node+"allVersions",{}).subscribe((response:any)=>{
    //   //console.log("response of api"+response);
    //   this.apiResponse = response;
    //   //console.log(this.apiResponse);
    //   if(this.apiResponse.error == false)
    //   {
    //     this.allVersions = this.apiResponse.record;
    //     //console.log("allVersions ",this.allVersions);
    //   }
    // });

    this._http.get(this.base_url_node+"getColorInterior",{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allColorInterior = this.apiResponse.record;
      }
       
    });
  }

  ngOnInit(): void {

    //console.log(this.route.snapshot.queryParams);
    //console.log(this.route.snapshot.queryParams['category']);
    //console.log(this.route.snapshot.queryParams['carModelArray']);
    //console.log(this.route.snapshot.fragment);


    

    if(this.route.snapshot.queryParams['category'] != "" && this.route.snapshot.queryParams['category'] != null)
    {
      this.categoryTypeForm = this.route.snapshot.queryParams['category'];
      //console.log("categoryTypeForm 110  "+this.categoryTypeForm);
      if(this.categoryTypeForm == 'Porsche Voitures')
      {
        this.tab1Div = true;
        this.tab2Div = false;
      }else{
        this.tab1Div = false;
        this.tab2Div = true;
      }

    }
    if(this.route.snapshot.queryParams['userAddress'] != "" && this.route.snapshot.queryParams['userAddress'] != null && this.route.snapshot.queryParams['userLatitude'] != "" && this.route.snapshot.queryParams['userLatitude'] != null && this.route.snapshot.queryParams['userLongitude'] != "" && this.route.snapshot.queryParams['userLongitude'] != null)
    {
      this.userAddress = this.route.snapshot.queryParams['userAddress'];
      this.userLatitude = parseFloat(this.route.snapshot.queryParams['userLatitude']);
      this.userLongitude = parseFloat(this.route.snapshot.queryParams['userLongitude']);
    }
    
    if(this.route.snapshot.queryParams['ad_name'] != "" && this.route.snapshot.queryParams['ad_name'] != null)
    {
      this.ad_name = this.route.snapshot.queryParams['ad_name'];
    }

    // http://localhost:4200/productList?category=Porsche%20Voitures&carModelArray=62e3b87d9c0dc0df532cfd07&colorArray=&yearArray=2024&yearArray_2=2021&chahisArray=62e3d26dec6f493144a2533f&fuel=62e3d32dec6f493144a25381&type_of_gearbox=62e3d3e2ec6f493144a253bc&vehicle_condition=&pors_warranty=yes&warranty=yes&maintenance_booklet=yes&maintenance_invoice=no&accidented=&original_paint=yes&matching_number=yes&matching_color_paint=no&matching_color_interior=&deductible_VAT=&minPrice=&maxPrice=&minKilometer=10000&maxKilometer=125000&model_variant=64be5b9d64988cc7cbcb2573&accessModel=&subModel=&type_de_piece=&state=&OEM=&minAccessPrice=&maxAccessPrice=&yearArrayAccess=&ad_name=&userLatitude=0&userLongitude=0&userAddress=&carVersion=Test%201&nearByKm=&onlineSince=10&vendeurType=pro_user&color_name=&city=ukjghjgjh&report_piwi=yes&country=hkjhkh&zip_code=897987&colorInteriorId=6565c56fd14b71cc5ea30c26&colorId=62e3d541ec6f493144a253de&accessRegYear=&accessRegYear_2=
    //  accessModel=62e3ba3a8a232064cb320a8c&
    // subModel=62e3bbb3fa601e664881e488&
    // type_de_piece=62e4f5d65ed7e4f294719643&
    // accessRegYear=2021&accessRegYear_2=2024
    // state=Neuf&
    // OEM=no

    if(this.route.snapshot.queryParams['carModelArray'] != "" && this.route.snapshot.queryParams['carModelArray'] != null)
    {
      // this.old_carModelArray = this.route.snapshot.queryParams['carModelArray'].split(",");
      // this.carModelArray = this.route.snapshot.queryParams['carModelArray'].split(",");
      this.carModelArray = this.route.snapshot.queryParams['carModelArray'];
      //console.log(this.old_carModelArray);
      //console.log("hereee");
      //console.log("old_carModelArray "+this.old_carModelArray);
      console.log("carModelArray " ,this.carModelArray);
    }

    if(this.route.snapshot.queryParams['colorId'] != "" && this.route.snapshot.queryParams['colorId'] != null)
    {
      this.colorId = this.route.snapshot.queryParams['colorId'];
      //console.log(this.colorId);
    }
    if(this.route.snapshot.queryParams['colorInteriorId'] != "" && this.route.snapshot.queryParams['colorInteriorId'] != null)
    {
      this.colorInteriorId = this.route.snapshot.queryParams['colorInteriorId'];
      //console.log(this.colorInteriorId);
    }
    
    if(this.route.snapshot.queryParams['yearArray'] != "" && this.route.snapshot.queryParams['yearArray'] != null)
    {
      this.yearArray = this.route.snapshot.queryParams['yearArray'];
    }
    if(this.route.snapshot.queryParams['yearArray_2'] != "" && this.route.snapshot.queryParams['yearArray_2'] != null)
    {
      this.yearArray_2 = this.route.snapshot.queryParams['yearArray_2'];
    }

    if(this.route.snapshot.queryParams['accessRegYear'] != "" && this.route.snapshot.queryParams['accessRegYear'] != null)
    {
      this.accessRegYear = this.route.snapshot.queryParams['accessRegYear'];
    }
    if(this.route.snapshot.queryParams['accessRegYear_2'] != "" && this.route.snapshot.queryParams['accessRegYear_2'] != null)
    {
      this.accessRegYear_2 = this.route.snapshot.queryParams['accessRegYear_2'];
    }


    if(this.route.snapshot.queryParams['yearArrayAccess'] != "" && this.route.snapshot.queryParams['yearArrayAccess'] != null)
    {
      this.yearArrayAccess = this.route.snapshot.queryParams['yearArrayAccess'];
    }
    

    if(this.route.snapshot.queryParams['chahisArray'] != "" && this.route.snapshot.queryParams['chahisArray'] != null)
    {
      this.chahisArray = this.route.snapshot.queryParams['chahisArray'];
      console.log("this.chahisArray ", this.yearArray);
    }
    if(this.route.snapshot.queryParams['maintenance_invoice'] != "" && this.route.snapshot.queryParams['maintenance_invoice'] != null)
    {
      this.maintenance_invoice = this.route.snapshot.queryParams['maintenance_invoice'];
    }
    if(this.route.snapshot.queryParams['fuel'] != "" && this.route.snapshot.queryParams['fuel'] != null)
    {
      this.fuel = this.route.snapshot.queryParams['fuel'];
    }
    if(this.route.snapshot.queryParams['type_of_gearbox'] != "" && this.route.snapshot.queryParams['type_of_gearbox'] != null)
    {
      this.type_of_gearbox = this.route.snapshot.queryParams['type_of_gearbox'];
    }
    if(this.route.snapshot.queryParams['vehicle_condition'] != "" && this.route.snapshot.queryParams['vehicle_condition'] != null)
    {
      this.vehicle_condition = this.route.snapshot.queryParams['vehicle_condition'];
    }
    if(this.route.snapshot.queryParams['warranty'] != "" && this.route.snapshot.queryParams['warranty'] != null)
    {
      this.warranty = this.route.snapshot.queryParams['warranty'];
    }
    if(this.route.snapshot.queryParams['maintenance_booklet'] != "" && this.route.snapshot.queryParams['maintenance_booklet'] != null)
    {
      this.maintenance_booklet = this.route.snapshot.queryParams['maintenance_booklet'];
    }
    if(this.route.snapshot.queryParams['accidented'] != "" && this.route.snapshot.queryParams['accidented'] != null)
    {
      this.accidented = this.route.snapshot.queryParams['accidented'];
    }
    if(this.route.snapshot.queryParams['original_paint'] != "" && this.route.snapshot.queryParams['original_paint'] != null)
    {
      this.original_paint = this.route.snapshot.queryParams['original_paint'];
    }
    if(this.route.snapshot.queryParams['matching_number'] != "" && this.route.snapshot.queryParams['matching_number'] != null)
    {
      this.matching_number = this.route.snapshot.queryParams['matching_number'];
    }
    if(this.route.snapshot.queryParams['matching_color_paint'] != "" && this.route.snapshot.queryParams['matching_color_paint'] != null)
    {
      this.matching_color_paint = this.route.snapshot.queryParams['matching_color_paint'];
    }
    if(this.route.snapshot.queryParams['matching_color_interior'] != "" && this.route.snapshot.queryParams['matching_color_interior'] != null)
    {
      this.matching_color_interior = this.route.snapshot.queryParams['matching_color_interior'];
    }
    if(this.route.snapshot.queryParams['deductible_VAT'] != "" && this.route.snapshot.queryParams['deductible_VAT'] != null)
    {
      this.deductible_VAT = this.route.snapshot.queryParams['deductible_VAT'];
    }
    if(this.route.snapshot.queryParams['minPrice'] != "" && this.route.snapshot.queryParams['minPrice'] != null)
    {
      this.minPrice = this.route.snapshot.queryParams['minPrice'];
    }
    if(this.route.snapshot.queryParams['maxPrice'] != "" && this.route.snapshot.queryParams['maxPrice'] != null)
    {
      this.maxPrice = this.route.snapshot.queryParams['maxPrice'];
    }
    if(this.route.snapshot.queryParams['minKilometer'] != "" && this.route.snapshot.queryParams['minKilometer'] != null)
    {
      this.minKilometer = this.route.snapshot.queryParams['minKilometer'];
    }
    if(this.route.snapshot.queryParams['maxKilometer'] != "" && this.route.snapshot.queryParams['maxKilometer'] != null)
    {
      this.maxKilometer = this.route.snapshot.queryParams['maxKilometer'];
    }
    if(this.route.snapshot.queryParams['model_variant'] != "" && this.route.snapshot.queryParams['model_variant'] != null)
    {
      this.model_variant = this.route.snapshot.queryParams['model_variant'];
    }
    if(this.route.snapshot.queryParams['accessModel'] != "" && this.route.snapshot.queryParams['accessModel'] != null)
    {
      this.accessModel = this.route.snapshot.queryParams['accessModel'];
      //console.log("hereeeeee 216" + this.accessModel);
      this.queryParam = { "parent_id":this.accessModel  };
      this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
        
        //console.log("response of api"+response);
        this.apiResponse = response;
        if(this.apiResponse.error == false)
        {
          this.allSubModelList = this.apiResponse.record;
          //console.log("response of api allSubModelList "+this.allSubModelList);
        }
      });
    }
    
    if(this.route.snapshot.queryParams['subModel'] != "" && this.route.snapshot.queryParams['subModel'] != null)
    {
      this.subModel = this.route.snapshot.queryParams['subModel'];
      //console.log("hereeeeee 216" + this.subModel);
    }
    
    if(this.route.snapshot.queryParams['type_de_piece'] != "" && this.route.snapshot.queryParams['type_de_piece'] != null)
    {
      this.type_de_piece = this.route.snapshot.queryParams['type_de_piece'];
      //console.log("hereeeeee 254" + this.type_de_piece);
    }
    if(this.route.snapshot.queryParams['state'] != "" && this.route.snapshot.queryParams['state'] != null)
    {
      this.state = this.route.snapshot.queryParams['state'];
      //console.log("hereeeeee 254" + this.state);
    }
    if(this.route.snapshot.queryParams['OEM'] != "" && this.route.snapshot.queryParams['OEM'] != null)
    {
      this.OEM = this.route.snapshot.queryParams['OEM'];
      //console.log("hereeeeee 254" + this.OEM);
    }
    if(this.route.snapshot.queryParams['minAccessPrice'] != "" && this.route.snapshot.queryParams['minAccessPrice'] != null)
    {
      this.minAccessPrice = this.route.snapshot.queryParams['minAccessPrice'];
      //console.log("hereeeeee 254" + this.minAccessPrice);
    }
    if(this.route.snapshot.queryParams['maxAccessPrice'] != "" && this.route.snapshot.queryParams['maxAccessPrice'] != null)
    {
      this.maxAccessPrice = this.route.snapshot.queryParams['maxAccessPrice'];
      //console.log("hereeeeee 254" + this.maxAccessPrice);
    }


    if(this.route.snapshot.queryParams['zip_code'] != "" && this.route.snapshot.queryParams['zip_code'] != null)
    {
      this.zip_code = this.route.snapshot.queryParams['zip_code'];
    }

    if(this.route.snapshot.queryParams['country'] != "" && this.route.snapshot.queryParams['country'] != null)
    {
      this.country = this.route.snapshot.queryParams['country'];
    }
    if(this.route.snapshot.queryParams['report_piwi'] != "" && this.route.snapshot.queryParams['report_piwi'] != null)
    {
      this.report_piwi = this.route.snapshot.queryParams['report_piwi'];
    }
    if(this.route.snapshot.queryParams['city'] != "" && this.route.snapshot.queryParams['city'] != null)
    {
      this.city = this.route.snapshot.queryParams['city'];
    }
    if(this.route.snapshot.queryParams['vendeurType'] != "" && this.route.snapshot.queryParams['vendeurType'] != null)
    {
      this.vendeurType = this.route.snapshot.queryParams['vendeurType'];
    }
    if(this.route.snapshot.queryParams['onlineSince'] != "" && this.route.snapshot.queryParams['onlineSince'] != null)
    {
      this.onlineSince = this.route.snapshot.queryParams['onlineSince'];
    }
    if(this.route.snapshot.queryParams['color_name'] != "" && this.route.snapshot.queryParams['color_name'] != null)
    {
      this.color_name = this.route.snapshot.queryParams['color_name'];
    }
    if(this.route.snapshot.queryParams['nearByKm'] != "" && this.route.snapshot.queryParams['nearByKm'] != null)
    {
      this.nearByKm = this.route.snapshot.queryParams['nearByKm'];
    }
    if(this.route.snapshot.queryParams['carVersion'] != "" && this.route.snapshot.queryParams['carVersion'] != null)
    {
      this.carVersion = this.route.snapshot.queryParams['carVersion'];
    }
    if(this.route.snapshot.queryParams['pors_warranty'] != "" && this.route.snapshot.queryParams['pors_warranty'] != null)
    {
      this.pors_warranty = this.route.snapshot.queryParams['pors_warranty'];
    }
    
    // zip_code ,  country   report_piwi    city   vendeurType   color_name   onlineSince   nearByKm   carVersion   pors_warranty
    
    



    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          console.log("userImage "+this.apiResponse.userRecord.userImage);
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
          
        }
      });
    }
    this._http.get(this.getCategory,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCategoryList = this.apiResponse.record;
        this.cat1 = this.allCategoryList[0].category;
        this.cat2 = this.allCategoryList[1].category;
      }
      
    });
    this._http.get(this.allYesNoApi,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allYesNo = this.apiResponse.record;
      }
    });
    this._http.get(this.registration_year,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allYear = this.apiResponse.record;
      }
      
    });
    this.queryParam = {attribute_type:"Porsche Voitures"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allModelList = this.apiResponse.record;
        //console.log(this.allModelList);
        
        
      }
    });
    this.queryParam = {attribute_type:"Pièces et accessoires"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allAccessModelList = this.apiResponse.record;
        
      }
    });
    //Type de châssis
    this.queryParam = { "parent_id":"62e3d257ec6f493144a2533a"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allChasisType = this.apiResponse.record;
      }
    });
    //Carburant
    this.queryParam = { "parent_id":"62e3d307ec6f493144a25377"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allCarburant = this.apiResponse.record;
        //console.log("allCarburant"+this.allCarburant);
      }
    });
    //Etat du véhicule
    this.queryParam = { "parent_id":"62e3d38aec6f493144a25390"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allEtatDuVehicle = this.apiResponse.record;
      }
    });
    //Boîte de vitesse
    this.queryParam = { "parent_id":"62e3d3d3ec6f493144a253b7"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allBoiteDeVitesse = this.apiResponse.record;
      }
    });
    //getColor
    this._http.get(this.getColor,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allColor = this.apiResponse.record;
      }
       
    });
    //type_de_piece
    this.queryParam = { "parent_id":"62e4f5a35ed7e4f294719634"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allTypeDePiece = this.apiResponse.record;
        
      }
    });
    // let queryParam = {user_id:this.user_id}
    // this._http.post(this.getWebProductList,queryParam).subscribe((response:any)=>{
    //   this.apiResponse = response;
    //   //console.log(this.apiResponse);
    //   if(this.apiResponse.error == false)
    //   {
    //     this.allCarAd = this.apiResponse.record;
    //   }
    // });


    this.queryParam = {
      "ad_name":this.ad_name,
      "userAddress":this.userAddress,
      "userLatitude":this.userLatitude,
      "userLongitude":this.userLongitude,
      'category': this.categoryTypeForm,
      'carModelArray':this.carModelArray,
      'yearArray':this.yearArray,
      'yearArray_2':this.yearArray_2,
      'chahisArray':this.chahisArray,
      'fuel':this.fuel,
      'type_of_gearbox':this.type_of_gearbox,
      'vehicle_condition':this.vehicle_condition,
      'colorId':this.colorId,
      'colorInteriorId':this.colorInteriorId,
      'warranty':this.warranty,
      'maintenance_booklet':this.maintenance_booklet,
      'maintenance_invoice':this.maintenance_invoice,
      'accidented':this.accidented,
      'original_paint':this.original_paint,
      'matching_number':this.matching_number,
      'matching_color_paint':this.matching_color_paint,
      'matching_color_interior':this.matching_color_interior,
      'deductible_VAT':this.deductible_VAT,
      'minPrice':this.minPrice,
      'maxPrice':this.maxPrice,
      'minKilometer':this.minKilometer,
      'maxKilometer':this.maxKilometer,
      'model_variant':this.model_variant,
      'accessModel':this.accessModel,
      'subModel':this.subModel,
      'yearArrayAccess':this.yearArrayAccess,
      'accessRegYear':this.accessRegYear,
      'accessRegYear_2':this.accessRegYear_2,
      'type_de_piece':this.type_de_piece,
      'state':this.state,
      'OEM':this.OEM,
      'minAccessPrice':this.minAccessPrice,
      'maxAccessPrice':this.maxAccessPrice,
      'user_id':this.user_id,
      'zip_code':this.zip_code,
      'country':this.country,
      'report_piwi':this.report_piwi,
      'city':this.city,
      'vendeurType':this.vendeurType,
      'color_name':this.color_name,
      'onlineSince':this.onlineSince,
      'nearByKm':this.nearByKm,
      'carVersion':this.carVersion,
      'pors_warranty':this.pors_warranty
    };

    //zip_code ,  country   report_piwi    city   vendeurType   color_name   onlineSince   nearByKm   carVersion   pors_warranty
    //console.log(this.queryParam);
    
    this._http.post(this.getWebProductList,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      console.log("ng on init function calling ");
      if(this.apiResponse.error == false)
      {
        ///this.allTypeDePiece = this.apiResponse.record;
        
        //this.allCarAd = this.apiResponse.record;
        const fruits = this.apiResponse.record;
        fruits.map((object:any)=>{
          //console.log(object)
          this.allCarAd.push(object);
        });
      }
    });

  }

  
  handleAddressChange(address: any) {
    this.userAddress = address.formatted_address
    this.userLatitude = parseFloat(address.geometry.location.lat());
    this.userLongitude = parseFloat(address.geometry.location.lng());
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);
    console.log("address "+ JSON.stringify(address));
  }
  
  onCheckboxChange(e:any,f:any)
  {
    //console.log("e.target ", e);
    console.log("f  ", f);
    this.radioStatus = true;
    this.radio_model_heading = true;
    
    this.model_heading = f;
    console.log("this.radioStatus ",this.radioStatus);

    let cc = {
      paramName:"carModelArray",
      paramValue:f
    };
    this.carModelArray = e.target.value;
    const paramName2 = "carModelArray";
    const filteredPeople = this.filterArray.filter( (item) => item.paramName != paramName2);
    this.filterArray = filteredPeople;
    this.filterArray.push(cc);
    //console.log("this.filterArray ",this.filterArray);
  


    this.queryParam = { "parent_id":e.target.value  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      
      //console.log("response of api"+response);
      this.apiResponse = response; 
      //this.allVersions = this.apiResponse.all_version;
      if(this.apiResponse.error == false)
      {
        this.allSubModelListCar = this.apiResponse.record;
        // if(this.apiResponse.m_name_rec)
        // {
        //   this.model_name_new = this.apiResponse.m_name_rec.model_name;
        // }
        
        //console.log("response of api apiResponse ", this.apiResponse);
      }
    });
    
  }
  removeModel(event:any)
  {
    if(event ==  "carModelArray" )
    {
      
      this.model_heading="Modèle";
      this.carModelArray = "";
      this.radioStatus = false;
      this.radio_model_heading = false;
      console.log("this.radioStatus ",this.radioStatus);
    }

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  onGenerationChange(e:any,f:any)
  {
    console.log("response of api e "+e);
    console.log("response of api f "+f);
    this.model_variant_heading = f;
    this.radio_model_variant = true;
    this.queryParam = { "parent_id":f  };
    this._http.post(this.base_url_node+"allVersionSubmodel",this.queryParam).subscribe((response:any)=>{
      
      //console.log("response of api"+response);
      this.apiResponse = response; 
      //
      if(this.apiResponse.error == false)
      {
        this.allVersions = this.apiResponse.all_version;
        //console.log("response of api apiResponse ", this.apiResponse);
      }
    });


    let cc = {
      paramName:"model_variant",
      paramValue:f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "model_variant")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  
  removeGeneration(event:any)
  {
    if(event ==  "model_variant" )
    {
      this.model_variant_heading="Génération";
      this.form.value.model_variant = "";
      this.radio_model_variant = false;
    }
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  onVersionChange(e:any,f:any)
  {
    this.carVersion_heading = f;
    this.radio_carVersion = true;
    let cc = {
      paramName:"carVersion",
      paramValue:f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "carVersion")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  removecarVersion(event:any)
  {
    if(event ==  "carVersion" )
    {
      this.carVersion_heading="Version";
      this.form.value.carVersion = "";
      this.radio_carVersion = false;
    }
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  removeChahis(event:any)
  {
    if(event ==  "carVersion" )
    {
      this.form.value.carVersion = "";

      this.chahis_heading = "Type de châssis";  
      this.radio_chahis = false;
      this.chahisArray = "";

    }
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  removeValFilter(event:any)
  {
    console.log("event --->>>> ",event);
    if(event ==  "carModelArray" )
    {
      this.carModelArray = "";
      this.radioStatus = false;
      console.log("this.radioStatus ",this.radioStatus);
    }
    if(event ==  "model_variant" )
    {
      this.form.value.model_variant = "";
    }
    if(event ==  "chahisArray" )
    {
      this.chahisArray = "";
    }
    if(event ==  "type_of_gearbox" )
    {
      this.form.value.type_of_gearbox = "";
    }
    if(event ==  "yearArray_2" )
    {
      this.yearArray_2 = 0;
    }
    if(event ==  "minKilometer" )
    {
      this.form.value.minKilometer= "";
    }
    if(event ==  "maxKilometer" )
    {
      this.form.value.maxKilometer= "";
    }
    if(event ==  "fuel" )
    {
      this.form.value.fuel = "";
    }
    if(event ==  "colorId" )
    {
      this.form.value.colorId = "";
    }
    if(event ==  "colorInteriorId" )
    {
      this.form.value.colorInteriorId = "";
    }
    if(event ==  "pors_warranty" )
    {
      this.form.value.pors_warranty = "";
    }
    if(event ==  "warranty" )
    {
      this.form.value.warranty = "";
    }
    if(event ==  "maintenance_booklet" )
    {
      this.form.value.maintenance_booklet = "";
    }
    if(event ==  "maintenance_invoice" )
    {
      this.form.value.maintenance_invoice = "";
    }
    if(event ==  "report_piwi" )
    {
      this.form.value.report_piwi = "";
    }
    if(event ==  "matching_number" )
    {
      this.form.value.matching_number = "";
    }
    if(event ==  "matching_color_paint" )
    {
      this.form.value.matching_color_paint = "";
    }
    if(event ==  "original_paint" )
    {
      this.form.value.original_paint = "";
    }
    
    if(event ==  "vendeurType" )
    {
      this.form.value.vendeurType = "";
    }
    
    if(event ==  "onlineSince" )
    {
      this.form.value.onlineSince = "";
    } 
    
    
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    console.log("removeAarray --->>>> ",removeAarray);
    this.filterArray = removeAarray;
  }
  removeType_of_gearbox(event:any)
  {
      this.form.value.carVersion = "";
      this.type_of_gearbox_heading = "Boîte de vitesse";  
      this.radio_type_of_gearbox = false;
      this.form.value.type_of_gearbox = ""; 

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_fuel(event:any)
  {
      this.fuel_heading = "Carburant";  
      this.radio_fuel = false;
      this.form.value.fuel = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_Color_exterior(event:any)
  {
    this.color_exterior_heading = "Couleur extérieur";  
    this.radio_color_exterior  = false;
    this.form.value.colorId = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_Color_interior(event:any)
  {
    this.color_interior_heading = "Couleur intérieur";  
    this.radio_color_interior  = false;
    this.form.value.colorInteriorId = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_porsWarranty(event:any)
  {
    this.pors_warranty_heading = "Garantie Porsche Approved";  
    this.radio_pors_warranty  = false;
    this.form.value.pors_warranty = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_warranty(event:any)
  {
    this.warranty_heading = "Garantie";  
    this.radio_warranty  = false;
    this.form.value.warranty = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_maintenance_booklet(event:any)
  {
    this.maintenance_booklet_heading = "Carnet d’entretien";  
    this.radio_maintenance_booklet  = false;
    this.form.value.maintenance_booklet = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_maintenance_invoice(event:any)
  {
    this.maintenance_invoice_heading = "Facture d’entretien";  
    this.radio_maintenance_invoice  = false;
    this.form.value.maintenance_invoice = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_report_piwi(event:any)
  {
    this.report_piwi_heading = "Rapport piwis";  
    this.radio_report_piwi  = false;
    this.form.value.report_piwi = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_matching_number(event:any)
  {
    this.matching_number_heading = "Matching numbers";  
    this.radio_matching_number  = false;
    this.form.value.matching_number = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_matching_color_paint(event:any)
  {
    this.matching_color_paint_heading = "Matching colors";  
    this.radio_matching_color_paint  = false;
    this.form.value.matching_color_paint = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_original_paint(event:any)
  {
    this.original_paint_heading = "Peinture d’origine";  
    this.radio_original_paint  = false;
    this.form.value.original_paint = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_onlineSince(event:any)
  {
    this.onlineSince_heading = "Annonce en ligne depuis";  
    this.radio_onlineSince  = false;
    this.form.value.onlineSince = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_Vendeur(event:any)
  {
     
    this.vendeurType_heading = "Vendeur";  
    this.radio_vendeurType  = false;
    this.form.value.vendeurType = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_yearArray(event:any)
  {
    this.onYearChange_heading = "Année";
    this.radio_onYearChange  = false;
    this.yearArray = 0;

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }

  remove_yearArray_2(event:any)
  {
     
    this.onYearChange_2_heading = "";
    this.radio_onYearChange_2  = false;
    this.yearArray_2 = 0;

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_minKilometer(event:any)
  {
    this.minKilometer_heading = "kilométrage";
    this.radio_minKilometer  = false;
    this.form.value.minKilometer = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_maxKilometer(event:any)
  {
    this.maxKilometer_heading = "";
    this.radio_maxKilometer  = false;
    this.form.value.maxKilometer = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_accessModel(event:any)
  {
    this.accessModel_heading = "Modèle";
    this.radio_accessModel  = false;
    this.form.value.accessModel = "";

    


    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_subModel(event:any)
  {
    this.subModel_heading = "Génération";
    this.radio_subModel  = false;
    this.form.value.subModel = "";
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_accessRegYear(event:any)
  { 

    this.accessRegYear_heading = "Année";
    this.radio_accessRegYear  = false;
    this.form.value.accessRegYear = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_accessRegYear_2(event:any)
  {
    this.accessRegYear_2_heading = "";
    this.radio_accessRegYear_2  = false;
    this.form.value.accessRegYear_2 = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArrayAccess.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArrayAccess = removeAarray;
    console.log("filterArrayAccess --->>>> ", this.filterArrayAccess);
  }
  remove_type_de_piece(event:any)
  {
    this.type_de_piece_heading = "Type de pièce";
    this.radio_type_de_piece  = false;
    this.form.value.type_de_piece = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArrayAccess.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArrayAccess = removeAarray;
    console.log("filterArrayAccess --->>>> ", this.filterArrayAccess);
  }
  remove_state(event:any)
  {
    this.state_heading = "État";
    this.radio_state  = false;
    this.form.value.state = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArrayAccess.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArrayAccess = removeAarray;
    console.log("filterArrayAccess --->>>> ", this.filterArrayAccess);
  }
  remove_OEM(event:any)
  {
    this.OEM_heading = "Pièce d'origine Porsche (OEM)";
    this.radio_OEM  = false;
    this.form.value.OEM = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArrayAccess.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArrayAccess = removeAarray;
    console.log("filterArrayAccess --->>>> ", this.filterArrayAccess);
  }

  removeValFilterAccess(event:any)
  {
    if(event ==  "accessModel" )
    {
      this.form.value.accessModel = "";
    } 
    if(event ==  "subModel" )
    {
      this.form.value.subModel = "";
    } 
    if(event ==  "accessRegYear_2" )
    {
      this.form.value.accessRegYear_2 = "";
    } 
    
    if(event ==  "accessRegYear" )
    {
      this.form.value.accessRegYear = "";
    } 
    if(event ==  "type_de_piece" )
    {
      this.form.value.type_de_piece = "";
    } 
    if(event ==  "state" )
    {
      this.form.value.state = "";
    } 
    if(event ==  "OEM" )
    {
      this.form.value.OEM = "";
    } 
      

    let removeAces_var = this.filterArrayAccess.filter((val)=>{
      if(event != val.paramName)
      {
        return val;
      }
    });
    this.filterArrayAccess = removeAces_var;
  }
  onColorChange(e:any,f:any)
  {
  this.color_exterior_heading = f;  
  this.radio_color_exterior  = true;

    let cc = {
      paramName:"colorId",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "colorId")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  onColorInteriorChange(e:any,f:any)
  {
    this.color_interior_heading = f;  
    this.radio_color_interior  = true;
    let cc = {
      paramName:"colorInteriorId",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "colorInteriorId")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  porsWarranty(f:any)
  {

    this.pors_warranty_heading = f;  
    this.radio_pors_warranty  = true;

    let cc = {
      paramName:"pors_warranty",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "pors_warranty")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  warrantyChange(f:any)
  {
    
    this.warranty_heading = f;  
    this.radio_warranty  = true;
    let cc = {
      paramName:"warranty",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "warranty")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  maintenanceBookletChange(f:any)
  {
    this.maintenance_booklet_heading = f;  
    this.radio_maintenance_booklet  = true;
    let cc = {
      paramName:"maintenance_booklet",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "maintenance_booklet")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  maintenanceInvoiceChange(f:any)
  {
    this.maintenance_invoice_heading = f;  
    this.radio_maintenance_invoice  = true;
    let cc = {
      paramName:"maintenance_invoice",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "maintenance_invoice")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  reportPiwiChange(f:any)
  {
    this.report_piwi_heading = f;  
    this.radio_report_piwi  = true;
    let cc = {
      paramName:"report_piwi",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "report_piwi")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  
  matchingNumberChange(f:any)
  {
    this.matching_number_heading = f;  
    this.radio_matching_number  = true;
    let cc = {
      paramName:"matching_number",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "matching_number")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  
  matching_color_paint_change(f:any)
  {
    
    this.matching_color_paint_heading = f;  
    this.radio_matching_color_paint  = true;
    let cc = {
      paramName:"matching_color_paint",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "matching_color_paint")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  original_paint_change(f:any)
  {
    
    this.original_paint_heading = f;  
    this.radio_original_paint  = true;
    let cc = {
      paramName:"original_paint",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "original_paint")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  vendeurType_change(f:any)
  {
    this.vendeurType_heading = f;  
    this.radio_vendeurType  = true;
    let cc = {
      paramName:"vendeurType",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "vendeurType")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  onYearChange_2(e:any,f:any) 
  {
    this.yearArray_2 = f;

    
    this.onYearChange_2_heading = f; 
    this.radio_onYearChange_2 = true;
    

    console.log("this.yearArray_2   -----   ", this.yearArray_2 );
    console.log("this.fffffffff   -----   ", f );

    let cc = {
      paramName:"yearArray_2",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "yearArray_2")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  onYearChange(e:any,f:any) 
  {
    this.onYearChange_heading = f;
    this.radio_onYearChange  = true;

    this.yearArray = f;
    console.log("this.yearArray -----   ", this.yearArray);
    let cc = {
      paramName:"yearArray",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "yearArray")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
    
  }
  changeMinKilo(e:any)
  {
    //minKilometer
    this.minKilometer_heading = e.target.value; 
    this.radio_minKilometer  = true;
    
    console.log("this.yearArray -----   ", e.target.value);
    let cc = {
      paramName:"minKilometer",
      paramValue: e.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "minKilometer")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  changeMaxKilo(e:any)
  {
    //maxKilometer
    this.maxKilometer_heading = e.target.value;  
    this.radio_maxKilometer  = true;

    console.log("this.yearArray -----   ", e.target.value);
    let cc = {
      paramName:"maxKilometer",
      paramValue: e.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "maxKilometer")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  changeOnlineSince(e:any)
  {
    
    this.radio_onlineSince  = true;
    console.log("changeOnlineSince -----   ", e.target.value);
    let j = "1 jour";
    if(e.target.value == 1)
    {
      j = "1 jour";
    }else{
      j = e.target.value+" jours";
    }
    this.onlineSince_heading = j;  
    let cc = {
      paramName:"onlineSince",
      paramValue: j
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "onlineSince")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  onFuelChange(e:any,f:any)
  {
    //maxKilometer

    
    this.fuel_heading = f;  
    this.radio_fuel = true;


    console.log("this.yearArray -----   ", e.target.value);
    let cc = {
      paramName:"fuel",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "fuel")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  onYearChangeAccess(e:any,f:any) { 
    this.accessRegYear_heading = f;
    this.radio_accessRegYear  = true;
    let cc = {
      paramName:"accessRegYear",
      paramValue: f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "accessRegYear")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);

  }

  onYearChangeAccess_2(e:any,f:any)
  { 
    this.accessRegYear_2_heading = f;
    this.radio_accessRegYear_2  = true;

    let cc = {
      paramName:"accessRegYear_2",
      paramValue: f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "accessRegYear_2")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);

  }

  onchahisChange(e:any,f:any) {
    //console.log(e.target.value);

    
  this.chahis_heading = f;  
  this.radio_chahis = true;

  this.chahisArray = e.target.value;
    let cc = {
      paramName:"chahisArray",
      paramValue:f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "chahisArray")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  onTypeGearBoxChange(e:any,f:any)
  {

    this.type_of_gearbox_heading = f; 
    this.radio_type_of_gearbox  = true;


    let cc = {
      paramName:"type_of_gearbox",
      paramValue:f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "type_of_gearbox")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  accessModelFun(eVal:any,f:any)
  {
    this.accessModel_heading = f;
    this.radio_accessModel  = true;
    let bb = {
      paramName:"accessModel",
      paramValue:f
    };
    let param2 = "accessModel";
    let ar_new = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != param2)
      {
        return val;
      }
    });
    this.filterArrayAccess = ar_new;
    this.filterArrayAccess.push(bb);

    this.queryParam = { "parent_id":eVal  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allSubModelList = this.apiResponse.record;
        //console.log("response of api allSubModelList "+this.allSubModelList);
      }
    });
  }
  onGenerationChangeAccess(e:any,f:any)
  {
    this.subModel_heading = f;
    this.radio_subModel  = true;
    let cc = {
      paramName:"subModel",
      paramValue:f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "subModel")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);
  }
  typeDePieceChange(e:any,f:any)
  {
    
    this.type_de_piece_heading = f;
    this.radio_type_de_piece  = true;
    let cc = {
      paramName:"type_de_piece",
      paramValue:f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "type_de_piece")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);
  }
  
  state_change(f:any)
  {
    this.state_heading = f;
    this.radio_state  = true;

    let cc = {
      paramName:"state",
      paramValue: f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "state")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);
  }
  oem_change(f:any)
  {
    this.OEM_heading = f;
    this.radio_OEM  = true;

    let cc = {
      paramName:"OEM",
      paramValue: f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "OEM")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);
  }

  
  zipCodeFunction(f:any)
  {
    console.log("val ", f.target.value);
    this.zip_code_heading = f.target.value;  
    this.radio_zip_code = true;
    let cc = {
      paramName:"zip_code",
      paramValue:f.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "zip_code")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  remove_zip_code(event:any)
  {
    this.zip_code_heading = "Code postal";
    this.radio_zip_code  = false;
    this.form.value.zip_code = "";
 
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
 
  cityFunction(f:any)
  {
    console.log("val ", f.target.value);
    this.city_heading = f.target.value;  
    this.radio_city = true;
    let cc = {
      paramName:"city",
      paramValue:f.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "city")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  remove_city(event:any)
  {
    this.city_heading = "Ville";
    this.radio_city  = false;
    this.form.value.city = "";
 
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
   
  countryFunction(f:any)
  {
    console.log("val ", f.target.value);
    this.country_heading = f.target.value;  
    this.radio_country = true;
    let cc = {
      paramName:"country",
      paramValue:f.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "country")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  remove_country(event:any)
  {
    this.country_heading = "Pays";
    this.radio_country  = false;
    this.form.value.country = "";
 
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }

  
  showSlide()
  {
    //console.log("heree");
    this.showDiv = true;
    this.showDivButton = false;
  }
  hideSlide()
  {
    this.showDiv = false;
    this.showDivButton = true;
  }
  categoryRadioBtn(categoryType:string)
  {
    //console.log(categoryType);
    this.categoryTypeForm = categoryType;
    if(categoryType == 'Porsche Voitures')
    {
      this.tab1Div = true;
      this.tab2Div = false;
    }else{
      this.tab1Div = false;
      this.tab2Div = true;
    }
    this.submit();
  }

  form = new UntypedFormGroup({
    //carModel: new UntypedFormControl('',[]),
    carVersion: new UntypedFormControl('',[]),
    report_piwi: new UntypedFormControl('',[]),
    fuel: new UntypedFormControl('',[]),
    type_of_gearbox: new UntypedFormControl('',[]),
    vehicle_condition: new UntypedFormControl('',[]),
    colorId: new UntypedFormControl('',[]),
    colorInteriorId: new UntypedFormControl('',[]),
    color_name: new UntypedFormControl('',[]),
    pors_warranty: new UntypedFormControl('', []),
    warranty: new UntypedFormControl('', []),
    maintenance_booklet: new UntypedFormControl('', []),
    maintenance_invoice: new UntypedFormControl('', []),
    accidented: new UntypedFormControl('', []),
    original_paint: new UntypedFormControl('', []),
    matching_number: new UntypedFormControl('', []),
    matching_color_paint: new UntypedFormControl('', []),
    matching_color_interior: new UntypedFormControl('', []),
    price_for_pors: new UntypedFormControl('', []),
    deductible_VAT: new UntypedFormControl('', []),
    minPrice: new UntypedFormControl('', []),
    maxPrice: new UntypedFormControl('', []),
    minKilometer: new UntypedFormControl('', []),
    onlineSince: new UntypedFormControl('', []),
    vendeurType: new UntypedFormControl('', []),
    maxKilometer: new UntypedFormControl('', []),
    model_variant: new UntypedFormControl('', []),
    yearArray: new UntypedFormControl('', []),
    yearArray_2: new UntypedFormControl('', []),
    accessModel: new UntypedFormControl('', []),
    subModel: new UntypedFormControl('', []),
    type_de_piece: new UntypedFormControl('', []),
    state: new UntypedFormControl('', []),
    OEM: new UntypedFormControl('', []),
    minAccessPrice: new UntypedFormControl('', []),
    maxAccessPrice: new UntypedFormControl('', []),
    accessRegYear_2: new UntypedFormControl('', []),
    accessRegYear: new UntypedFormControl('', []),
    address: new UntypedFormControl('', []),
    city: new UntypedFormControl('', []),
    country: new UntypedFormControl('', []),
    zip_code: new UntypedFormControl('', []),
    ad_name: new UntypedFormControl('', []),
    nearByKm: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }
  
  onClickMaintenance(mainten:any)
  {
    //console.log("474 "+mainten);
    this.maintenance_invoice = mainten;
  }
  fuelClick(fuelVal:any)
  {
    //console.log("474 "+fuelVal);
    this.fuel = fuelVal;
  }
  typeOfGearboxClick(geraBoxVal:any)
  {
    this.type_of_gearbox = geraBoxVal;
  }
  vehicleConditionClick(vehicle_conditionVal:any)
  {
    this.vehicle_condition = vehicle_conditionVal;
    //console.log("this.vehicle_condition "+this.vehicle_condition);
  }
  onClickWarranty(WarrantyVal:any)
  {
    this.warranty = WarrantyVal;
    //console.log("this.warranty "+this.warranty);
  }
  onClickMaintenanceBooklet(maintenance_bookletVal:any)
  {
    this.maintenance_booklet = maintenance_bookletVal;
    //console.log("this.maintenance_booklet "+this.maintenance_booklet);
  }
  onClickAccidented(accidentedVal:any)
  {
    this.accidented = accidentedVal;
    //console.log("this.accidented "+this.accidented);
  }
  onClickOriginalPaint(original_paintVal:any)
  {
    this.original_paint = original_paintVal;
    //console.log("this.original_paint "+this.original_paint);
    
  }
  onClickMatchingNumber(matching_numberVal:any)
  {
    this.matching_number = matching_numberVal;
    //console.log("this.matching_number "+this.matching_number);
    
  }
  onClickMatchingColorPaint(matching_color_paintVal:any)
  {
    this.matching_color_paint = matching_color_paintVal;
    //console.log("this.matching_color_paint "+this.matching_color_paint);
    
  }
  onClickMatchingColorInterior(matching_color_interiorVal:any)
  {
    this.matching_color_interior = matching_color_interiorVal;
    //console.log("this.matching_color_interior "+this.matching_color_interior);
    
  }
  onClickDeductibleVAT(deductible_VATVal:any)
  {
    this.deductible_VAT = deductible_VATVal;
    //console.log("this.deductible_VAT "+this.deductible_VAT);
    
  }
  onClickTypeDePiece(type_de_pieceVal:any)
  {
    this.type_de_piece = type_de_pieceVal;
    //console.log("this.type_de_piece "+this.type_de_piece);
    
  }
  onClickState(stateVal:any)
  {
    this.state = stateVal;
    //console.log("this.state "+this.state);
  }
  onClickOEM(OEMVal:any)
  {
    this.OEM = OEMVal;
    //console.log("this.OEM "+this.OEM);
  }

  sorting_data(e:any)
  {
    this.sorting_var = e.target.value;
    console.log("this.sorting_var ", this.sorting_var);
    
    this.queryParam = {
      "ad_name":this.ad_name,
      "userAddress":this.userAddress,
      "userLatitude":this.userLatitude,
      "userLongitude":this.userLongitude,
      'category': this.categoryTypeForm,
      'carModelArray':this.carModelArray,
      'yearArray':this.yearArray,
      'yearArray_2':this.yearArray_2,
      'chahisArray':this.chahisArray,
      'fuel':this.fuel,
      'type_of_gearbox':this.type_of_gearbox,
      'vehicle_condition':this.vehicle_condition,
      'colorId':this.colorId,
      'colorInteriorId':this.colorInteriorId,
      'warranty':this.warranty,
      'maintenance_booklet':this.maintenance_booklet,
      'maintenance_invoice':this.maintenance_invoice,
      'accidented':this.accidented,
      'original_paint':this.original_paint,
      'matching_number':this.matching_number,
      'matching_color_paint':this.matching_color_paint,
      'matching_color_interior':this.matching_color_interior,
      'deductible_VAT':this.deductible_VAT,
      'minPrice':this.minPrice,
      'maxPrice':this.maxPrice,
      'minKilometer':this.minKilometer,
      'maxKilometer':this.maxKilometer,
      'model_variant':this.model_variant,
      'accessModel':this.accessModel,
      'subModel':this.subModel,
      'yearArrayAccess':this.yearArrayAccess,
      'accessRegYear':this.accessRegYear,
      'accessRegYear_2':this.accessRegYear_2,
      'type_de_piece':this.type_de_piece,
      'state':this.state,
      'OEM':this.OEM,
      'minAccessPrice':this.minAccessPrice,
      'maxAccessPrice':this.maxAccessPrice,
      'user_id':this.user_id,
      'zip_code':this.zip_code,
      'country':this.country,
      'report_piwi':this.report_piwi,
      'city':this.city,
      'vendeurType':this.vendeurType,
      'color_name':this.color_name,
      'onlineSince':this.onlineSince,
      'nearByKm':this.nearByKm,
      'carVersion':this.carVersion,
      'pors_warranty':this.pors_warranty,
      'sorting_var':this.sorting_var
    };

    //console.log(this.queryParam);
    
    this._http.post(this.getWebProductList,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      this.allCarAd = this.apiResponse.record;
      if(this.apiResponse.error == false)
      {
        ///this.allTypeDePiece = this.apiResponse.record;
        //this.allCarAd = this.apiResponse.record;
      }
    });

  }
  submit(){
    //console.log("here");this.router.navigate([this.redirectionUrl]);
    //console.log("this.maintenance_invoice  "+this.maintenance_invoice);
    /*
   
    */
    this.fuel=this.form.value.fuel;  this.type_of_gearbox=this.form.value.type_of_gearbox;
    this.vehicle_condition=this.form.value.vehicle_condition;  this.pors_warranty=this.form.value.pors_warranty;
    this.warranty=this.form.value.warranty;  this.maintenance_booklet=this.form.value.maintenance_booklet;
    this.maintenance_invoice=this.form.value.maintenance_invoice;  this.accidented=this.form.value.accidented;
    this.original_paint=this.form.value.original_paint;  this.matching_number=this.form.value.matching_number;
    this.matching_color_paint=this.form.value.matching_color_paint;  this.matching_color_interior=this.form.value.matching_color_interior;
    this.deductible_VAT=this.form.value.deductible_VAT;  this.minPrice=this.form.value.minPrice;
    this.maxPrice=this.form.value.maxPrice; this.minKilometer=this.form.value.minKilometer;
    this.maxKilometer=this.form.value.maxKilometer; this.model_variant=this.form.value.model_variant;
    this.accessModel=this.form.value.accessModel; this.subModel=this.form.value.subModel; 
    this.type_de_piece=this.form.value.type_de_piece;  this.state=this.form.value.state; 
      
    this.OEM=this.form.value.OEM;  this.minAccessPrice=this.form.value.minAccessPrice;
    this.maxAccessPrice=this.form.value.maxAccessPrice;
    this.yearArrayAccess=this.yearArrayAccess;
    this.ad_name=this.form.value.ad_name;
    this.userLatitude=this.userLatitude; this.userLongitude=this.userLongitude; 
    this.userAddress=this.userAddress;  this.carVersion=this.form.value.carVersion;
    this.nearByKm=this.form.value.nearByKm;  this.onlineSince=this.form.value.onlineSince;this.vendeurType=this.form.value.vendeurType; this.color_name=this.form.value.color_name;this.city=this.form.value.city;  this.report_piwi=this.form.value.report_piwi;
    this.country=this.form.value.country;  this.zip_code=this.form.value.zip_code;
    this.colorInteriorId=this.form.value.colorInteriorId; 
    this.colorId=this.form.value.colorId;  this.accessRegYear=this.form.value.accessRegYear;this.accessRegYear_2=this.form.value.accessRegYear_2;

    this.router.navigate(['.'], { relativeTo: this.route, queryParams: { 
      "ad_name":this.form.value.ad_name,
      "userAddress":this.userAddress,
      "userLatitude":this.userLatitude,
      "userLongitude":this.userLongitude,
      'category': this.categoryTypeForm,
      'carModelArray':this.carModelString,
      'yearArray':this.yearArrayString,
      'chahisArray':this.chahisArrayString,
      'fuel':this.fuel,
      'type_of_gearbox':this.type_of_gearbox,
      'vehicle_condition':this.vehicle_condition,
      'colorId':this.colorId,
      'warranty':this.warranty,
      'maintenance_booklet':this.maintenance_booklet,
      'maintenance_invoice':this.maintenance_invoice,
      'accidented':this.accidented,
      'original_paint':this.original_paint,
      'matching_number':this.matching_number,
      'matching_color_paint':this.matching_color_paint,
      'matching_color_interior':this.matching_color_interior,
      'deductible_VAT':this.deductible_VAT,
      'minPrice':this.minPrice,
      'maxPrice':this.maxPrice,
      'minKilometer':this.minKilometer,
      'maxKilometer':this.maxKilometer,
      'model_variant':this.model_variant,
      'accessModel':this.accessModel,
      'subModel':this.subModel,
      'yearArrayAccess':this.yearArrayAccess,
      'type_de_piece':this.type_de_piece,
      'state':this.state,
      'OEM':this.OEM,
      'minAccessPrice':this.minAccessPrice,
      'maxAccessPrice':this.maxAccessPrice,
      'zip_code':this.zip_code,
      'country':this.country,
      'report_piwi':this.report_piwi,
      'city':this.city,
      'vendeurType':this.vendeurType,
      'color_name':this.color_name,
      'onlineSince':this.onlineSince,
      'nearByKm':this.nearByKm,
      'carVersion':this.carVersion,
      'pors_warranty':this.pors_warranty,
      'colorInteriorId':this.colorInteriorId,
      'accessRegYear':this.accessRegYear,
      'accessRegYear_2':this.accessRegYear_2
     }, queryParamsHandling: 'merge'});

    

     

    //console.log("now button submit");
    //console.log(this.carModelArray);
    //console.log(this.categoryTypeForm);
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      
      this.queryParam = {
        "ad_name":this.ad_name,
        "userAddress":this.userAddress,
        "userLatitude":this.userLatitude,
        "userLongitude":this.userLongitude,
        'category': this.categoryTypeForm,
        'carModelArray':this.carModelArray,
        'yearArray':this.yearArray,
        'yearArray_2':this.yearArray_2,
        'chahisArray':this.chahisArray,
        'fuel':this.fuel,
        'type_of_gearbox':this.type_of_gearbox,
        'vehicle_condition':this.vehicle_condition,
        'colorId':this.colorId,
        'colorInteriorId':this.colorInteriorId,
        'warranty':this.warranty,
        'maintenance_booklet':this.maintenance_booklet,
        'maintenance_invoice':this.maintenance_invoice,
        'accidented':this.accidented,
        'original_paint':this.original_paint,
        'matching_number':this.matching_number,
        'matching_color_paint':this.matching_color_paint,
        'matching_color_interior':this.matching_color_interior,
        'deductible_VAT':this.deductible_VAT,
        'minPrice':this.minPrice,
        'maxPrice':this.maxPrice,
        'minKilometer':this.minKilometer,
        'maxKilometer':this.maxKilometer,
        'model_variant':this.model_variant,
        'accessModel':this.accessModel,
        'subModel':this.subModel,
        'yearArrayAccess':this.yearArrayAccess,
        'accessRegYear':this.accessRegYear,
        'accessRegYear_2':this.accessRegYear_2,
        'type_de_piece':this.type_de_piece,
        'state':this.state,
        'OEM':this.OEM,
        'minAccessPrice':this.minAccessPrice,
        'maxAccessPrice':this.maxAccessPrice,
        'user_id':this.user_id,
        'zip_code':this.zip_code,
        'country':this.country,
        'report_piwi':this.report_piwi,
        'city':this.city,
        'vendeurType':this.vendeurType,
        'color_name':this.color_name,
        'onlineSince':this.onlineSince,
        'nearByKm':this.nearByKm,
        'carVersion':this.carVersion,
        'pors_warranty':this.pors_warranty
      };

      //console.log(this.queryParam);
      
      this._http.post(this.getWebProductList,this.queryParam).subscribe((response:any)=>{
        this.apiResponse = response;
        //console.log(this.apiResponse);
        this.allCarAd = this.apiResponse.record;
        if(this.apiResponse.error == false)
        {
          ///this.allTypeDePiece = this.apiResponse.record;
          //this.allCarAd = this.apiResponse.record;
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
  @HostListener('window:scroll', [])
  onScroll(): void 
  {
    const triggerAt: number = 100; 
    /* perform an event when the user has scrolled over the point of 128px from the bottom */
    if (document.body.scrollHeight - (window.innerHeight + window.scrollY) < triggerAt) {
      //doSomething();
      console.log("in bottom");
      
      if(this.totalPageNumber > this.pageStart)
      {
         
        

        this.queryParam = {
          "ad_name":this.ad_name,
          "userAddress":this.userAddress,
          "userLatitude":this.userLatitude,
          "userLongitude":this.userLongitude,
          'category': this.categoryTypeForm,
          'carModelArray':this.carModelArray,
          'yearArray':this.yearArray,
          'yearArray_2':this.yearArray_2,
          'chahisArray':this.chahisArray,
          'fuel':this.fuel,
          'type_of_gearbox':this.type_of_gearbox,
          'vehicle_condition':this.vehicle_condition,
          'colorId':this.colorId,
          'colorInteriorId':this.colorInteriorId,
          'warranty':this.warranty,
          'maintenance_booklet':this.maintenance_booklet,
          'maintenance_invoice':this.maintenance_invoice,
          'accidented':this.accidented,
          'original_paint':this.original_paint,
          'matching_number':this.matching_number,
          'matching_color_paint':this.matching_color_paint,
          'matching_color_interior':this.matching_color_interior,
          'deductible_VAT':this.deductible_VAT,
          'minPrice':this.minPrice,
          'maxPrice':this.maxPrice,
          'minKilometer':this.minKilometer,
          'maxKilometer':this.maxKilometer,
          'model_variant':this.model_variant,
          'accessModel':this.accessModel,
          'subModel':this.subModel,
          'yearArrayAccess':this.yearArrayAccess,
          'accessRegYear':this.accessRegYear,
          'accessRegYear_2':this.accessRegYear_2,
          'type_de_piece':this.type_de_piece,
          'state':this.state,
          'OEM':this.OEM,
          'minAccessPrice':this.minAccessPrice,
          'maxAccessPrice':this.maxAccessPrice,
          'user_id':this.user_id,
          'zip_code':this.zip_code,
          'country':this.country,
          'report_piwi':this.report_piwi,
          'city':this.city,
          'vendeurType':this.vendeurType,
          'color_name':this.color_name,
          'onlineSince':this.onlineSince,
          'nearByKm':this.nearByKm,
          'carVersion':this.carVersion,
          'pors_warranty':this.pors_warranty,
          "pageNo":this.pageNo,
          "sorting_var":this.sorting_var,
        };
  
        //console.log(this.queryParam);
        console.log("this.api_response_var "+this.api_response_var)
        if(this.api_response_var == 0)
        {
          this.api_response_var = 1;
          this._http.post(this.getWebProductList,this.queryParam).subscribe((response:any)=>{
            
            this.apiResponse = response;
            //console.log(this.apiResponse);
              this.api_response_var = 0;
            if(this.apiResponse.error == false)
            {
              // this.allCarAdNew  .push(this.allCarAd);
              // console.log("this.allCarAd");

              // const fruits = ["Banana", "Orange", "Apple", "Mango"];
              // fruits.includes("Mango");
              
              // console.log(this.allCarAd);
              // console.log("this.allCarAdNew");
              // console.log(this.allCarAdNew);
              const fruits = this.apiResponse.record;
              fruits.map((object:any)=>{
                //console.log(object)
                this.allCarAd.push(object);

              });
              console.log("this.allCarAdNew");
              console.log(this.allCarAdNew);
              this.pageNo ++;
              //this.allCarAd = this.apiResponse.record;
              ///this.allTypeDePiece = this.apiResponse.record;
              //this.allCarAd = this.apiResponse.record;
            }
          });
        }
        


      } 
    }else{
      console.log("not in bottom");
    }
  }

  carAdFav(id=null,user_id=null,status:any)
  {
    // console.log("id "+id);
    // console.log("user_id "+user_id);
    // console.log("status "+status);
    let queryParam = {ad_id:id,user_id:user_id,status:status};
    this._http.post(this.adUserFavAdd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.location.reload();
      }
    });
  }
  AccessAdFav(id=null,user_id=null,status:any)
  {
    // console.log("id "+id);
    // console.log("user_id "+user_id);
    // console.log("status "+status);
    let queryParam = {ad_id:id,user_id:user_id,status:status};
    this._http.post(this.adUserFavAdd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.location.reload();
      }
    });
  }
}
