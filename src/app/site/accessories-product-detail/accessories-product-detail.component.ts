import { Component, OnInit, ViewChild } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';


@Component({
  selector: 'app-accessories-product-detail',
  templateUrl: './accessories-product-detail.component.html',
  styleUrls: ['./accessories-product-detail.component.css']
})
export class AccessoriesProductDetailComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;webSingleProduct:any;allBanner:any;allCategory:any;
  counterNum:number;getCategory:any;getWebHomeCarAd:any;allCarAd:any;allAccessAd:any;adUserFavAdd:any; getWebHomeAccessAd:any;ad_id:any;allCarAdSimilar:any;
  webAddToCart:any;cartApiResponse:any;webCheckItemInCart:any;ad_name:any;
  slides = [];ItemInCart:number;showNumberVar:boolean = true;
  hideNumberVar:boolean = false;
  followSellerUrl:any; followApires:any;
  apiResponse2:any
  modelArray:any
  slideConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "dots": false,
    "infinite": true,
    "autoplay" : true,
    "autoplaySpeed" : 1500,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      }
    ]
  };
  obj = {};

  imageObject:object [] = [];


  centerLatitude = 47.034577;
  centerLongitude = 1.156914;
  old_model_name:string="";old_model_variant:string=""; old_ad_name:string="";
  old_price:string = "";
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.webSingleProduct = this.base_url_node+"webSingleProduct";
    this.adUserFavAdd = this.base_url_node+"adUserFavAdd";
    this.webAddToCart = this.base_url_node+"webAddToCart";
    this.webCheckItemInCart = this.base_url_node+"webCheckItemInCartApi";
    this.followSellerUrl = this.base_url_node+"followUnfollowSeller";
    
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    this.counterNum = 0;

    this.ad_id = this.actRoute.snapshot.params['id'];
    this.ItemInCart = 0;

    if(this.user_id != "" && this.user_id != null)
    {
      this.queryParam = {"user_id":this.user_id,"ad_id":this.ad_id};
      this._http.post(this.base_url_node+"updateAdViewCount",this.queryParam).subscribe((response:any)=>{
        console.log(response);
      });
    }

  }

  ngOnInit(): void {
    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          //console.log("userImage "+this.apiResponse.userRecord.userImage);
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
          
        }
      });
    }
    
    let queryParam = {ad_id:this.ad_id,user_id:this.user_id}
    this._http.post(this.webSingleProduct,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCarAd = this.apiResponse.record;
        this.modelArray = this.apiResponse.resultForModel;
        this.ad_name = this.allCarAd[0].ad_name;
        //console.log(this.allCarAd[0].location.coordinates[0]);
        //console.log(this.allCarAd[0].location.coordinates[1]);
        if(this.allCarAd[0].location)
        {
          //console.log("herrrrreeeeeeeee");
          //console.log(this.allCarAd[0].location.coordinates.length);
          if(this.allCarAd[0].location.coordinates.length > 0)
          {
            //console.log("herrrrreeeeeeeee");
            this.centerLatitude = parseFloat(this.allCarAd[0].location.coordinates[0]);
            this.centerLongitude = parseFloat(this.allCarAd[0].location.coordinates[1]);
            console.log(this.centerLatitude);
            console.log(this.centerLongitude);
            this.marker = {
              position: { lat: this.centerLatitude, lng: this.centerLongitude },
            }
            
          }
        }
        this.allCarAdSimilar = this.apiResponse.recordSimilar;
        
        
        if(this.allCarAd[0].accessoires_image.length > 0)
        {
         
          this.imageObject = this.allCarAd[0].accessoires_image;
        }
        console.log("this.allCarAd[0].accessoires_image  " , this.allCarAd[0].accessoires_image);
        console.log("this.imageObject ",this.imageObject);
        if(this.allCarAd[0].modelId.length > 0)
        {
          this.old_model_name = this.allCarAd[0].modelId[0].model_name
        }

        //objProduct.subModelId[0].model_name
        
        if(this.allCarAd[0].subModelId.length > 0)
        {
          this.old_model_variant = this.allCarAd[0].subModelId[0].model_name
        }

        //this.old_model_variant = this.allCarAd[0].model_variant;
        this.old_ad_name = this.allCarAd[0].ad_name;
        this.old_price = this.allCarAd[0].price;
        
        //objProduct.model_variant
        //objProduct.ad_name
      }
    });

    
    this.queryParam = {ad_id:this.ad_id,user_id:this.user_id}
    this._http.post(this.webCheckItemInCart,this.queryParam).subscribe((response:any)=>{
      //console.log("coart count "+JSON.stringify(response));
      if(response.error == false)
      {
        this.ItemInCart = response.record;
      }
    });

  }
  carAdFav(id=null,user_id=null,status:any)
  {
    //console.log("id "+id);
    //console.log("user_id "+user_id);
    //console.log("status "+status);
    let queryParam = {ad_id:id,user_id:user_id,status:status};
    this._http.post(this.adUserFavAdd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        
      }
    });

  }
  AccessAdFav(id=null,user_id=null,status:any)
  {
    //console.log("id "+id);
    //console.log("user_id "+user_id);
    //console.log("status "+status);
    let queryParam = {ad_id:id,user_id:user_id,status:status};
    this._http.post(this.adUserFavAdd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        
      }
    });
  }

  
  mapOptions: google.maps.MapOptions = {
    center: { lat: this.centerLatitude, lng: this.centerLongitude },
    zoom : 4
  }
  marker = {
    position: { lat: this.centerLatitude, lng: this.centerLongitude },
  }
  

  addTocart(cart_ad_id=null,looged_in_user=null)
  {
    //console.log("cart_ad_id "+cart_ad_id);
    //console.log("looged_in_user "+looged_in_user);
    this.queryParam = {"ad_id":cart_ad_id,"user_id":looged_in_user};
    this._http.post(this.webAddToCart,this.queryParam).subscribe((response:any)=>{
      //console.log(response);
      this.cartApiResponse = response;
      this.queryParam = {ad_id:this.ad_id,user_id:this.user_id}
      this._http.post(this.webCheckItemInCart,this.queryParam).subscribe((response:any)=>{
        //console.log("coart count "+JSON.stringify(response));
        if(response.error == false)
        {
          this.ItemInCart = response.record;
        }
      });

    });
  }

  openModalPics()
  {
    //console.log("hereeeeeee");
  }
  showNumber()
  {
    
    this.showNumberVar = false;
    this.hideNumberVar = true;
    // console.log("showNumberq");
    // console.log(this.user_id);
    if(this.user_id != "" && this.user_id != null)
    {
      this.queryParam = {"user_id":this.user_id,"ad_id":this.ad_id};
      this._http.post(this.base_url_node+"updateAccessNumberCount",this.queryParam).subscribe((response:any)=>{
        console.log(response);
      });
    }
  }
  hideNumber()
  {
    this.showNumberVar = true;
    this.hideNumberVar = false;
  }

  followSeller(eve:any){
    
    // console.log(eve.userId._id)
    // console.log(this.user_id)

    const parms={
      "user_id":this.user_id,
	    "seller_id":eve.userId._id
    }

    this._http.post(this.followSellerUrl, parms).subscribe(res=>{
      this.followApires =  res;
      console.log(this.followApires);
      this.apiResponse2 = this.followApires;
      if(this.followApires.error == false){
        setTimeout(() => {
          window.location.reload()
        }, 2000);
      }
    })

    
  }
}
