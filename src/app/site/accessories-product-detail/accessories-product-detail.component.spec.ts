import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesProductDetailComponent } from './accessories-product-detail.component';

describe('AccessoriesProductDetailComponent', () => {
  let component: AccessoriesProductDetailComponent;
  let fixture: ComponentFixture<AccessoriesProductDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesProductDetailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesProductDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
