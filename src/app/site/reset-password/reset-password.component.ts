import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webResetPasswordSubmit:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;
  gender:any;     date_of_birth:any;
  webUserEditProfileSubmit:any;
  inputType:string="password"; inputPosition:number=1;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService) {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webResetPasswordSubmit = this.base_url_node+"webResetPasswordSubmit";

    this.user_id = this.actRoute.snapshot.params['id'];

   }

  ngOnInit(): void {
  }
  form = new UntypedFormGroup({
    user_id: new UntypedFormControl('',[]),
    password: new UntypedFormControl('', [ Validators.required, Validators.minLength(8), Validators.maxLength(50) ]),
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }
  eyeClick()
  {
    if(this.inputPosition == 1)
    {
      this.inputPosition = 2;
      this.inputType="text";
    }else{
      this.inputPosition = 1;
      this.inputType="password";
    }
  }
  submit(){
    //console.log("here");
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);

      this._http.post(this.webResetPasswordSubmit,this.formValue).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          localStorage.clear();
          setTimeout(() => {
            window.location.href = this.base_url+"/login";
          }, 3000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
