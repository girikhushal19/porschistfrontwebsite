import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-mobile-header',
  templateUrl: './mobile-header.component.html',
  styleUrls: ['./mobile-header.component.css']
})
export class MobileHeaderComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;getBannerHomePage:any;allBanner:any;allCategory:any;
  getCategory:any;autocompleteOption:any
  autocompleteURl:any
  mySuggestion:any; autoCompleteData:any
  public countries = [
    {
      id: 1,
      name: 'Audi',
    },
    {
      id: 2,
      name: 'BMW',
    },
    {
      id: 3,
      name: 'Benz',
    },
    {
      id: 4,
      name: 'Toyota',
    },
    {
      id: 5,
      name: 'Paris',
    },
    {
      id: 6,
      name: 'France',
    }
  ];
  keyword = '_id';
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.getBannerHomePage = this.base_url_node+"getBannerHomePage";
    this.getCategory = this.base_url_node+"getCategory";
    this.autocompleteURl = this.base_url_node+"webHeaderAutoSuggestion";
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    //  console.log(this.token);
    //  console.log(this.user_id);
      ///console.log(this.user_type);
  }

  ngOnInit(): void {
    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          //console.log("loggedin_time "+this.apiResponse.userRecord.loggedin_time);
          const date = new Date();
          var hours = Math.abs(date.getTime() - new Date(this.apiResponse.userRecord.loggedin_time).getTime()) / 3600000;
          console.log("hours "+hours);
          if(hours > 24)
          {
            window.location.href = this.base_url+"logout";
          }
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
        }
      });
    }
  }

  selectEvent(item:any) {
    console.log(item.target.value)
  }
  

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e:any) {
    console.log(e.target.value)
  }

  onKeydownEvent(event:any){
   

    this.autocompleteOption={
     "advertisemnet_name":event.target.value
    }
    
    this._http.post(this.autocompleteURl, this.autocompleteOption).subscribe(res=>{
      this.mySuggestion = res
      this.autoCompleteData = this.mySuggestion.record

    })
  }

}
