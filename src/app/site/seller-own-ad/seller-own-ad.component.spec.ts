import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SellerOwnAdComponent } from './seller-own-ad.component';

describe('SellerOwnAdComponent', () => {
  let component: SellerOwnAdComponent;
  let fixture: ComponentFixture<SellerOwnAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SellerOwnAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SellerOwnAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
