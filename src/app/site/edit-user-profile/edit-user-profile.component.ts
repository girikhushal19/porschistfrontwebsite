import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-edit-user-profile',
  templateUrl: './edit-user-profile.component.html',
  styleUrls: ['./edit-user-profile.component.css']
})
export class EditUserProfileComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;  userName:any; telephone:any;
  zip_code:any;whatsapp:any;
  city:any;
  country:any;

  gender:any;     date_of_birth:any;
  webUserEditProfileSubmit:any;
  address: string = '';
    userLatitude: string = '';
    userLongitude: string = '';

    myFiles:string [] = [];
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetFullUserProfile";

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }

    this.webUserEditProfileSubmit = this.base_url_node+"webUserEditProfileSubmit";
  }

  ngOnInit(): void {

    this.myFiles = [];

    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {

        this.zip_code = this.apiResponse.userRecord.zip_code;
        this.city = this.apiResponse.userRecord.city;
        this.country = this.apiResponse.userRecord.country;
        this.telephone = this.apiResponse.userRecord.telephone;
        this.whatsapp = this.apiResponse.userRecord.whatsapp;
        this.userName = this.apiResponse.userRecord.userName;

        //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
        this.firstName = this.apiResponse.userRecord.firstName;
        this.lastName = this.apiResponse.userRecord.lastName;
        this.user_email = this.apiResponse.userRecord.email;
        this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
        this.address = this.apiResponse.userRecord.address;
        this.gender = this.apiResponse.userRecord.gender;
        this.date_of_birth = this.apiResponse.userRecord.date_of_birth;
        this.userLatitude = this.apiResponse.userRecord.latitude;
        this.userLongitude = this.apiResponse.userRecord.longitude;
        //gender //address //latitude //longitude date_of_birth
        if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
        {
          this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
        }else{
          this.userImage = "assets/images/logo/user.png";
        }
        
      }
    });
  }

  handleAddressChange(address: any) {
    this.address = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);

    if(address)
    {
      if(address.address_components)
      {
        address.address_components.forEach((val:any)=>{
          if(val)
          {
            if(val.types)
            {
              if(val.types.length > 0)
              {
                for(let x=0; x<val.types.length; x++)
                {
                  console.log(val.types[x]);
                  if(val.types[x] == "postal_code")
                  {
                    this.zip_code = val.long_name;
                  }
                  if(val.types[x] == "country")
                  {
                    this.country = val.long_name;
                  }
                  if(val.types[x] == "locality")
                  {
                    this.city = val.long_name;
                  }
                }
              }
            }
          }
        })
      }
    }

  }

  form = new UntypedFormGroup({
    userName: new UntypedFormControl('', [Validators.required]),
    telephone: new UntypedFormControl('', []),
    whatsapp: new UntypedFormControl('', []),
    zip_code: new UntypedFormControl('', []),
    city: new UntypedFormControl('', []),
    country: new UntypedFormControl('', []),

    images: new UntypedFormControl('', []),
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required, Validators.email]),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    address: new UntypedFormControl('', [Validators.required]),
   // date_of_birth: new UntypedFormControl('', [Validators.required]),
    //gender: new UntypedFormControl('', [ ]),
    latitude: new UntypedFormControl('', []),
    longitude: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }
  onFileChange(event:any)
  {
    this.myFiles = [];
      for (var i = 0; i < event.target.files.length; i++)
      { 
          this.myFiles.push(event.target.files[i]);
      }
  }
  submit(){
    console.log("here");
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      console.log(this.formValue);

      //30/11/2000

      this.formData = new FormData(); 
      
      this.formData.append('whatsapp', this.form.value.whatsapp);
      this.formData.append('telephone', this.form.value.telephone);
      this.formData.append('firstName', this.form.value.firstName);
      this.formData.append('userName', this.form.value.userName);
      this.formData.append('zip_code', this.form.value.zip_code);
          this.formData.append('city', this.form.value.city);
          this.formData.append('country', this.form.value.country);

      this.formData.append('lastName', this.form.value.lastName);
      this.formData.append('email', this.form.value.email);
      this.formData.append('mobileNumber', this.form.value.mobileNumber);
      //this.formData.append('date_of_birth', this.form.value.date_of_birth);
      //this.formData.append('gender', this.form.value.gender);
      this.formData.append('address', this.address);
      this.formData.append('latitude', this.userLatitude);
      this.formData.append('longitude', this.userLongitude); 
      this.formData.append('edit_id', this.user_id); 
      //this.formData.append('file', this.images);
      for (var i = 0; i < this.myFiles.length; i++)
      { 
        this.formData.append("filename", this.myFiles[i]);
      }

      this._http.post(this.webUserEditProfileSubmit,this.formData).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          setTimeout(() => {
            // alert(this.user_id)
            //window.location.reload();
            window.location.href="/userProfile/"+this.user_id
          }, 2000);

        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


}
