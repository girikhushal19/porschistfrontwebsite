import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;
  gender:any;     date_of_birth:any;
  webUserChangePasswordSubmit:any;
  address: string = '';
    userLatitude: string = '';
    userLongitude: string = '';

    myFiles:string [] = [];
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService) { 

    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webForgotPasswordSubmit";
  }

  ngOnInit(): void {
  }

  form = new UntypedFormGroup({
    email: new UntypedFormControl('',[Validators.required, Validators.email]),
  });
  
  get f(){
    return this.form.controls;
  }
  

   


  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  submit(){
    //console.log("here");
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);

      this._http.post(this.webGetUserProfile,this.formValue).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {

          this.form.reset();
          setTimeout(() => {
            //window.location.href = this.base_url+"/login";
          }, 3000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

}
