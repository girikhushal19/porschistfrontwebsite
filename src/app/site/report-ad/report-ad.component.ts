import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-report-ad',
  templateUrl: './report-ad.component.html',
  styleUrls: ['./report-ad.component.css']
})
export class ReportAdComponent implements OnInit {

  base_url_node:any
  aboutusURL:any;webUserLoginSubmit:any;
  apiRes:any;user_id:any;
  formData:any;formValue:any;apiResponse:any;ad_id:any;apiResponse2:any;
  firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;

  constructor(private loginAuthObj:LoginauthenticationService,private _http:HttpClient,private router: Router,private actRoute: ActivatedRoute) { 
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.aboutusURL = this.base_url_node+"getcontactUs";
    this.webUserLoginSubmit = this.base_url_node+"adUserReport";
    
    this.ad_id =  this.actRoute.snapshot.params['id'];
    this.user_id = this.loginAuthObj.userLoggedInId();
    
    let queryParam = {"user_id":this.user_id};
    this._http.post(this.base_url_node+"webGetUserProfile",queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      console.log("user record " ,this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
        this.firstName = this.apiResponse.userRecord.firstName;
        this.lastName = this.apiResponse.userRecord.lastName;
        this.user_email = this.apiResponse.userRecord.email;
        this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
        console.log("userImage "+this.apiResponse.userRecord.userImage);
        
        
      }
    });


  }

  ngOnInit(): void {
    this.getAbout()
  }

  form = new UntypedFormGroup({
    user_id: new UntypedFormControl('', [ ]),
    ad_id: new UntypedFormControl('', []),
    reason: new UntypedFormControl('', [ ]),
    message: new UntypedFormControl('', [Validators.required]),

  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }


  getAbout(){
    this._http.get(this.aboutusURL).subscribe(res=>{
      this.apiRes= res;
      console.log("this.apiRes ",this.apiRes);
    })
  }
  submit(){
    if (this.form.valid)
    {
      console.log("submit here") ;
      this.formValue = this.form.value;
      this._http.post(this.webUserLoginSubmit,this.formValue).subscribe((response:any)=>{
      this.apiResponse2 = response;
      console.log(this.apiResponse2);
        if(this.apiResponse2.error == false)
        {
          setTimeout(()=>{
            window.location.reload();
          },2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
