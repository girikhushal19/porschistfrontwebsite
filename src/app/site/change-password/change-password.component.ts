import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
 
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import Validation from '../utils/validation';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;
  gender:any;     date_of_birth:any;
  webUserChangePasswordSubmit:any;
  address: string = '';
    userLatitude: string = '';
    userLongitude: string = '';

    myFiles:string [] = [];
    inputType:string="password"; inputPosition:number=1;
    inputType2:string="password"; inputPosition2:number=1;
    inputType3:string="password"; inputPosition3:number=1;
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetFullUserProfile";

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }

    this.webUserChangePasswordSubmit = this.base_url_node+"webUserChangePasswordSubmit";
  }

  ngOnInit(): void {

    this.form = new UntypedFormGroup({
      user_id: new UntypedFormControl('',[]),
      old_password: new UntypedFormControl('', [ Validators.required, Validators.minLength(8), Validators.maxLength(50) ]),
      confirmPassword: new UntypedFormControl('', [Validators.required]),
      new_password: new UntypedFormControl('', [ Validators.required, Validators.minLength(8), Validators.maxLength(50) ])
    },
    {
      validators: [Validation.match('new_password', 'confirmPassword')]
    });
  }


  form = new UntypedFormGroup({
    user_id: new UntypedFormControl('',[]),
    old_password: new UntypedFormControl('', [ Validators.required, Validators.minLength(8), Validators.maxLength(50) ]),
    new_password: new UntypedFormControl('', [ Validators.required, Validators.minLength(8), Validators.maxLength(50) ]),
    confirmPassword: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }
  

   


  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }
  
  eyeClick()
  {
    if(this.inputPosition == 1)
    {
      this.inputPosition = 2;
      this.inputType="text";
    }else{
      this.inputPosition = 1;
      this.inputType="password";
    }
  }
  eyeClick2()
  {
    if(this.inputPosition2 == 1)
    {
      this.inputPosition2 = 2;
      this.inputType2="text";
    }else{
      this.inputPosition2 = 1;
      this.inputType2="password";
    }
  }
  eyeClick3()
  {
    if(this.inputPosition3 == 1)
    {
      this.inputPosition3 = 2;
      this.inputType3="text";
    }else{
      this.inputPosition3 = 1;
      this.inputType3="password";
    }
  }
  submit(){
    //console.log("here");
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      //console.log(this.formValue);

      this._http.post(this.webUserChangePasswordSubmit,this.formValue).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          localStorage.clear();
          setTimeout(() => {
            window.location.href = this.base_url+"/login";
          }, 3000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


}
