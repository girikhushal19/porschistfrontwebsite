import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdNormalUserTopUrgentComponent } from './ad-normal-user-top-urgent.component';

describe('AdNormalUserTopUrgentComponent', () => {
  let component: AdNormalUserTopUrgentComponent;
  let fixture: ComponentFixture<AdNormalUserTopUrgentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdNormalUserTopUrgentComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdNormalUserTopUrgentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
