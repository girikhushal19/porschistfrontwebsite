import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-get-cart',
  templateUrl: './get-cart.component.html',
  styleUrls: ['./get-cart.component.css']
})
export class GetCartComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;
  webUserLoginSubmit:any;
  formData:any;formValue:any;
  apiResponse:any;
  apiResponseAddress:any;
  token:any; user_id:any; user_type:any;
  queryParam:any;
  webGetUserProfile:any;
  firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;webAddDeliveryAddress:any;allBanner:any;allCategory:any;
   getCategory:any;getWebHomeCarAd:any;allCarAd:any;allCartAccessAd:any;webSubmitCart:any; getWebHomeAccessAd:any;ad_id:any;allCarAdSimilar:any;webGetDeliveryAddress:any;
   webGetUserCart:any;cartApiResponse:any;webCheckItemInCart:any;allDeliveryAddress:any;
   total_product_price:number;total_shipping_price:number;final_price:number;
   address: string = '';
    userLatitude: string = '';
    userLongitude: string = '';webRemoveCartItem:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.webAddDeliveryAddress = this.base_url_node+"webAddDeliveryAddress";
    this.webSubmitCart = this.base_url_node+"webSubmitCart";
    this.webGetUserCart = this.base_url_node+"webGetUserCart";
    this.webCheckItemInCart = this.base_url_node+"webCheckItemInCartApi";
    this.webGetDeliveryAddress = this.base_url_node+"webGetDeliveryAddress";
    this.webRemoveCartItem = this.base_url_node+"webRemoveCartItem";
    
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }
    this.total_product_price=0;this.total_shipping_price=0;this.final_price=0;   
     
  }

  ngOnInit(): void {
    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          //console.log("userImage "+this.apiResponse.userRecord.userImage);
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
          
        }
      });
    }

    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserCart,this.queryParam).subscribe((response:any)=>{
      if(response.error == false)
      {
        this.allCartAccessAd = response.record;
        this.total_product_price = response.total_product_price;
        this.total_shipping_price = response.total_shipping_price;
        this.final_price = response.final_price;
      }
    });


    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetDeliveryAddress,this.queryParam).subscribe((response:any)=>{
      if(response.error == false)
      {
        this.allDeliveryAddress = response.record;
      }
    });


  }
  deleteCartItem(cart_id:any)
  {
    console.log(cart_id);
    this.queryParam = {cart_id:cart_id};
    this._http.post(this.webRemoveCartItem,this.queryParam).subscribe((response:any)=>{
      //console.log(response);
      this.apiResponseAddress = response;
      if(this.apiResponseAddress  )
      {
        this.queryParam = {"user_id":this.user_id};
        this._http.post(this.webGetUserCart,this.queryParam).subscribe((response:any)=>{
          if(response.error == false)
          {
            this.allCartAccessAd = response.record;
            this.total_product_price = response.total_product_price;
            this.total_shipping_price = response.total_shipping_price;
            this.final_price = response.final_price;
          }
        });
      }
    });
  }
  form = new UntypedFormGroup({
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    user_id: new UntypedFormControl('', []),
    payment_type: new UntypedFormControl('', [Validators.required]),
    address_id: new UntypedFormControl('', [Validators.required]),
    
  });
  
  get f(){
    return this.form.controls;
  }
  
  formAddress = new UntypedFormGroup({
    user_id: new UntypedFormControl('', []),
    address: new UntypedFormControl('', [Validators.required]),
    latitude: new UntypedFormControl('', []),
    longitude: new UntypedFormControl('', []),
  });
  
  get formAddressFun(){
    return this.formAddress.controls;
  }
  handleAddressChange(address: any) {
    this.address = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);
  }
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  submit(){
    //console.log("here");
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      //let valuess = {"otp":this.enteredOtp,"user_id":this.user_id}
      this._http.post(this.webSubmitCart,this.formValue).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          setTimeout(() => {
            //window.location.href = this.base_url+"webaccessoiresPurchase?paymentId="+this.apiResponse.order_id;

            window.location.href = this.base_url+"checkout";
            
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }

  submitAddress(){
    //console.log("hereeeeeeeeeeeee");
    if (this.formAddress.valid)
    {
      this.formValue = this.formAddress.value;
      //let valuess = {"otp":this.enteredOtp,"user_id":this.user_id}
      this._http.post(this.webAddDeliveryAddress,this.formValue).subscribe((response:any)=>{
      this.apiResponseAddress = response;
      console.log(this.apiResponseAddress);
        if(this.apiResponseAddress.error == false)
        {
          setTimeout(() => {
            window.location.reload();
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.formAddress); 
      // validate all form fields
    }
  }

}
