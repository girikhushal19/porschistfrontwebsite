import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProPlanRenewalSuccessComponent } from './pro-plan-renewal-success.component';

describe('ProPlanRenewalSuccessComponent', () => {
  let component: ProPlanRenewalSuccessComponent;
  let fixture: ComponentFixture<ProPlanRenewalSuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProPlanRenewalSuccessComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ProPlanRenewalSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
