import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;webAddDeliveryAddress:any;allBanner:any;allCategory:any;
   getCategory:any;getWebHomeCarAd:any;allCarAd:any;allCartAccessAd:any;webCheckout:any; getWebHomeAccessAd:any;ad_id:any;allCarAdSimilar:any;webGetDeliveryAddress:any;
   webGetUserCart:any;cartApiResponse:any;webCheckItemInCart:any;allDeliveryAddress:any;
   total_product_price:number;total_shipping_price:number;final_price:number;
   address: string = '';payment_type:any;title:any;
    userLatitude: string = '';
    userLongitude: string = '';
    payment_method:any; paymentParams:any; paymentErr:any
    taxPercent:any; tax:any; subtotal:any
    self_pickup:any
    with_out_shipping_final_price:any
    item_owner_user_type:string="";price_red:string="step_2";
    constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
    { 
      this.base_url = this.loginAuthObj.base_url;
      this.base_url_node = this.loginAuthObj.base_url_node_admin;
      this.base_url_node_only = this.loginAuthObj.base_url_node;
      this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
      this.webGetUserCart = this.base_url_node+"webGetUserCart";
      this.webGetDeliveryAddress = this.base_url_node+"webGetDeliveryAddress";
      this.webCheckout = this.base_url_node+"webCheckout";
      
      this.token = this.loginAuthObj.userLogin();
      this.user_id = this.loginAuthObj.userLoggedInId();
      this.user_type = this.loginAuthObj.userLoggedInType();
      this.paymentErr =  true
      
  
      //  console.log(this.token);
      //  console.log(this.user_id);
      //  console.log(this.user_type);
      if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
      {
        window.location.href = this.base_url;
      }
      this.total_product_price=0;this.total_shipping_price=0;this.final_price=0;   
       
    }
    form = new UntypedFormGroup({
      //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      user_id: new UntypedFormControl('', []),
      checkbox_terms: new UntypedFormControl('', [Validators.required]),
    });
    
    get f(){
      return this.form.controls;
    }
    validateAllFormFields(formGroup: UntypedFormGroup) 
    {
      Object.keys(formGroup.controls).forEach(field => {
        const control = formGroup.get(field);
        if (control instanceof UntypedFormControl)
        {
          control.markAsTouched({ onlySelf: true });
        } else if (control instanceof UntypedFormGroup) 
        {
          this.validateAllFormFields(control);
        }
      });
    }

  ngOnInit(): void {

    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          //console.log("userImage "+this.apiResponse.userRecord.userImage);
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
          
        }
      });
    }

    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserCart,this.queryParam).subscribe((response:any)=>{
      console.log(response , "test")
      if(response.error == false)
      {
        //this.allCartAccessAd = response.record;
        this.total_product_price = response.total_product_price;
        this.total_shipping_price = response.total_shipping_price;
        this.final_price = response.final_price;
        this.payment_type = response.record[0].payment_type;
        this.title = response.record[0].ad_id[0].ad_name;
        this.taxPercent = response.tax_percentage
        this.tax = response.tax_price
        this.subtotal = response.sub_total
       this.self_pickup = response.self_pickup
       this.with_out_shipping_final_price = response.with_out_shipping_final_price
       this.item_owner_user_type = response.user_type;
       console.log("this.item_owner_user_type ", this.item_owner_user_type);

      }
    });


  }


  submit(){
    return
    //console.log("here");
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      console.log(this.formValue)
      
      //let valuess = {"otp":this.enteredOtp,"user_id":this.user_id}
      this._http.post(this.webCheckout,this.formValue).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          
          setTimeout(() => {
            var redirect_url_sdf = this.base_url_node_only+"api/webaccessoiresPurchase?paymentId="+this.apiResponse.order_id;
            console.log(redirect_url_sdf);
            window.location.href = redirect_url_sdf;
           // window.location.href = this.base_url+"checkout";
            
          }, 2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
  paymentmethod(eve:any){
    console.log(eve.target.value)
    this.payment_method = eve.target.value;
    if(this.payment_method == 'Paypal')
    {
      this.price_red = "step_2";
    }else{
      this.price_red = "step";
    }
    this.paymentErr =  true
  }

  checkoutProcess(){

    this.paymentParams = {
      "user_id":this.user_id,
      "payment_type":this.payment_method
    }

    console.log( this.paymentParams)
   if(this.payment_method == undefined){
    this.paymentErr =  false
   }else{
    this._http.post(this.webCheckout, this.paymentParams).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          
          setTimeout(() => {
            var redirect_url_sdf = this.base_url_node_only+"api/webaccessoiresPurchase?paymentId="+this.apiResponse.order_id;
            console.log("redirect_url_sdf");
            console.log(redirect_url_sdf);
            window.location.href = redirect_url_sdf;
           // window.location.href = this.base_url+"checkout";
            
          }, 2000);
        }
      });
   }

  }


}
