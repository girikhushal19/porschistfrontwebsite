import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-header-for-ad',
  templateUrl: './header-for-ad.component.html',
  styleUrls: ['./header-for-ad.component.css']
})
export class HeaderForAdComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;getBannerHomePage:any;allBanner:any;allCategory:any;
  getCategory:any; 
  cartCountURL:any; cartCount:any; cartCountval:any
  notFound:any;   old_title:any;record:any;
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.getBannerHomePage = this.base_url_node+"getBannerHomePage";
    this.getCategory = this.base_url_node+"getCategory";
    this.cartCountURL = this.base_url_node+"webGetUserCart";
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    //  console.log(this.token);
    //  console.log(this.user_id);
      console.log(this.user_type);


      this._http.get(this.base_url_node+"getHomeTitle/").subscribe((response:any)=>{
        console.log("getSingleModel"+JSON.stringify(response));
        if(response.error == false)
        { 
          this.record = response.record;
          this.old_title = this.record.title; 
          console.log("old_attribute_type"+this.old_title)
  
        }
        
      });


  }

  ngOnInit(): void {
    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          console.log("userImage "+this.apiResponse.userRecord.userImage);
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
        }
      });
      this._http.post(this.cartCountURL,this.queryParam).subscribe(res=>{
        this.cartCount = res
        this.cartCountval  = this.cartCount.record.length
       
      })
    }

    
  }

}