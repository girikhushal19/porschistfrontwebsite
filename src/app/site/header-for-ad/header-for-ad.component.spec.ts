import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderForAdComponent } from './header-for-ad.component';

describe('HeaderForAdComponent', () => {
  let component: HeaderForAdComponent;
  let fixture: ComponentFixture<HeaderForAdComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderForAdComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(HeaderForAdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
