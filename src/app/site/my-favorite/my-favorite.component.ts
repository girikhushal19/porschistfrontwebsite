import { Component, OnInit } from '@angular/core';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-my-favorite',
  templateUrl: './my-favorite.component.html',
  styleUrls: ['./my-favorite.component.css']
})
export class MyFavoriteComponent implements OnInit {
  base_url:any;base_url_node:any;base_url_node_only:any
  myfavURL:any; user_id:any;favOptions:any
  favRes:any;
  favitems:any
  adUserFavAdd:any
  apiResponse:any
  getWebAllUserFavAd:any; Favsellers:any; favSellerList:any
  followSellerUrl:any; followApires:any
  apiResponse2:any
  constructor(private _http:HttpClient,private loginAuthObj:LoginauthenticationService) { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.myfavURL = this.base_url_node+"getWebAllUserFavAd";
    this.adUserFavAdd = this.base_url_node+"adUserFavAdd";
    this.getWebAllUserFavAd = this.base_url_node+"getAdFollowedSeller";
    this.followSellerUrl = this.base_url_node+"followUnfollowSeller";

    this.user_id = localStorage.getItem("_id")
    this.favOptions={
      user_id :this.user_id
    }
  }

  ngOnInit(): void {
    this.getfavAds()

    this._http.post(this.getWebAllUserFavAd, this.favOptions).subscribe(res=>{
      this.Favsellers =  res
      if(this.Favsellers.success){
        this.favSellerList = this.Favsellers.errorMessage
        console.log("seller1 --->>>>> ", this.favSellerList)
      }
    })
  }

  getfavAds(){
    this._http.post(this.myfavURL, this.favOptions).subscribe(res=>{
      this.favRes =  res;
       
      this.favitems = this.favRes.data
      
      console.log("favitems ", this.favitems);
      
    })
  }
  
  carAdFav(id=null,user_id=null,status:any)
  {
    
    //console.log("id "+id);
    //console.log("user_id "+user_id);
    //console.log("status "+status);
    let queryParam = {ad_id:id,user_id:user_id,status:status};
   
    this._http.post(this.adUserFavAdd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.location.reload();
      }
    });

  }
  AccessAdFav(id=null,user_id=null,status:any)
  {
     
    //console.log("id "+id);
    //console.log("user_id "+user_id);
    //console.log("status "+status);
    let queryParam = {ad_id:id,user_id:user_id,status:status};
    
    this._http.post(this.adUserFavAdd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.location.reload();
      }
    });

  }

  followSeller(eve:any){
  
    const parms={
      "user_id":eve.user_id,
	    "seller_id":eve.seller_id
    }
    console.log(this.followSellerUrl, parms)

    this._http.post(this.followSellerUrl, parms).subscribe(res=>{
      this.followApires =  res
      console.log(this.followApires)
      if(this.followApires.error == false){
        this.apiResponse2 = this.followApires
        setTimeout(() => {
          window.location.reload()
        }, 2000);
       
      }
    })

    
  }

}
