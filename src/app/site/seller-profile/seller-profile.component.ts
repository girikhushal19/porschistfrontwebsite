import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators, UntypedFormBuilder } from '@angular/forms';
import { LoginauthenticationService } from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { GoogleMap, MapInfoWindow, MapMarker } from '@angular/google-maps';

@Component({
  selector: 'app-seller-profile',
  templateUrl: './seller-profile.component.html',
  styleUrls: ['./seller-profile.component.css']
})
export class SellerProfileComponent implements OnInit {
  base_url: any; base_url_node: any; base_url_node_only: any
  token: any; user_id: any; user_type: any; seller_id: any
  getUserApi: any; userRes: any; myuser: any; ad: any
  followSellerUrl: any; followApires: any; apiResponse: any;
  apiResponse2: any;
  is_follow:any;
  totalPageNumber: any; selectedIndex: number; numbers: any;
  constructor(private actRoute: ActivatedRoute, private _http: HttpClient,
    private loginAuthObj: LoginauthenticationService) {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.getUserApi = this.base_url_node + "webUserDetailAllAd";
    this.followSellerUrl = this.base_url_node + "followUnfollowSeller";


    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    this.seller_id = this.actRoute.snapshot.params['id'];
    this.selectedIndex = 0;
    

    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }

    
  }

  ngOnInit(): void {
    const param_count = {
      "user_id": this.seller_id
    }
    this._http.post(this.base_url_node + "webUserDetailAllAdCount", param_count).subscribe(res => {
      this.apiResponse = res
      console.log(this.apiResponse, "apiResponse")
      if (this.apiResponse.error == false) {
        this.totalPageNumber = this.apiResponse.record;
        this.numbers = Array(this.totalPageNumber).fill(0).map((m, n) => n);
      }
    })
    this.getallBanner(0);
  }
  getallBanner(numofpage = 0) {
    this.selectedIndex = numofpage;
    const params = {
      "user_id": this.seller_id,
      "loggedIn_user_id": this.user_id,
      "pageNo": numofpage
    }
    this._http.post(this.getUserApi, params).subscribe(res => {
      this.userRes = res
      console.log(this.userRes, "hdgfyh")
      if (this.userRes.error == false) 
      {
        console.log("this.userRes.allReadyLike ", this.userRes.allReadyLike);
        this.is_follow = this.userRes.allReadyLike;
        this.myuser = this.userRes.record
        this.ad = this.myuser[0].Ad
      }
    })
  }

  followSeller(eve: any) {

    const parms = {
      "user_id": this.user_id,
      "seller_id": eve._id
    }

    this._http.post(this.followSellerUrl, parms).subscribe(res => {
      this.followApires = res
      console.log(this.followApires)
      if (this.followApires.success == true) {
        this.apiResponse2 = this.followApires
        setTimeout(() => {
          window.location.reload()
        }, 2000);

      }
    })


  }
}
