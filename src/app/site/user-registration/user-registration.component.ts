import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators, UntypedFormBuilder, FormGroup } from '@angular/forms';
import { LoginauthenticationService } from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import Validation from '../utils/validation';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {


  base_url = ""; base_url_node = ""; base_url_node_only: any; webUserRegistrationSubmit: any;
  formData: any; formValue: any; apiResponse: any;
  pro_user: any; normal_user: any;
  address: string = '';
  userLatitude: string = '';
  userLongitude: string = '';
  normal: any
  pro: any
  //proForm !: FormGroup
  myusertype: any
  address2: any; userLatitude2: any; userLongitude2: any
  userCountry: any
  proUserAPI: any
  siren: any; notsiren: any
  confirmPasswordPro: any
  showError: any;
  siretError:string = "";
  userDocumentError:string = "";
  registration_numberError:string = "";
  registration_certificateError:string = "";

  myFiles:string [] = [];
  myFiles_rs:string [] = [];
  google_zip_code:any;google_city:string="";google_country:string="";
  genderVal:string="M";
  constructor(private _http: HttpClient, private formBuilder: UntypedFormBuilder, private loginAuthObj: LoginauthenticationService) {
    this.pro_user = "pro_user";
    this.normal_user = "normal_user";
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webUserRegistrationSubmit = this.base_url_node + "webUserRegistrationSubmit";
    this.proUserAPI = this.base_url_node + "proUserRegistration";
    this.normal = false
    this.pro = true
    this.siren = true
    this.notsiren = true
    this.showError = true
  }

  ngOnInit(): void {

    this.myFiles = [];
    this.myFiles_rs = [];
    this.form = this.formBuilder.group(
      {
        userName: new UntypedFormControl('', [Validators.required]),
        firstName: new UntypedFormControl('', [Validators.required]),
        lastName: new UntypedFormControl('', [Validators.required]),
        email: new UntypedFormControl('', [Validators.required, Validators.email]),
        password: new UntypedFormControl('', [Validators.required, Validators.minLength(8)]),
        confirmPassword: new UntypedFormControl('', [Validators.required]),
        mobileNumber: new UntypedFormControl('', [Validators.required]),
        whatsapp: new UntypedFormControl('', []),
        address: new UntypedFormControl('', [Validators.required]),
        latitude: new UntypedFormControl('', []),
        longitude: new UntypedFormControl('', []),
        zip_code: new UntypedFormControl('', [Validators.required]),
        city: new UntypedFormControl('', [Validators.required]),
        country: new UntypedFormControl('', [Validators.required]),
        termsCondition: new UntypedFormControl('', [Validators.required]),
        privacyPolicy: new UntypedFormControl('', [Validators.required]),
      },
      {
        validators: [Validation.match('password', 'confirmPassword')]
      }
    );
    this.proForm = new UntypedFormGroup({
      activity_description: new UntypedFormControl('', [Validators.required]),
      firstName: new UntypedFormControl('', [Validators.required]),
      lastName: new UntypedFormControl('', [Validators.required]),
      enterprise: new UntypedFormControl('', [Validators.required]),
      email: new UntypedFormControl('', [Validators.required, Validators.email]),
      //password: new UntypedFormControl('', [Validators.required, Validators.minLength(8)]),
      //confirmPassword_Pro: new UntypedFormControl('', [Validators.required]),
      mobileNumber: new UntypedFormControl('', [Validators.required]),
      siret: new UntypedFormControl('', [Validators.required]),
      userDocument: new UntypedFormControl('', []),
      address: new UntypedFormControl('', [Validators.required]),
      latitude: new UntypedFormControl('', []),
      longitude: new UntypedFormControl('', []),
      registration_number: new UntypedFormControl('', []),
      registration_certificate: new UntypedFormControl('', []),
      zip_code: new UntypedFormControl('', [Validators.required]),
      city: new UntypedFormControl('', [Validators.required]),
      country: new UntypedFormControl('', [Validators.required]),
      additional_information: new UntypedFormControl('', []),
      main_activity: new UntypedFormControl('', [ ]),
      website_url: new UntypedFormControl('', [ ]),
      //userName: new UntypedFormControl('', [Validators.required]),
      whatsapp: new UntypedFormControl('', [ ]),
      termsCondition: new UntypedFormControl('', [Validators.required]),
        privacyPolicy: new UntypedFormControl('', [Validators.required]),
        
        //gender: new UntypedFormControl('', []),
        ceo: new UntypedFormControl('', [Validators.required]),
        portable: new UntypedFormControl('', [ ]),
        area_code: new UntypedFormControl('', [Validators.required]),
    }
    // ,
    // {
    //   validators: [Validation.match('password', 'confirmPassword_Pro')]
    // }
    );
  }

  handleAddressChange(address: any) {
    this.address = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);
    console.log("address ",address);
    console.log("address ",address.address_components);
    if(address)
    {
      if(address.address_components)
      {
        address.address_components.forEach((val:any)=>{
          if(val)
          {
            if(val.types)
            {
              if(val.types.length > 0)
              {
                for(let x=0; x<val.types.length; x++)
                {
                  console.log(val.types[x]);
                  if(val.types[x] == "postal_code")
                  {
                    this.google_zip_code = val.long_name;
                  }
                  if(val.types[x] == "country")
                  {
                    this.google_country = val.long_name;
                  }
                  if(val.types[x] == "locality")
                  {
                    this.google_city = val.long_name;
                  }
                }
              }
            }
          }
        })
      }
    }
    
  }

 

  handleAddressChange2(address: any) {

    
    console.log(address)
    console.log(address.address_components)
    const countyname = address.address_components

    const test = countyname.filter((x: any) => {
      return x.types[0] == 'country'
    })
    if(test.length > 0)
    {
      this.userCountry = test[0].long_name;
    }
    

    if (this.userCountry == 'France') {
      this.siren = false;
      this.notsiren = true;

      
    } else {
      this.siren = true;
      this.notsiren = false;
       
    }

    this.address2 = address.formatted_address
    this.userLatitude2 = address.geometry.location.lat()
    this.userLongitude2 = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);

    console.log("address ",address);
    console.log("address ",address.address_components);
    if(address)
    {
      if(address.address_components)
      {
        address.address_components.forEach((val:any)=>{
          if(val)
          {
            if(val.types)
            {
              if(val.types.length > 0)
              {
                for(let x=0; x<val.types.length; x++)
                {
                  console.log(val.types[x]);
                  if(val.types[x] == "postal_code")
                  {
                    this.google_zip_code = val.long_name;
                  }
                  if(val.types[x] == "country")
                  {
                    this.google_country = val.long_name;
                  }
                  if(val.types[x] == "locality")
                  {
                    this.google_city = val.long_name;
                  }
                }
              }
            }
          }
        })
      }
    }

    
  }

  form = new UntypedFormGroup({

    userName: new UntypedFormControl('', [Validators.required]),
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    email: new UntypedFormControl('', [Validators.required, Validators.email]),
    password: new UntypedFormControl('', [Validators.required, Validators.minLength(8)]),
    confirmPassword: new UntypedFormControl('', [Validators.required]),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    whatsapp: new UntypedFormControl('', []),
    address: new UntypedFormControl('', [Validators.required]),
    latitude: new UntypedFormControl('', []),
    longitude: new UntypedFormControl('', []),
    zip_code: new UntypedFormControl('', [Validators.required]),
    city: new UntypedFormControl('', [Validators.required]),
    country: new UntypedFormControl('', [Validators.required]),
    termsCondition: new UntypedFormControl('', [Validators.required]),
  });
  proForm = new UntypedFormGroup({
    activity_description: new UntypedFormControl('', [Validators.required]),
    //userName: new UntypedFormControl('', [Validators.required]),
    whatsapp: new UntypedFormControl('', [ ]),
    additional_information: new UntypedFormControl('', []),
    firstName: new UntypedFormControl('', [Validators.required]),
    lastName: new UntypedFormControl('', [Validators.required]),
    enterprise: new UntypedFormControl('', [Validators.required]),
     
    //gender: new UntypedFormControl('', []),
    ceo: new UntypedFormControl('', [Validators.required]),
    portable: new UntypedFormControl('', [ ]),
    area_code: new UntypedFormControl('', [Validators.required]),


    email: new UntypedFormControl('', [Validators.required, Validators.email]),
    //password: new UntypedFormControl('', [Validators.required, Validators.minLength(8)]),
    //confirmPassword_Pro: new UntypedFormControl('', [Validators.required]),
    mobileNumber: new UntypedFormControl('', [Validators.required]),
    
    siret: new UntypedFormControl('', [Validators.required]),
    userDocument: new UntypedFormControl('', []),
    address: new UntypedFormControl('', [Validators.required]),
    latitude: new UntypedFormControl('', []),
    longitude: new UntypedFormControl('', []),
    registration_number: new UntypedFormControl('', []),
    registration_certificate: new UntypedFormControl('', []),
    zip_code: new UntypedFormControl('', [Validators.required]),
    city: new UntypedFormControl('', [Validators.required]),
    country: new UntypedFormControl('', [Validators.required]),
    //activity_principle: new UntypedFormControl('', [Validators.required]),
    main_activity: new UntypedFormControl('', [ ]),
    website_url: new UntypedFormControl('', [ ]),
  });
  onFileChange(event:any)
  {
    this.myFiles = [];
    for (var i = 0; i < event.target.files.length; i++)
    { 
        this.myFiles.push(event.target.files[i]);
    }
  }
  onFileChange_rs(event:any)
  {
    this.myFiles_rs = [];
    for (var i = 0; i < event.target.files.length; i++)
    { 
      this.myFiles_rs.push(event.target.files[i]);
    }
  }
  genderClick(val:any)
  {
    this.genderVal = val;
    console.log("genderVal ", this.genderVal);
  }
  get f() {
    return this.form.controls;
  }
  get g() {
    return this.proForm.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  submit() {
    console.log(this.form.valid)
    if (this.form.valid) {
      this.formValue = this.form.value;
      this.formValue.user_type = "normal_user"

      this._http.post(this.webUserRegistrationSubmit, this.formValue).subscribe((response: any) => {
        this.apiResponse = response;
        console.log(this.apiResponse);
        if (this.apiResponse.error == false) {
          setTimeout(() => {
            //window.location.reload();
            //
           // window.location.href = this.apiResponse.email_verify_url;
            window.location.href = this.base_url+"/successRegistration";
          }, 2000);
        }
      });
    } else {
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form);
      // validate all form fields
    }
  }

  proSubmit() {

    this.proForm.value.country = this.userCountry
    this.proForm.value.address = this.address2
    this.proForm.value.longitude = this.userLongitude2
    this.proForm.value.latitude = this.userLatitude2

    
    //  console.log("this.siretError" + this.siretError);
    //  console.log("this.siretError" + this.userDocumentError);
    //  console.log("this.siretError" + this.registration_numberError);
    //  console.log("this.siretError" + this.registration_certificateError);
    //  console.log("here 253");
     //console.log(this.proForm.value.password)


    //  if (this.proForm.value.password == this.confirmPasswordPro.value) {
    //    this.showError = true
       if (this.proForm.valid)
       {
          // if (this.userCountry == 'France')
          // {
          //   this.siretError = "";
          //   this.userDocumentError = "";
          //   this.registration_numberError = "";
          //   this.registration_certificateError = "";
          //   if(this.proForm.value.siret == "" && this.proForm.value.userDocument == "")
          //   {
          //     this.siretError = "Le SIREN est obligatoire.";
          //     this.userDocumentError = "Le KBIS est obligatoire.";
          //     return;
          //   }
          //   if(this.proForm.value.siret == "")
          //   {
          //     this.siretError = "Le SIREN est obligatoire.";
          //     return;
          //   }
          //   if(this.proForm.value.userDocument == "")
          //   {
          //     this.userDocumentError = "Le KBIS est obligatoire.";
          //     return;
          //   }
          // }else{
          //   this.siretError = "";
          //   this.userDocumentError = "";
          //   this.registration_numberError = "";
          //   this.registration_certificateError = "";
          //   if(this.proForm.value.registration_number == "" && this.proForm.value.registration_certificate == "")
          //   {
          //     this.registration_numberError = "Le numéro d'enregistrement est obligatoire.";
          //     this.registration_certificateError = "Le certificat d'enregistrement est obligatoire.";
          //     return;
          //   }
          //   if(this.proForm.value.registration_number == "")
          //   {
          //     this.registration_numberError = "Le numéro d'enregistrement est obligatoire.";
          //     return;
          //   }
          //   if(this.proForm.value.registration_certificate == "")
          //   {
          //     this.registration_certificateError = "Le certificat d'enregistrement est obligatoire.";
          //     return;
          //   }
          // }

          this.formData = new FormData(); 
          this.formData.append('additional_information', this.proForm.value.additional_information);
          this.formData.append('zip_code', this.proForm.value.zip_code);
          this.formData.append('city', this.proForm.value.city);
          this.formData.append('country', this.proForm.value.country);
          this.formData.append('whatsapp', this.proForm.value.whatsapp);
          this.formData.append('userName', this.proForm.value.userName);


          this.formData.append('firstName', this.proForm.value.firstName);
          this.formData.append('lastName', this.proForm.value.lastName);
          this.formData.append('enterprise', this.proForm.value.enterprise);
          this.formData.append('email', this.proForm.value.email);
          this.formData.append('mobileNumber', this.proForm.value.mobileNumber);
          this.formData.append('password', this.proForm.value.password); 
          this.formData.append('address', this.proForm.value.address);
          this.formData.append('latitude', this.proForm.value.latitude);
          this.formData.append('longitude', this.proForm.value.longitude);
          this.formData.append('siret', this.proForm.value.siret);
          this.formData.append('registration_number', this.proForm.value.registration_number);
           
          this.formData.append('main_activity', this.proForm.value.main_activity);
          this.formData.append('activity_description', this.proForm.value.activity_description);
          this.formData.append('website_url', this.proForm.value.website_url);
           
          this.formData.append('gender', this.genderVal);
          this.formData.append('ceo', this.proForm.value.ceo);
          this.formData.append('portable', this.proForm.value.portable);
          this.formData.append('area_code', this.proForm.value.area_code);

           

          
          
          //this.formData.append('file', this.images);
          for (var i = 0; i < this.myFiles.length; i++)
          { 
            this.formData.append("userDocument", this.myFiles[i]);
          } 
          for (var i = 0; i < this.myFiles_rs.length; i++)
          { 
            this.formData.append("registration_certificate", this.myFiles_rs[i]);
          } 
        this._http.post(this.proUserAPI, this.formData).subscribe(res => {
          this.apiResponse = res;
          console.log(this.apiResponse);
          if (this.apiResponse.error == false) {
            setTimeout(() => {
              // window.location.reload();
              //window.location.href = this.apiResponse.email_verify_url;
             
              window.location.href = this.base_url+"successProRegistration";
              
            }, 2000);
          }
  
        })
      } else {
  
        this.validateAllFormFields(this.proForm);
  
      }
    //  } else {
    //    this.showError = false
    //  }

    




  }

  showPro(eve: any) {
    this.myusertype = eve.target.value
    console.log(eve.target.value)
    this.normal = true
    this.pro = false
  }
  showNormal(eve: any) {
    this.myusertype = eve.target.value
    console.log(eve.target.value)
    this.normal = false
    this.pro = true
  }

}
