import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resend-verification-email',
  templateUrl: './resend-verification-email.component.html',
  styleUrls: ['./resend-verification-email.component.css']
})
export class ResendVerificationEmailComponent implements OnInit {

 

  base_url = "";base_url_node = "";base_url_node_only:any;webEmailOTPVerification:any;
  formData:any;formValue:any;apiResponse:any;user_id:any;queryParam:any;webResendOTPOnEmail:any;
  webGetUserProfile:any;firstName:any;lastName:any;user_email:any;enteredOtp:any;
  constructor(private actRoute: ActivatedRoute,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webResendOTPOnEmail = this.base_url_node+"webResendOTPOnEmail";
  }

  ngOnInit(): void {

  }

  form = new UntypedFormGroup({
    email: new UntypedFormControl('', [Validators.required]),
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  submit(){
    //console.log("here");
    if (this.form.valid)
    {
      
      this.formValue = this.form.value;
      this._http.post(this.webResendOTPOnEmail,this.form.value).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          this.form.reset();
          setTimeout(() => {
            //window.location.href = this.base_url+"login";
            
          }, 2000);
        }
      });
    }else{
      this.apiResponse = {};
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
  
  
}
