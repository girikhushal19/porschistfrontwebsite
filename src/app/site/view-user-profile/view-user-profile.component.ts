import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-view-user-profile',
  templateUrl: './view-user-profile.component.html',
  styleUrls: ['./view-user-profile.component.css']
})
export class ViewUserProfileComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any; userName:any; telephone:any;
  zip_code:any;user_whatsapp:any;
  city:any;
  country:any;
  gender:any; address:any;   date_of_birth:any;
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetFullUserProfile";

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }
  }

  ngOnInit(): void {
    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
        this.zip_code = this.apiResponse.userRecord.zip_code;
        this.city = this.apiResponse.userRecord.city;
        this.country = this.apiResponse.userRecord.country;
        
        this.telephone = this.apiResponse.userRecord.telephone;
        this.userName = this.apiResponse.userRecord.userName;
        this.firstName = this.apiResponse.userRecord.firstName;
        this.lastName = this.apiResponse.userRecord.lastName;
        this.user_email = this.apiResponse.userRecord.email;
        this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
        this.user_whatsapp = this.apiResponse.userRecord.whatsapp;
        this.address = this.apiResponse.userRecord.address;
        this.gender = this.apiResponse.userRecord.gender;
        this.date_of_birth = this.apiResponse.userRecord.date_of_birth;
        
        //gender //address //latitude //longitude date_of_birth
        if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
        {
          this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
        }else{
          this.userImage = "assets/images/logo/user.png";
        }
        
      }
    });
  }

}
