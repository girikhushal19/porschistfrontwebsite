import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdCarPostProfessionalComponent } from './ad-car-post-professional/ad-car-post-professional.component';
import { LoginComponent } from './login/login.component';
import { UserRegistrationComponent } from './user-registration/user-registration.component';
import { AdAccessoriesPostProfessionalComponent } from './ad-accessories-post-professional/ad-accessories-post-professional.component';
import { AdSuccessPageComponent } from './ad-success-page/ad-success-page.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { HomeComponent } from './home/home.component';
import { LogoutComponent } from './logout/logout.component';
import { ResendVerificationEmailComponent } from './resend-verification-email/resend-verification-email.component';
import { EditUserProfileComponent } from './edit-user-profile/edit-user-profile.component';
import { ViewUserProfileComponent } from './view-user-profile/view-user-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserAdProfessionalComponent } from './user-ad-professional/user-ad-professional.component';
import { EditCarAdPostComponent } from './edit-car-ad-post/edit-car-ad-post.component';
import { EditadAccessoriesComponent } from './editad-accessories/editad-accessories.component';
import { AdCarPostParticularComponent } from './ad-car-post-particular/ad-car-post-particular.component';

import { ProductListComponent } from './product-list/product-list.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { AccessoriesProductDetailComponent } from './accessories-product-detail/accessories-product-detail.component';
import { AdChatInitiateComponent } from './ad-chat-initiate/ad-chat-initiate.component';
import { AllMessageComponent } from './all-message/all-message.component';
import { GetCartComponent } from './get-cart/get-cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { UserPurchaseSuccessComponent } from './user-purchase-success/user-purchase-success.component';
import { AccessoriesPurchaseListComponent } from './accessories-purchase-list/accessories-purchase-list.component';
import { AdAccessoriesPostParticularComponent } from './ad-accessories-post-particular/ad-accessories-post-particular.component';
import { MyFavoriteComponent } from './my-favorite/my-favorite.component';
import { NotificationsComponent } from './notifications/notifications.component';

import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { AidePageComponent } from './aide-page/aide-page.component';
import { MyCartComponent } from './my-cart/my-cart.component';
import { MyCheckoutComponent } from './my-checkout/my-checkout.component';
import { ViewMyorderComponent } from './view-myorder/view-myorder.component';
import { BankInfoComponent } from './bank-info/bank-info.component';
import { AdNormalUserTopSearchComponent } from './ad-normal-user-top-search/ad-normal-user-top-search.component';
import { AdNormalUserTopUrgentComponent } from './ad-normal-user-top-urgent/ad-normal-user-top-urgent.component';
import { SellerProfileComponent } from './seller-profile/seller-profile.component';
import { AdNormalUserCarAdReactivateComponent } from './ad-normal-user-car-ad-reactivate/ad-normal-user-car-ad-reactivate.component';
import { AdNormalUserAcesAdReactivateComponent } from './ad-normal-user-aces-ad-reactivate/ad-normal-user-aces-ad-reactivate.component';
import { AdProUserSubscriptionReactivateComponent } from './ad-pro-user-subscription-reactivate/ad-pro-user-subscription-reactivate.component';
import { ProPlanRenewalSuccessComponent } from './pro-plan-renewal-success/pro-plan-renewal-success.component';
import { AdProUserAccessSubscriptionReactivateComponent } from './ad-pro-user-access-subscription-reactivate/ad-pro-user-access-subscription-reactivate.component';
import { ViewProUserProfileComponent } from './view-pro-user-profile/view-pro-user-profile.component';
import { EditProUserProfileComponent } from './edit-pro-user-profile/edit-pro-user-profile.component';
import { ReportAdComponent } from './report-ad/report-ad.component';
import { RatingAdComponent } from './rating-ad/rating-ad.component';
import { SuccessRegistrationComponent } from './success-registration/success-registration.component';
import { ReviresComponent } from './revires/revires.component';
import { ProfessionalRegistrationComponent } from './professional-registration/professional-registration.component';
import { SuccessProRegistrationComponent } from './success-pro-registration/success-pro-registration.component';
import { EmailProVerificationComponent } from './email-pro-verification/email-pro-verification.component'; 
import { SetNewPasswordComponent } from './set-new-password/set-new-password.component'; 

const routes: Routes = [
    {
      path : "",
      component : HomeComponent
    },
    {
      path : "adPost",
      component : AdCarPostProfessionalComponent
    },
    {
      path : "adPost/:id",
      component : AdCarPostProfessionalComponent
    },
    {
      path : "adPostParticular",
      component : AdCarPostParticularComponent
    },
    {
      path : "adPostParticular/:id",
      component : AdCarPostParticularComponent
    },
    
    {
      path : "login",
      component : LoginComponent
    },
    {
      path : "login/:callBackUrl",
      component : LoginComponent
    },
    {
      path : "registration",
      component : UserRegistrationComponent
    },
    {
      path : "userRegistration",
      component : ProfessionalRegistrationComponent
    },
    
    {
      path : "adAccessories/:id",
      component : AdAccessoriesPostProfessionalComponent
    },
    {
      path : "adAccessoriesParticular/:id",
      component : AdAccessoriesPostParticularComponent
    },
    
    {
      path : "adSuccess",
      component : AdSuccessPageComponent
    },
    {
      path : "proPlanRenewalSuccess",
      component : ProPlanRenewalSuccessComponent
    },
    {
      path : "emailVerification/:id",
      component : EmailVerificationComponent
    },
    {
      path : "emailProVerification/:id",
      component : EmailProVerificationComponent
    },
    {
      path : "setNewPassword/:id",
      component : SetNewPasswordComponent
    },
    {
      path : "userProfile/:id",
      component : UserProfileComponent
    },
    {
      path : "myProfile/:id",
      component : MyProfileComponent
    },
    {
      path : "resendVerificationCode",
      component : ResendVerificationEmailComponent
    },
    {
      path : "logout",
      component : LogoutComponent
    },
    {
      path : "editUserProfile/:id",
      component : EditUserProfileComponent
    },
    {
      path : "editProUserProfile/:id",
      component : EditProUserProfileComponent
    },
    {
      path : "viewUserProfile/:id",
      component : ViewUserProfileComponent
    },
    {
      path : "viewProUserProfile/:id",
      component : ViewProUserProfileComponent
    },
    
    {
      path : "changePassword/:id",
      component : ChangePasswordComponent
    },
    {
      path : "forgotPassword",
      component : ForgotPasswordComponent
    },
    {
      path : "resetPassword/:id",
      component : ResetPasswordComponent
    },
    {
      path : "adProfessionalUser",
      component : UserAdProfessionalComponent
    },
    {
      path : "editCarAdPost/:id",
      component : EditCarAdPostComponent
    },

    {
      path : "editAccessoriesAdPost/:id",
      component : EditadAccessoriesComponent
    },
    
    {
      path : "productList",
      component : ProductListComponent
    },
    {
      path : "productDetail/:id",
      component : ProductDetailComponent
    },
    {
      path : "accessoriesProductDetail/:id",
      component : AccessoriesProductDetailComponent
    },
    {
      path : "adChatInitiate/:id",
      component : AdChatInitiateComponent
    },
    {
      path : "viewOrder/:id",
      component : ViewMyorderComponent
    },
    {
      path : "allMessage",
      component : AllMessageComponent
    },
    {
      path : "getCart",
      component : GetCartComponent
    },
    {
      path : "my_cart",
      component : MyCartComponent
    },
    {
      path : "checkout",
      component : CheckoutComponent
    },
    {
      path : "my_checkout",
      component : MyCheckoutComponent
    },
    {
      path : "userPurchaseSuccess",
      component : UserPurchaseSuccessComponent
    },
    {
      path : "accessoriesPurchaseList",
      component : AccessoriesPurchaseListComponent
    },
    {
      path : "favorites",
      component : MyFavoriteComponent
    },
    {
      path : "notifications",
      component : NotificationsComponent
    },
    {
      path : "termsCondition",
      component : TermsConditionComponent
    },
    {
      path : "aboutUs",
      component : AboutUsComponent
    },
    {
      path : "contactUs",
      component : ContactUsComponent
    },
    {
      path : "privacyPolicy",
      component : PrivacyPolicyComponent
    },
    {
      path : "aide",
      component : AidePageComponent
    },
    {
      path : "allBanks",
      component : BankInfoComponent
    },
    {
      path : "adNormalUserTopSearch/:id",
      component : AdNormalUserTopSearchComponent
    },
    {
      path : "adNormalUserTopUrgent/:id",
      component : AdNormalUserTopUrgentComponent
    },
    {
      path : "adNormalUserAcesAdReactivate/:id",
      component : AdNormalUserAcesAdReactivateComponent
    },
    {
      path : "adNormalUserCarAdReactivate/:id",
      component : AdNormalUserCarAdReactivateComponent
    },
    {
      path : "seller/:id",
      component : SellerProfileComponent
    },
    {
      path : "subscriptionRenewal",
      component : AdProUserSubscriptionReactivateComponent
    },
    {
      path : "subscriptionAccessoriesRenewal",
      component : AdProUserAccessSubscriptionReactivateComponent
    },
    {
      path : "reportAd/:id",
      component : ReportAdComponent
    },
    {
      path : "ratingAd/:id",
      component : RatingAdComponent
    },
    {
      path : "successRegistration",
      component : SuccessRegistrationComponent
    },
    {
      path : "successProRegistration",
      component : SuccessProRegistrationComponent
    },
    {
      path : "review",
      component : ReviresComponent
      
    }
    
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }
