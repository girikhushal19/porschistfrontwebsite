import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  userAddress: string = ''; userLatitude: number = 0; userLongitude: number = 0;

  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;getBannerHomePage:any;allBanner:any;allCategory:any;
  counterNum:number;getCategory:any;getCategoryOnAdPage:any;getWebHomeCarAd:any;allCarAd:any;allAccessAd:any;adUserFavAdd:any; getWebHomeAccessAd:any;getWebHomeUserFavAd:any;allFavAd:any;
  adCounter:number = 0;
  showDiv:boolean = false;
  showDivButton:boolean = true;
  tab1Div:boolean = true;
  tab2Div:boolean = false;
  allCategoryList:any;allModel:any;allModelList:any;allAccessModelList:any;
  getSubAttribute:any;getColor:any;allColor:any;registration_year:any;allChasisType:any;allYear:any;allCarburant:any;allEtatDuVehicle:any;allBoiteDeVitesse:any; allColorInterior:any;
  allSubModelList:any; allSubModelListCar:any; allTypeDePiece:any;getWebHomeProductCount:any;
  cat1:any;cat2:any;
  cat1Heading:any;cat2Heading:any;
  categoryTypeForm:any;allWebSubModel:any;
  carModelArray:any;
  //yearArray:number [] = [];
  chahisArray:any;
  yearArray:Number = 0;  yearArray_2:Number = 0;
  colorArray:number [] = [];yearArrayAccess:number [] = [];
  redirectionUrl:string= "";
  
  filterArray:any [] = [];
  filterArrayAccess:any [] = [];
  slides = [
    ];
  slideConfig = {
    'cssEase': 'linear',
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "dots": false,
    "infinite": true,
    "autoplay" : true,
    "autoplaySpeed" : 1000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 767,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 1.5,
          slidesToScroll: 1,
        },
      }
    ]
  }; 
  allVersions:any; model_name_new:string="";
  radioStatus:boolean = false;
  model_heading:string="Modèle";  radio_model_heading:boolean = false;
  model_variant_heading:string="Génération";  radio_model_variant:boolean = false;
  carVersion_heading:string="Version";  radio_carVersion:boolean = false;
  chahis_heading:string="Type de châssis";  radio_chahis:boolean = false;
  type_of_gearbox_heading:string="Boîte de vitesse";  radio_type_of_gearbox:boolean = false;
  fuel_heading:string="Carburant";  radio_fuel:boolean = false;
  color_exterior_heading:string="Couleur extérieur";  radio_color_exterior:boolean = false;
  color_interior_heading:string="Couleur intérieur";  radio_color_interior:boolean = false;
  pors_warranty_heading:string="Garantie Porsche Approved";  radio_pors_warranty:boolean = false;
  warranty_heading:string="Garantie";  radio_warranty:boolean = false;
  maintenance_booklet_heading:string="Carnet d’entretien";  radio_maintenance_booklet:boolean = false;
  maintenance_invoice_heading:string="Facture d’entretien";  radio_maintenance_invoice:boolean = false;
  report_piwi_heading:string="Rapport piwis";  radio_report_piwi:boolean = false;
  matching_number_heading:string="Matching numbers";  radio_matching_number:boolean = false;
  matching_color_paint_heading:string="Matching colors";  radio_matching_color_paint:boolean = false;
  original_paint_heading:string="Peinture d’origine"; radio_original_paint:boolean = false;
  onlineSince_heading:string="Annonce en ligne depuis"; radio_onlineSince:boolean = false;
  vendeurType_heading:string="Vendeur"; radio_vendeurType:boolean = false;

  onYearChange_heading:string="Année";  radio_onYearChange:boolean = false;
  onYearChange_2_heading:string="";  radio_onYearChange_2:boolean = false;
  minKilometer_heading:string="kilométrage";  radio_minKilometer:boolean = false;
  maxKilometer_heading:string="";  radio_maxKilometer:boolean = false;

  accessModel_heading:string="Modèle";  radio_accessModel:boolean = false;
  subModel_heading:string="Modèle";  radio_subModel:boolean = false;

  accessRegYear_heading:string="Année";  radio_accessRegYear:boolean = false;
  accessRegYear_2_heading:string="";  radio_accessRegYear_2:boolean = false;
  type_de_piece_heading:string="Type de pièce";  radio_type_de_piece:boolean = false;
  state_heading:string="État";  radio_state:boolean = false;
  OEM_heading:string="Pièce d'origine Porsche (OEM)";  radio_OEM:boolean = false;
  
  zip_code_heading:any="Code postal";  radio_zip_code:boolean = false;
  city_heading:any="Ville";  radio_city:boolean = false;
  country_heading:any="Pays";  radio_country:boolean = false;
  cookie_not_set_yet:Boolean=true; old_cookie:string=""; ipAddress:any; 
  constructor(private cookie_Service: CookieService,private router: Router,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.getBannerHomePage = this.base_url_node+"getBannerHomePage";
    this.getCategory = this.base_url_node+"getCategory";
    this.getWebHomeCarAd = this.base_url_node+"getWebHomeCarAd";
    this.getWebHomeAccessAd = this.base_url_node+"getWebHomeAccessAd";
    this.adUserFavAdd = this.base_url_node+"adUserFavAdd";
    this.getWebHomeUserFavAd = this.base_url_node+"getWebHomeUserFavAd";
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    this.counterNum = 0;
    this.getCategoryOnAdPage = this.base_url_node+"getCategoryOnAdPage";
    this.allModel = this.base_url_node+"getModel";
    this.getSubAttribute = this.base_url_node+"getSubAttribute";
    this.registration_year = this.base_url_node+"registration_year";
    this.getColor = this.base_url_node+"getColor";
    this.allWebSubModel = this.base_url_node+"allWebSubModel";
    this.getWebHomeProductCount = this.base_url_node+"getWebHomeProductCount";
    this.categoryTypeForm = "Porsche Voitures";

    this._http.post(this.getWebHomeProductCount,{}).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        ///this.allTypeDePiece = this.apiResponse.record;
        //console.log(this.allModelList);
        this.adCounter = this.apiResponse.record;
      }
    });

    this._http.get(this.base_url_node+"getCookie/").subscribe((response:any)=>{
      console.log("getSingleModel"+JSON.stringify(response));
      if(response.error == false)
      {
        let cookie_rec = response.record;
 
        this.old_cookie = cookie_rec.title; 
        //console.log("old_attribute_type"+this.old_attribute_type)

      }
      
    });
    // this._http.get(this.base_url_node+"allVersions",{}).subscribe((response:any)=>{
    //   //console.log("response of api"+response);
    //   this.apiResponse = response;
    //   //console.log(this.apiResponse);
    //   if(this.apiResponse.error == false)
    //   {
    //     this.allVersions = this.apiResponse.record;
    //     //console.log("allVersions ",this.allVersions);
    //   }
    // });

 
    this.filterArray = []
    this.filterArrayAccess = []
    this.carModelArray = "";
    this.chahisArray= "";

    //this.cookie_Service.delete("Porschists_home","/","porschists.com",true,"None");
    

    let coke_val_first = this.cookie_Service.get("Porschists_home");
    console.log("coke_val_first ", coke_val_first);
    if(!coke_val_first)
    {
      //this.cookie_not_set_yet = false;
      //this.cookie_Service.set("Porschists_home","Porschists");
    }
    //let coke_val = this.cookie_Service.get("Porschists_home");
    //console.log("coke_val ", coke_val);
    this.getIPAddress();

  }
  getIPAddress()
  {
    this._http.get("http://api.ipify.org/?format=json").subscribe((res:any)=>{
      this.ipAddress = res.ip;
      console.log("this.ipAddress ", this.ipAddress);
      this._http.post(this.base_url_node+"checkCookie/",{"ipAddress" :this.ipAddress}).subscribe((response:any)=>{
      console.log("checkCookie ", response);
      console.log("api callledddddddddddd ", response);
      if(response.error == false)
      {
        this.cookie_not_set_yet = response.isIpAvailable;
        //console.log("old_attribute_type"+this.old_attribute_type)

      }
      
    });

    });
  }
  closeCookie(val:any)
  {
    this.cookie_not_set_yet = true;
    if(val == "cancel")
    {
      this.cookie_Service.delete("Porschists_home","/","porschists.com",true,"None");
    }
  }
  ngOnInit(): void {
    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          //console.log("userImage "+this.apiResponse.userRecord.userImage);
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
          
        }
      });
    }
    this._http.get(this.getBannerHomePage,{}).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allBanner = this.apiResponse.record;
      }
    });
    this._http.post(this.getCategory,{}).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCategory = this.apiResponse.record;
      }
    });

    this.queryParam = {user_id:this.user_id}
    this._http.post(this.getWebHomeUserFavAd,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
     // console.log(this.apiResponse, "error");
      if(this.apiResponse.error == false)
      {
        this.allFavAd = this.apiResponse.record;
        console.log(this.allFavAd, "allFavAd");
      }
    });

    let queryParam = {user_id:this.user_id}
    this._http.post(this.getWebHomeCarAd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCarAd = this.apiResponse.record;
        //console.log(this.allCarAd , "find")
      }
    });

    this.queryParam = {user_id:this.user_id}
    this._http.post(this.getWebHomeAccessAd,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allAccessAd = this.apiResponse.record;
      }
    });

    this._http.get(this.getCategoryOnAdPage,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allCategoryList = this.apiResponse.record;

        console.log("this.allCategoryList ",this.allCategoryList);
        
        this.cat1 = this.allCategoryList[0].category;
        this.cat2 = this.allCategoryList[1].category;
        
        this.cat1Heading =  this.allCategoryList[0].first_heading;
        this.cat2Heading =  this.allCategoryList[1].first_heading;

      }
      
    });
    this._http.get(this.registration_year,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allYear = this.apiResponse.record;
      }
      
    });

    this.queryParam = {attribute_type:"Porsche Voitures"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allModelList = this.apiResponse.record;
        console.log("allModelList ------>>>>>>> ", this.allModelList);
      }
    });

    this.queryParam = {attribute_type:"Pièces et accessoires"};
    this._http.post(this.allModel,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allAccessModelList = this.apiResponse.record;
        //console.log(this.allModelList);
      }
    });


    //Type de châssis
    this.queryParam = { "parent_id":"62e3d257ec6f493144a2533a"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allChasisType = this.apiResponse.record;
      }
    });

    //Carburant
    this.queryParam = { "parent_id":"62e3d307ec6f493144a25377"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allCarburant = this.apiResponse.record;
        //console.log("allCarburant"+this.allCarburant);
      }
    });
    //Etat du véhicule
    this.queryParam = { "parent_id":"62e3d38aec6f493144a25390"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allEtatDuVehicle = this.apiResponse.record;
      }
    });
    //Boîte de vitesse
    this.queryParam = { "parent_id":"62e3d3d3ec6f493144a253b7"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allBoiteDeVitesse = this.apiResponse.record;
      }
    });
    //getColor
    this._http.get(this.getColor,{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allColor = this.apiResponse.record;
      }
       
    });
    this._http.get(this.base_url_node+"getColorInterior",{}).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allColorInterior = this.apiResponse.record;
      }
       
    });
    
    //type_de_piece
    this.queryParam = { "parent_id":"62e4f5a35ed7e4f294719634"};
    this._http.post(this.getSubAttribute,this.queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allTypeDePiece = this.apiResponse.record;
        //console.log(this.allModelList);
      }
    });


  }

  handleAddressChange(address: any) {
    this.userAddress = address.formatted_address
    this.userLatitude = parseFloat(address.geometry.location.lat());
    this.userLongitude = parseFloat(address.geometry.location.lng());
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);
    console.log("address "+ JSON.stringify(address));
  }
  onCheckboxChange(e:any,f:any)
  {
    //console.log("e.target ", e);
    console.log("f  ", f);
    this.radioStatus = true;
    this.radio_model_heading = true;
    
    this.model_heading = f;
    console.log("this.radioStatus ",this.radioStatus);

    let cc = {
      paramName:"carModelArray",
      paramValue:f
    };
    this.carModelArray = e.target.value;
    
    

    const paramName2 = "carModelArray";
    const filteredPeople = this.filterArray.filter( (item) => item.paramName != paramName2);
    this.filterArray = filteredPeople;
    this.filterArray.push(cc);
    //console.log("this.filterArray ",this.filterArray);
  


    this.queryParam = { "parent_id":e.target.value  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      
      //console.log("response of api"+response);
      this.apiResponse = response; 
      //this.allVersions = this.apiResponse.all_version;
      if(this.apiResponse.error == false)
      {
        this.allSubModelListCar = this.apiResponse.record;
        // if(this.apiResponse.m_name_rec)
        // {
        //   this.model_name_new = this.apiResponse.m_name_rec.model_name;
        // }
        
        //console.log("response of api apiResponse ", this.apiResponse);
      }
    });
    
  }
  removeModel(event:any)
  {
    if(event ==  "carModelArray" )
    {
      
      this.model_heading="Modèle";
      this.carModelArray = "";
      this.radioStatus = false;
      this.radio_model_heading = false;
      console.log("this.radioStatus ",this.radioStatus);
    }

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  onGenerationChange(e:any,f:any)
  {
    console.log("response of api e "+e);
    console.log("this.carModelArray ", this.carModelArray);

    console.log("response of api f "+f);
    this.model_variant_heading = f;
    this.radio_model_variant = true;
    this.queryParam = { "parent_id":this.carModelArray,"generation":e };
      //   {
      //     "parent_id": "62e3b87d9c0dc0df532cfd07",
      //     "generation": "64be5bc764988cc7cbcb257a"
      // }
      
      // http://localhost:3001/api/allWebVersion
    this._http.post(this.base_url_node+"allWebVersion",this.queryParam).subscribe((response:any)=>{
      
      //console.log("response of api"+response);
      this.apiResponse = response; 
      //
      if(this.apiResponse.error == false)
      {
        this.allVersions = this.apiResponse.all_version;
        //console.log("response of api apiResponse ", this.apiResponse);
      }
    });


    let cc = {
      paramName:"model_variant",
      paramValue:f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "model_variant")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  
  removeGeneration(event:any)
  {
    if(event ==  "model_variant" )
    {
      this.model_variant_heading="Génération";
      this.form.value.model_variant = "";
      this.radio_model_variant = false;
    }
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  onVersionChange(e:any,f:any)
  {
    this.carVersion_heading = f;
    this.radio_carVersion = true;
    let cc = {
      paramName:"carVersion",
      paramValue:f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "carVersion")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  removecarVersion(event:any)
  {
    if(event ==  "carVersion" )
    {
      this.carVersion_heading="Version";
      this.form.value.carVersion = "";
      this.radio_carVersion = false;
    }
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  removeChahis(event:any)
  {
    if(event ==  "carVersion" )
    {
      this.form.value.carVersion = "";

      this.chahis_heading = "Type de châssis";  
      this.radio_chahis = false;
      this.chahisArray = "";

    }
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  removeValFilter(event:any)
  {
    console.log("event --->>>> ",event);
    if(event ==  "carModelArray" )
    {
      this.carModelArray = "";
      this.radioStatus = false;
      console.log("this.radioStatus ",this.radioStatus);
    }
    if(event ==  "model_variant" )
    {
      this.form.value.model_variant = "";
    }
    if(event ==  "chahisArray" )
    {
      this.chahisArray = "";
    }
    if(event ==  "type_of_gearbox" )
    {
      this.form.value.type_of_gearbox = "";
    }
    if(event ==  "yearArray_2" )
    {
      this.yearArray_2 = 0;
    }
    if(event ==  "minKilometer" )
    {
      this.form.value.minKilometer= "";
    }
    if(event ==  "maxKilometer" )
    {
      this.form.value.maxKilometer= "";
    }
    if(event ==  "fuel" )
    {
      this.form.value.fuel = "";
    }
    if(event ==  "colorId" )
    {
      this.form.value.colorId = "";
    }
    if(event ==  "colorInteriorId" )
    {
      this.form.value.colorInteriorId = "";
    }
    if(event ==  "pors_warranty" )
    {
      this.form.value.pors_warranty = "";
    }
    if(event ==  "warranty" )
    {
      this.form.value.warranty = "";
    }
    if(event ==  "maintenance_booklet" )
    {
      this.form.value.maintenance_booklet = "";
    }
    if(event ==  "maintenance_invoice" )
    {
      this.form.value.maintenance_invoice = "";
    }
    if(event ==  "report_piwi" )
    {
      this.form.value.report_piwi = "";
    }
    if(event ==  "matching_number" )
    {
      this.form.value.matching_number = "";
    }
    if(event ==  "matching_color_paint" )
    {
      this.form.value.matching_color_paint = "";
    }
    if(event ==  "original_paint" )
    {
      this.form.value.original_paint = "";
    }
    
    if(event ==  "vendeurType" )
    {
      this.form.value.vendeurType = "";
    }
    
    if(event ==  "onlineSince" )
    {
      this.form.value.onlineSince = "";
    } 
    
    
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    console.log("removeAarray --->>>> ",removeAarray);
    this.filterArray = removeAarray;
  }
  removeType_of_gearbox(event:any)
  {
      this.form.value.carVersion = "";
      this.type_of_gearbox_heading = "Boîte de vitesse";  
      this.radio_type_of_gearbox = false;
      this.form.value.type_of_gearbox = ""; 

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_fuel(event:any)
  {
      this.fuel_heading = "Carburant";  
      this.radio_fuel = false;
      this.form.value.fuel = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_Color_exterior(event:any)
  {
    this.color_exterior_heading = "Couleur extérieur";  
    this.radio_color_exterior  = false;
    this.form.value.colorId = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_Color_interior(event:any)
  {
    this.color_interior_heading = "Couleur intérieur";  
    this.radio_color_interior  = false;
    this.form.value.colorInteriorId = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_porsWarranty(event:any)
  {
    this.pors_warranty_heading = "Garantie Porsche Approved";  
    this.radio_pors_warranty  = false;
    this.form.value.pors_warranty = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_warranty(event:any)
  {
    this.warranty_heading = "Garantie";  
    this.radio_warranty  = false;
    this.form.value.warranty = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_maintenance_booklet(event:any)
  {
    this.maintenance_booklet_heading = "Carnet d’entretien";  
    this.radio_maintenance_booklet  = false;
    this.form.value.maintenance_booklet = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_maintenance_invoice(event:any)
  {
    this.maintenance_invoice_heading = "Facture d’entretien";  
    this.radio_maintenance_invoice  = false;
    this.form.value.maintenance_invoice = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_report_piwi(event:any)
  {
    this.report_piwi_heading = "Rapport piwis";  
    this.radio_report_piwi  = false;
    this.form.value.report_piwi = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_matching_number(event:any)
  {
    this.matching_number_heading = "Matching numbers";  
    this.radio_matching_number  = false;
    this.form.value.matching_number = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_matching_color_paint(event:any)
  {
    this.matching_color_paint_heading = "Matching colors";  
    this.radio_matching_color_paint  = false;
    this.form.value.matching_color_paint = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_original_paint(event:any)
  {
    this.original_paint_heading = "Peinture d’origine";  
    this.radio_original_paint  = false;
    this.form.value.original_paint = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_onlineSince(event:any)
  {
    this.onlineSince_heading = "Annonce en ligne depuis";  
    this.radio_onlineSince  = false;
    this.form.value.onlineSince = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_Vendeur(event:any)
  {
     
    this.vendeurType_heading = "Vendeur";  
    this.radio_vendeurType  = false;
    this.form.value.vendeurType = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);

  }
  remove_yearArray(event:any)
  {
    this.onYearChange_heading = "Année";
    this.radio_onYearChange  = false;
    this.yearArray = 0;

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }

  remove_yearArray_2(event:any)
  {
     
    this.onYearChange_2_heading = "";
    this.radio_onYearChange_2  = false;
    this.yearArray_2 = 0;

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_minKilometer(event:any)
  {
    this.minKilometer_heading = "kilométrage";
    this.radio_minKilometer  = false;
    this.form.value.minKilometer = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_maxKilometer(event:any)
  {
    this.maxKilometer_heading = "";
    this.radio_maxKilometer  = false;
    this.form.value.maxKilometer = "";

    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_accessModel(event:any)
  {
    this.accessModel_heading = "Modèle";
    this.radio_accessModel  = false;
    this.form.value.accessModel = "";

    


    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_subModel(event:any)
  {
    this.subModel_heading = "Génération";
    this.radio_subModel  = false;
    this.form.value.subModel = "";
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_accessRegYear(event:any)
  { 

    this.accessRegYear_heading = "Année";
    this.radio_accessRegYear  = false;
    this.form.value.accessRegYear = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  remove_accessRegYear_2(event:any)
  {
    this.accessRegYear_2_heading = "";
    this.radio_accessRegYear_2  = false;
    this.form.value.accessRegYear_2 = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArrayAccess.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArrayAccess = removeAarray;
    console.log("filterArrayAccess --->>>> ", this.filterArrayAccess);
  }
  remove_type_de_piece(event:any)
  {
    this.type_de_piece_heading = "Type de pièce";
    this.radio_type_de_piece  = false;
    this.form.value.type_de_piece = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArrayAccess.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArrayAccess = removeAarray;
    console.log("filterArrayAccess --->>>> ", this.filterArrayAccess);
  }
  remove_state(event:any)
  {
    this.state_heading = "État";
    this.radio_state  = false;
    this.form.value.state = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArrayAccess.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArrayAccess = removeAarray;
    console.log("filterArrayAccess --->>>> ", this.filterArrayAccess);
  }
  remove_OEM(event:any)
  {
    this.OEM_heading = "Pièce d'origine Porsche (OEM)";
    this.radio_OEM  = false;
    this.form.value.OEM = "";

    this.form.value.subModel = "";
    let removeAarray = this.filterArrayAccess.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArrayAccess = removeAarray;
    console.log("filterArrayAccess --->>>> ", this.filterArrayAccess);
  }

  removeValFilterAccess(event:any)
  {
    if(event ==  "accessModel" )
    {
      this.form.value.accessModel = "";
    } 
    if(event ==  "subModel" )
    {
      this.form.value.subModel = "";
    } 
    if(event ==  "accessRegYear_2" )
    {
      this.form.value.accessRegYear_2 = "";
    } 
    
    if(event ==  "accessRegYear" )
    {
      this.form.value.accessRegYear = "";
    } 
    if(event ==  "type_de_piece" )
    {
      this.form.value.type_de_piece = "";
    } 
    if(event ==  "state" )
    {
      this.form.value.state = "";
    } 
    if(event ==  "OEM" )
    {
      this.form.value.OEM = "";
    } 
      

    let removeAces_var = this.filterArrayAccess.filter((val)=>{
      if(event != val.paramName)
      {
        return val;
      }
    });
    this.filterArrayAccess = removeAces_var;
  }
  onColorChange(e:any,f:any)
  {
  this.color_exterior_heading = f;  
  this.radio_color_exterior  = true;

    let cc = {
      paramName:"colorId",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "colorId")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  onColorInteriorChange(e:any,f:any)
  {
    this.color_interior_heading = f;  
    this.radio_color_interior  = true;
    let cc = {
      paramName:"colorInteriorId",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "colorInteriorId")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  porsWarranty(f:any)
  {

    this.pors_warranty_heading = f;  
    this.radio_pors_warranty  = true;

    let cc = {
      paramName:"pors_warranty",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "pors_warranty")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  warrantyChange(f:any)
  {
    
    this.warranty_heading = f;  
    this.radio_warranty  = true;
    let cc = {
      paramName:"warranty",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "warranty")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  maintenanceBookletChange(f:any)
  {
    this.maintenance_booklet_heading = f;  
    this.radio_maintenance_booklet  = true;
    let cc = {
      paramName:"maintenance_booklet",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "maintenance_booklet")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  maintenanceInvoiceChange(f:any)
  {
    this.maintenance_invoice_heading = f;  
    this.radio_maintenance_invoice  = true;
    let cc = {
      paramName:"maintenance_invoice",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "maintenance_invoice")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  reportPiwiChange(f:any)
  {
    this.report_piwi_heading = f;  
    this.radio_report_piwi  = true;
    let cc = {
      paramName:"report_piwi",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "report_piwi")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  
  matchingNumberChange(f:any)
  {
    this.matching_number_heading = f;  
    this.radio_matching_number  = true;
    let cc = {
      paramName:"matching_number",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "matching_number")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  
  matching_color_paint_change(f:any)
  {
    
    this.matching_color_paint_heading = f;  
    this.radio_matching_color_paint  = true;
    let cc = {
      paramName:"matching_color_paint",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "matching_color_paint")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  original_paint_change(f:any)
  {
    
    this.original_paint_heading = f;  
    this.radio_original_paint  = true;
    let cc = {
      paramName:"original_paint",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "original_paint")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  vendeurType_change(f:any)
  {
    this.vendeurType_heading = f;  
    this.radio_vendeurType  = true;
    let cc = {
      paramName:"vendeurType",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "vendeurType")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  onYearChange_2(e:any,f:any) 
  {
    this.yearArray_2 = f;

    
    this.onYearChange_2_heading = f; 
    this.radio_onYearChange_2 = true;
    

    console.log("this.yearArray_2   -----   ", this.yearArray_2 );
    console.log("this.fffffffff   -----   ", f );

    let cc = {
      paramName:"yearArray_2",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "yearArray_2")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  onYearChange(e:any,f:any) 
  {
    this.onYearChange_heading = f;
    this.radio_onYearChange  = true;

    this.yearArray = f;
    console.log("this.yearArray -----   ", this.yearArray);
    let cc = {
      paramName:"yearArray",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "yearArray")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
    
  }
  changeMinKilo(e:any)
  {
    //minKilometer
    this.minKilometer_heading = e.target.value; 
    this.radio_minKilometer  = true;
    
    console.log("this.yearArray -----   ", e.target.value);
    let cc = {
      paramName:"minKilometer",
      paramValue: e.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "minKilometer")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  changeMaxKilo(e:any)
  {
    //maxKilometer
    this.maxKilometer_heading = e.target.value;  
    this.radio_maxKilometer  = true;

    console.log("this.yearArray -----   ", e.target.value);
    let cc = {
      paramName:"maxKilometer",
      paramValue: e.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "maxKilometer")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  changeOnlineSince(e:any)
  {
    
    this.radio_onlineSince  = true;
    console.log("changeOnlineSince -----   ", e.target.value);
    let j = "1 jour";
    if(e.target.value == 1)
    {
      j = "1 jour";
    }else{
      j = e.target.value+" jours";
    }
    this.onlineSince_heading = j;  
    let cc = {
      paramName:"onlineSince",
      paramValue: j
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "onlineSince")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  onFuelChange(e:any,f:any)
  {
    //maxKilometer

    
    this.fuel_heading = f;  
    this.radio_fuel = true;


    console.log("this.yearArray -----   ", e.target.value);
    let cc = {
      paramName:"fuel",
      paramValue: f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "fuel")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }

  onYearChangeAccess(e:any,f:any) { 
    this.accessRegYear_heading = f;
    this.radio_accessRegYear  = true;
    let cc = {
      paramName:"accessRegYear",
      paramValue: f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "accessRegYear")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);

  }

  onYearChangeAccess_2(e:any,f:any)
  { 
    this.accessRegYear_2_heading = f;
    this.radio_accessRegYear_2  = true;

    let cc = {
      paramName:"accessRegYear_2",
      paramValue: f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "accessRegYear_2")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);

  }

  onchahisChange(e:any,f:any) {
    //console.log(e.target.value);

    
  this.chahis_heading = f;  
  this.radio_chahis = true;

  this.chahisArray = e.target.value;
    let cc = {
      paramName:"chahisArray",
      paramValue:f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "chahisArray")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);
  }
  onTypeGearBoxChange(e:any,f:any)
  {

    this.type_of_gearbox_heading = f; 
    this.radio_type_of_gearbox  = true;


    let cc = {
      paramName:"type_of_gearbox",
      paramValue:f
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "type_of_gearbox")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  accessModelFun(eVal:any,f:any)
  {
    this.accessModel_heading = f;
    this.radio_accessModel  = true;
    let bb = {
      paramName:"accessModel",
      paramValue:f
    };
    let param2 = "accessModel";
    let ar_new = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != param2)
      {
        return val;
      }
    });
    this.filterArrayAccess = ar_new;
    this.filterArrayAccess.push(bb);

    this.queryParam = { "parent_id":eVal  };
    this._http.post(this.allWebSubModel,this.queryParam).subscribe((response:any)=>{
      
      //console.log("response of api"+response);
      this.apiResponse = response;
      if(this.apiResponse.error == false)
      {
        this.allSubModelList = this.apiResponse.record;
        //console.log("response of api allSubModelList "+this.allSubModelList);
      }
    });
  }
  onGenerationChangeAccess(e:any,f:any)
  {
    this.subModel_heading = f;
    this.radio_subModel  = true;
    let cc = {
      paramName:"subModel",
      paramValue:f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "subModel")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);
  }
  typeDePieceChange(e:any,f:any)
  {
    
    this.type_de_piece_heading = f;
    this.radio_type_de_piece  = true;
    let cc = {
      paramName:"type_de_piece",
      paramValue:f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "type_de_piece")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);
  }
  
  state_change(f:any)
  {
    this.state_heading = f;
    this.radio_state  = true;

    let cc = {
      paramName:"state",
      paramValue: f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "state")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);
  }
  oem_change(f:any)
  {
    this.OEM_heading = f;
    this.radio_OEM  = true;

    let cc = {
      paramName:"OEM",
      paramValue: f
    };
    let m_v_con = this.filterArrayAccess.filter((val)=>{
      if(val.paramName != "OEM")
      {
        return val;
      }
    });
    this.filterArrayAccess = m_v_con;
    this.filterArrayAccess.push(cc);
    console.log("this.filterArrayAccess ",this.filterArrayAccess);
  }
  showSlide()
  {
    //console.log("heree");
    this.showDiv = true;
    this.showDivButton = false;
  }
  hideSlide()
  {
    this.showDiv = false;
    this.showDivButton = true;
  }
  categoryRadioBtn(categoryType:string)
  {
    this.filterArray = [];
    this.filterArrayAccess = [];

    console.log("categoryType");
    console.log(categoryType);
    this.categoryTypeForm = categoryType;
    if(categoryType == 'Porsche Voitures')
    {
      console.log("ifffffffffff");
      this.tab1Div = true;
      this.tab2Div = false;
    }else{
      console.log("elseeeeeeeeee");
      this.tab1Div = false;
      this.tab2Div = true;
    }
  }

  form = new UntypedFormGroup({
    //carModel: new UntypedFormControl('',[]),
    carVersion: new UntypedFormControl('',[]),
    report_piwi: new UntypedFormControl('',[]),
    fuel: new UntypedFormControl('',[]),
    type_of_gearbox: new UntypedFormControl('',[]),
    vehicle_condition: new UntypedFormControl('',[]),
    colorId: new UntypedFormControl('',[]),
    colorInteriorId: new UntypedFormControl('',[]),
    color_name: new UntypedFormControl('',[]),
    pors_warranty: new UntypedFormControl('', []),
    warranty: new UntypedFormControl('', []),
    maintenance_booklet: new UntypedFormControl('', []),
    maintenance_invoice: new UntypedFormControl('', []),
    accidented: new UntypedFormControl('', []),
    original_paint: new UntypedFormControl('', []),
    matching_number: new UntypedFormControl('', []),
    matching_color_paint: new UntypedFormControl('', []),
    matching_color_interior: new UntypedFormControl('', []),
    price_for_pors: new UntypedFormControl('', []),
    deductible_VAT: new UntypedFormControl('', []),
    minPrice: new UntypedFormControl('', []),
    maxPrice: new UntypedFormControl('', []),
    minKilometer: new UntypedFormControl('', []),
    onlineSince: new UntypedFormControl('', []),
    vendeurType: new UntypedFormControl('', []),
    maxKilometer: new UntypedFormControl('', []),
    model_variant: new UntypedFormControl('', []),
    yearArray: new UntypedFormControl('', []),
    yearArray_2: new UntypedFormControl('', []),
    accessModel: new UntypedFormControl('', []),
    subModel: new UntypedFormControl('', []),
    type_de_piece: new UntypedFormControl('', []),
    state: new UntypedFormControl('', []),
    OEM: new UntypedFormControl('', []),
    minAccessPrice: new UntypedFormControl('', []),
    maxAccessPrice: new UntypedFormControl('', []),
    accessRegYear_2: new UntypedFormControl('', []),
    accessRegYear: new UntypedFormControl('', []),
    address: new UntypedFormControl('', []),
    city: new UntypedFormControl('', []),
    country: new UntypedFormControl('', []),
    zip_code: new UntypedFormControl('', []),
    ad_name: new UntypedFormControl('', []),
    nearByKm: new UntypedFormControl('', []),
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  zipCodeFunction(f:any)
  {
    console.log("val ", f.target.value);
    this.zip_code_heading = f.target.value;  
    this.radio_zip_code = true;
    let cc = {
      paramName:"zip_code",
      paramValue:f.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "zip_code")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  remove_zip_code(event:any)
  {
    this.zip_code_heading = "Code postal";
    this.radio_zip_code  = false;
    this.form.value.zip_code = "";
 
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
 
  cityFunction(f:any)
  {
    console.log("val ", f.target.value);
    this.city_heading = f.target.value;  
    this.radio_city = true;
    let cc = {
      paramName:"city",
      paramValue:f.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "city")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  remove_city(event:any)
  {
    this.city_heading = "Ville";
    this.radio_city  = false;
    this.form.value.city = "";
 
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
   
  countryFunction(f:any)
  {
    console.log("val ", f.target.value);
    this.country_heading = f.target.value;  
    this.radio_country = true;
    let cc = {
      paramName:"country",
      paramValue:f.target.value
    };
    let m_v_con = this.filterArray.filter((val)=>{
      if(val.paramName != "country")
      {
        return val;
      }
    });
    this.filterArray = m_v_con;
    this.filterArray.push(cc);
    console.log("this.filterArray ",this.filterArray);

  }
  remove_country(event:any)
  {
    this.country_heading = "Pays";
    this.radio_country  = false;
    this.form.value.country = "";
 
    let removeAarray = this.filterArray.filter((val)=>{
      console.log("val ", val);
      //paramName
      if(val.paramName != event)
      {
        return val;
      }
    })
    this.filterArray = removeAarray;
    console.log("filterArray --->>>> ", this.filterArray);
  }
  submit(){
    //console.log("here");
    //console.log("here");
    console.log("yearArray ", this.form.value.yearArray);
    console.log("yearArray_2 ", this.form.value.yearArray_2);
    this.redirectionUrl = this.base_url+"productList?category="+this.categoryTypeForm+"&carModelArray="+this.carModelArray+"&colorArray="+this.colorArray+"&yearArray="+this.yearArray+"&yearArray_2="+this.yearArray_2+"&chahisArray="+this.chahisArray+"&fuel="+this.form.value.fuel+"&type_of_gearbox="+this.form.value.type_of_gearbox+"&vehicle_condition="+this.form.value.vehicle_condition+"&pors_warranty="+this.form.value.pors_warranty+"&warranty="+this.form.value.warranty+"&maintenance_booklet="+this.form.value.maintenance_booklet+"&maintenance_invoice="+this.form.value.maintenance_invoice+"&accidented="+this.form.value.accidented+"&original_paint="+this.form.value.original_paint+"&matching_number="+this.form.value.matching_number+"&matching_color_paint="+this.form.value.matching_color_paint+"&matching_color_interior="+this.form.value.matching_color_interior+"&deductible_VAT="+this.form.value.deductible_VAT+"&minPrice="+this.form.value.minPrice+"&maxPrice="+this.form.value.maxPrice+"&minKilometer="+this.form.value.minKilometer+"&maxKilometer="+this.form.value.maxKilometer+"&model_variant="+this.form.value.model_variant+"&accessModel="+this.form.value.accessModel+"&subModel="+this.form.value.subModel+"&type_de_piece="+this.form.value.type_de_piece+"&state="+this.form.value.state+"&OEM="+this.form.value.OEM+"&minAccessPrice="+this.form.value.minAccessPrice+"&maxAccessPrice="+this.form.value.maxAccessPrice+"&yearArrayAccess="+this.yearArrayAccess+"&ad_name="+this.form.value.ad_name+"&userLatitude="+this.userLatitude+"&userLongitude="+this.userLongitude+"&userAddress="+this.userAddress+"&carVersion="+this.form.value.carVersion+"&nearByKm="+this.form.value.nearByKm+"&onlineSince="+this.form.value.onlineSince+"&vendeurType="+this.form.value.vendeurType+"&color_name="+this.form.value.color_name+"&city="+this.form.value.city+"&report_piwi="+this.form.value.report_piwi+"&country="+this.form.value.country+"&zip_code="+this.form.value.zip_code+"&colorInteriorId="+this.form.value.colorInteriorId+"&colorId="+this.form.value.colorId+"&accessRegYear="+this.form.value.accessRegYear+"&accessRegYear_2="+this.form.value.accessRegYear_2;
    
 

    //console.log(this.redirectionUrl);
    //
    
    //this.router.navigate([this.redirectionUrl]);
    //.log(this.carModelArray);
    //console.log(this.categoryTypeForm);

    console.log("userAddress "+this.userAddress);
    //console.log("userLatitude "+this.userLatitude);
    //console.log("userLongitude "+this.userLongitude);
    //console.log("ad_name "+this.form.value.ad_name);

    if (this.form.valid)
    {
      this.formValue = this.form.value;
      
      this.queryParam = {
        "carVersion":this.form.value.carVersion,
        "report_piwi":this.form.value.report_piwi,
        "ad_name":this.form.value.ad_name,
        "nearByKm":this.form.value.nearByKm,
        "userAddress":this.userAddress,
        "userLatitude":this.userLatitude,
        "userLongitude":this.userLongitude,
        "category":this.categoryTypeForm,
        "carModelArray":this.carModelArray,
        "colorArray":this.colorArray,
        "yearArray":this.form.value.yearArray,
        "yearArray_2":this.form.value.yearArray_2,
        "chahisArray":this.chahisArray,
        "fuel":this.form.value.fuel,
        "type_of_gearbox":this.form.value.type_of_gearbox,
        "vehicle_condition":this.form.value.vehicle_condition,
        "pors_warranty":this.form.value.pors_warranty,
        "warranty":this.form.value.warranty,
        "maintenance_booklet":this.form.value.maintenance_booklet,
        "maintenance_invoice":this.form.value.maintenance_invoice,
        "accidented":this.form.value.accidented,
        "original_paint":this.form.value.original_paint,
        "matching_number":this.form.value.matching_number,
        "matching_color_paint":this.form.value.matching_color_paint,
        "matching_color_interior":this.form.value.matching_color_interior,
        "deductible_VAT":this.form.value.deductible_VAT,
        "minPrice":this.form.value.minPrice,
        "maxPrice":this.form.value.maxPrice,
        "minKilometer":this.form.value.minKilometer,
        "onlineSince":this.form.value.onlineSince,
        "vendeurType":this.form.value.vendeurType,
        
        "model_variant":this.form.value.model_variant,
        "maxKilometer":this.form.value.maxKilometer,
        "color_name":this.form.value.color_name,
        
        "accessModel":this.form.value.accessModel,
        "subModel":this.form.value.subModel,
        "type_de_piece":this.form.value.type_de_piece,
        "state":this.form.value.state,
        "OEM":this.form.value.OEM,
        "minAccessPrice":this.form.value.minAccessPrice,
        "maxAccessPrice":this.form.value.maxAccessPrice,
        "yearArrayAccess":this.yearArrayAccess,
        "user_id":this.user_id,
        "zip_code":this.form.value.zip_code,
        "city":this.form.value.city,
        "country":this.form.value.country,
        "colorInteriorId":this.form.value.colorInteriorId,
        "colorId":this.form.value.colorId,
        "accessRegYear":this.form.value.accessRegYear,
        "accessRegYear_2":this.form.value.accessRegYear_2,
        
      };


      //console.log(this.queryParam);
      
      this._http.post(this.getWebHomeProductCount,this.queryParam).subscribe((response:any)=>{
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          ///this.allTypeDePiece = this.apiResponse.record;
          //console.log(this.allModelList);
          this.adCounter = this.apiResponse.record;
          //window.location.href = this.redirectionUrl;
          var aa  = this.redirectionUrl;
          console.log(aa);
          window.setTimeout(function(){
            window.location.href = aa;
          },2000);
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }


  carAdFav(id=null,user_id=null,status:any)
  {
    
    //console.log("id "+id);
    //console.log("user_id "+user_id);
    //console.log("status "+status);
    let queryParam = {ad_id:id,user_id:user_id,status:status};
    this._http.post(this.adUserFavAdd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.location.reload();
      }
    });

  }
  AccessAdFav(id=null,user_id=null,status:any)
  {
    
    //console.log("id "+id);
    //console.log("user_id "+user_id);
    //console.log("status "+status);
    let queryParam = {ad_id:id,user_id:user_id,status:status};
    this._http.post(this.adUserFavAdd,queryParam).subscribe((response:any)=>{
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        window.location.reload();
      }
    });

  }
}
