import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;token:any; user_id:any; user_type:any;
  queryParam:any;webGetUserProfile:any;firstName:any; lastName:any; user_email:any; user_mobileNumber:any;userImage:any;getBannerHomePage:any;allBanner:any;allCategory:any;
  getCategory:any;autocompleteOption:any
  autocompleteURl:any
  mySuggestion:any; autoCompleteData:any
  cartCountURL:any; cartCount:any; cartCountval:any
  notFound:any
  public countries = [
    
  ];
  keyword = '_id';record:any;  old_title:any; notificationCount:Number = 0;
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node+"webGetUserProfile";
    this.getBannerHomePage = this.base_url_node+"getBannerHomePage";
    this.getCategory = this.base_url_node+"getCategory";
    this.autocompleteURl = this.base_url_node+"webHeaderAutoSuggestion";
    this.cartCountURL = this.base_url_node+"webGetUserCart";
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    this.notFound =  "test"
    
    //console.log("this.base_url "+this.base_url);
    //  console.log(this.token);
    //  console.log(this.user_id);
      ///console.log(this.user_type);

      this._http.get(this.base_url_node+"getHomeTitle/").subscribe((response:any)=>{
        console.log("getSingleModel"+JSON.stringify(response));
        if(response.error == false)
        { 
          this.record = response.record;
          this.old_title = this.record.title; 
          console.log("old_attribute_type"+this.old_title)
  
        }
        
      });
      this._http.get(this.base_url_node+"getNotificationCount/"+this.user_id).subscribe((response:any)=>{
        //console.log("getSingleModel"+JSON.stringify(response));
        if(response.error == false)
        { 
          this.notificationCount = response.record;
  
        }
        
      });
  }

  ngOnInit(): void {
    if(this.user_id)
    {
      this.queryParam = {"user_id":this.user_id};
      this._http.post(this.webGetUserProfile,this.queryParam).subscribe((response:any)=>{
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          //console.log("loggedin_time "+this.apiResponse.userRecord.loggedin_time);
          const date = new Date();
          var hours = Math.abs(date.getTime() - new Date(this.apiResponse.userRecord.loggedin_time).getTime()) / 3600000;
          console.log("hours "+hours);
          if(hours > 24)
          {
            window.location.href = this.base_url+"logout";
          }
          if(this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null)
          {
            this.userImage = this.base_url_node_only+"public/uploads/userProfile/"+this.apiResponse.userRecord.userImage;
          }else{
            this.userImage = "assets/images/logo/user.png";
          }
        }
      });

      this._http.post(this.cartCountURL,this.queryParam).subscribe(res=>{
        this.cartCount = res
        this.cartCountval  = this.cartCount.record.length
       
      })
    }
  }

  selectEvent(item:any) {
    console.log(item, "testsyy")
    console.log(item._id, "_id")
    console.log(item.category, "category")
    let urllll = this.base_url+"productList?category="+item.category+"&carModelArray=&colorArray=&yearArray=&chahisArray=&fuel=&type_of_gearbox=&vehicle_condition=&warranty=&maintenance_booklet=&maintenance_invoice=&accidented=&original_paint=&matching_number=&matching_color_paint=&matching_color_interior=&deductible_VAT=&minPrice=&maxPrice=&minKilometer=&maxKilometer=&model_variant=&accessModel=&subModel=&type_de_piece=&state=&OEM=&minAccessPrice=&maxAccessPrice=&yearArrayAccess=&ad_name="+item._id+"&userLatitude=0&userLongitude=0&userAddress=";
    console.log("urllll " , urllll)
    window.location.href = urllll
    //window.location.href = this.base_url+"productList?queryString="+item._id
    //?category=Porsche%20Voitures&carModelArray=&colorArray=&yearArray=&chahisArray=&fuel=&type_of_gearbox=&vehicle_condition=&warranty=&maintenance_booklet=&maintenance_invoice=&accidented=&original_paint=&matching_number=&matching_color_paint=&matching_color_interior=&deductible_VAT=&minPrice=&maxPrice=&minKilometer=&maxKilometer=&model_variant=&accessModel=&subModel=&type_de_piece=&state=&OEM=&minAccessPrice=&maxAccessPrice=&yearArrayAccess=&ad_name=Test&userLatitude=0&userLongitude=0&userAddress=
    //window.location.href = this.base_url+"productList?queryString="+item._id
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e:any) {
    console.log(e.target.value)
  }

  onKeydownEvent(event:any){
   

    this.autocompleteOption={
     "advertisemnet_name":event.target.value
    }
    
    this._http.post(this.autocompleteURl, this.autocompleteOption).subscribe(res=>{
      this.mySuggestion = res
      this.autoCompleteData = this.mySuggestion.record

    })
  }

}
