import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AccessoriesPurchaseListComponent } from './accessories-purchase-list.component';

describe('AccessoriesPurchaseListComponent', () => {
  let component: AccessoriesPurchaseListComponent;
  let fixture: ComponentFixture<AccessoriesPurchaseListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccessoriesPurchaseListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AccessoriesPurchaseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
