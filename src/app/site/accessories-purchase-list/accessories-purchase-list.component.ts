import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators, UntypedFormBuilder, FormGroup } from '@angular/forms';
import { LoginauthenticationService } from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-accessories-purchase-list',
  templateUrl: './accessories-purchase-list.component.html',
  styleUrls: ['./accessories-purchase-list.component.css']
})
export class AccessoriesPurchaseListComponent implements OnInit {

  base_url = ""; base_url_node = ""; base_url_node_only: any;
  webUserLoginSubmit: any;
  formData: any; formValue: any;
  apiResponse: any; allPurchaseItem: any;
  token: any; user_id: any; user_type: any;
  queryParam: any; webGetAccessoriesPurchase: any;
  webGetUserProfile: any;
  firstName: any; lastName: any; user_email: any; user_mobileNumber: any; userImage: any;
  paymentURL: any; paymentOption: any; paymentTransactions: any
  Paquets: any; Produits: any
  webGetOwnSalesURL:any;  webGetOwnSalesparams:any
  mysales:any; sales:any
  paypal:any;other:any
  payPalURL:any; payPalparams:any; paypalApires:any
  payPalForm !: FormGroup
  form!: FormGroup
  bankURL:any; bankApiRes:any;p:any
  numbers:any; selectedIndex:any

  webGetOwnSalesCount:any;webGetOwnSalesProcessCount:any; webGetOwnSalesCancelCount:any
  webGetOwnSalesCompleteCount:any

  webGetOwnSalesProcess:any; webGetOwnSalesCancel:any
  webGetOwnSalesComplete:any

  webGetOwnSalesProcessparams:any; webGetOwnSalesCancelparams:any
  webGetOwnSalesCompleteparams:any

  webGetOwnSalesProcessRes:any; webGetOwnSalesCancelRes:any
  webGetOwnSalesCompleteRes:any

  webGetOwnSalesCountrecord:any;webGetOwnSalesProcessCountrecord:any; webGetOwnSalesCancelCountrecord:any
  webGetOwnSalesCompleteCountrecord:any


  webShippingProcess:any; webShippingCancel:any; webShippingComplete:any
  webShippingRes:any
  errorShow:any

  allsales:any; completedNumbers:any; processingNumbers:any; cancelNumbers:any


  getSellerEarning:any; getSellerEarningRes:any; singleTransaction:any

  mytransactiontable:any
  getSellerEarningMultipleOrder:any; getSellerEarningMultipleOrderRes:any; allTrasactions:any

  userAddress:any;userLatitude:any;userLongitude:any;centerLatitude:any;centerLongitude:any;google_zip_code:any;google_country:any;marker:any;google_city:any;


  constructor(private _http: HttpClient, private formBuilder: UntypedFormBuilder, private loginAuthObj: LoginauthenticationService) {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webGetUserProfile = this.base_url_node + "webGetUserProfile";
    this.webGetAccessoriesPurchase = this.base_url_node + "webGetAccessoriesPurchase";
    this.paymentURL = this.base_url_node + "proUserPaymentHistory";
    this.webGetOwnSalesURL = this.base_url_node + "webGetOwnSales";
    this.payPalURL = this.base_url_node + "addUpdatePaypalEmail";
    this.bankURL = this.base_url_node + "addBankInformation";

    this.getSellerEarning = this.base_url_node + "getSellerEarning";

    this.getSellerEarningMultipleOrder = this.base_url_node + "getSellerEarningMultipleOrder";

    this.webGetOwnSalesProcess = this.base_url_node + "webGetOwnSalesProcess";
    this.webGetOwnSalesCancel = this.base_url_node + "webGetOwnSalesCancel";
    this.webGetOwnSalesComplete = this.base_url_node + "webGetOwnSalesComplete";

    this.webShippingProcess = this.base_url_node + "webShippingProcess/";
    this.webShippingCancel = this.base_url_node + "webShippingCancel/";
    this.webShippingComplete = this.base_url_node + "webShippingComplete/";

    
    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    this.Paquets = false
    this.Produits = true
    this.paypal = true
    this.other = true
    this.selectedIndex = 0;

    

    this.paymentTransactions = []
    this.webGetOwnSalesCount = this.base_url_node + "webGetOwnSalesCount/"+this.user_id;
    this.webGetOwnSalesProcessCount = this.base_url_node + "webGetOwnSalesProcessCount/"+this.user_id;
    this.webGetOwnSalesCancelCount = this.base_url_node + "webGetOwnSalesCancelCount/"+this.user_id;
    this.webGetOwnSalesCompleteCount = this.base_url_node + "webGetOwnSalesCompleteCount/"+this.user_id;

    //  console.log(this.token);
    //  console.log(this.user_id);
    //  console.log(this.user_type);
    if (this.token === "" || this.user_id === "" || this.user_id === null || this.token === null) {
      window.location.href = this.base_url;
    }
    
    this.payPalForm = new UntypedFormGroup({
    
      paypal_email: new UntypedFormControl('', [Validators.required]),
      
    });

    this.form = new UntypedFormGroup({
      
      account_holder_name:new UntypedFormControl('', Validators.required),
      address:new UntypedFormControl('', Validators.required),
      latitude:new UntypedFormControl( ),
      longitude:new UntypedFormControl( ),
      zip_code:new UntypedFormControl('', Validators.required),
      city:new UntypedFormControl('', Validators.required),
      country:new UntypedFormControl('', Validators.required),
      shipping_type_1:new UntypedFormControl('', Validators.required),

      bank_name:new UntypedFormControl('', Validators.required),
      iban:new UntypedFormControl('', Validators.required),
      bic:new UntypedFormControl('', Validators.required)

    });

    this.mytransactiontable = true
  }
  get formThirdStepFun(){
    return this.form.controls;
  }

  ngOnInit(): void {


    this._http.post(this.getSellerEarning, {"seller_id":this.user_id}).subscribe(res=>{
      this.getSellerEarningRes = res
      if(this.getSellerEarningRes.error == false){
        this.singleTransaction = this.getSellerEarningRes.record
        console.log( this.singleTransaction, " this.singleTransaction")
      }
    })
 

    this.paymentOption = {
      "user_id": this.user_id,
      "pageNo": 0
    }

    this._http.post(this.paymentURL, this.paymentOption).subscribe(res => {
      this.apiResponse = res;
      this.paymentTransactions = this.apiResponse.record
      console.log(this.paymentTransactions, "paymentTransactions history ")
    })

    if (this.user_id) {
      this.queryParam = { "user_id": this.user_id };
      this._http.post(this.webGetUserProfile, this.queryParam).subscribe((response: any) => {
        //console.log("response of api"+response);
        this.apiResponse = response;
        //console.log(this.apiResponse);
        if (this.apiResponse.error == false) {
          //console.log("userRecord "+JSON.stringify(this.apiResponse.userRecord));
          this.firstName = this.apiResponse.userRecord.firstName;
          this.lastName = this.apiResponse.userRecord.lastName;
          this.user_email = this.apiResponse.userRecord.email;
          this.user_mobileNumber = this.apiResponse.userRecord.mobileNumber;
          //console.log("userImage "+this.apiResponse.userRecord.userImage);
          if (this.apiResponse.userRecord.userImage != "" && this.apiResponse.userRecord.userImage != null) {
            this.userImage = this.base_url_node_only + "public/uploads/userProfile/" + this.apiResponse.userRecord.userImage;
          } else {
            this.userImage = "assets/images/logo/user.png";
          }

        }
      });
    }

    this.queryParam = { "user_id": this.user_id };
    this._http.post(this.webGetAccessoriesPurchase, this.queryParam).subscribe((response: any) => {
      if (response.error == false) {
        this.allPurchaseItem = response.record;
        console.log( this.allPurchaseItem, " this.allPurchaseItem")
       
      }
    });

   
  
    this.webGetOwnSalesparams = {
      "user_id": this.user_id,
      "pageNo": 0
    }

    this._http.post(this.webGetOwnSalesURL, this.webGetOwnSalesparams).subscribe(res=>{
      this.mysales = res
      this.sales = this.mysales.record

      console.log(this.sales, "my sales")
       
    })

    // process
    this.webGetOwnSalesProcessparams = {
      "user_id": this.user_id,
      "pageNo": 0
    }
    this._http.post(this.webGetOwnSalesProcess, this.webGetOwnSalesProcessparams).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesProcessRes = this.apiResponse.record;
      
    })
    // complete
    this.webGetOwnSalesCompleteparams = {
      "user_id": this.user_id,
      "pageNo": 0
    }
    this._http.post(this.webGetOwnSalesComplete, this.webGetOwnSalesCompleteparams).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesCompleteRes = this.apiResponse.record;
      console.log("this.webGetOwnSalesCompleteRes 199 line no")
      console.log(this.webGetOwnSalesCompleteRes);
      console.log(this.webGetOwnSalesCompleteRes.length);
      
    })
  
    // cancel
    this.webGetOwnSalesCancelparams = {
      "user_id": this.user_id,
      "pageNo": 0
    }
    this._http.post(this.webGetOwnSalesCancel, this.webGetOwnSalesCancelparams).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesCancelRes = this.apiResponse.record;
      
    })
   
   
    this._http.get(this.webGetOwnSalesCount).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesCountrecord = this.apiResponse.record;
      const myNumber = this.apiResponse.record/10
      this.allsales = Array(Math.ceil(myNumber)).fill(0).map((m,n)=>n);
      
    })
    this._http.get(this.webGetOwnSalesProcessCount).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesProcessCountrecord = this.apiResponse.record;
      const myNumber = this.apiResponse.record/10;
      this.processingNumbers = Array(Math.ceil(myNumber)).fill(0).map((m,n)=>n);
    })
    this._http.get(this.webGetOwnSalesCancelCount).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesCancelCountrecord = this.apiResponse.record;
      const myNumber = this.apiResponse.record/10
      this.cancelNumbers = Array(Math.ceil(myNumber)).fill(0).map((m,n)=>n);
    })
    this._http.get(this.webGetOwnSalesCompleteCount).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesCompleteCountrecord = this.apiResponse.record;
      const myNumber = this.apiResponse.record/10
      this.completedNumbers = Array(Math.ceil(myNumber)).fill(0).map((m,n)=>n);
    })



  }
  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }
  Banksubmit(){
    if (this.form.invalid)
      {
        this.validateAllFormFields(this.form);  return;
      }else{

        //{"address": this.userAddress,"latitude":this.userLatitude,"longitude":this.userLongitude,"ad_id":this.ad_id,"zip_code":this.formFifthStep.value.zip_code,"city":this.formFifthStep.value.city,"country":this.formFifthStep.value.country};

      
      this.form.value.address = this.userAddress;
      this.form.value.latitude = this.userLatitude;
      this.form.value.longitude = this.userLongitude;
      
      this.form.value.user_id = this.user_id;
      this._http.post(this.bankURL,this.form.value).subscribe(res=>{
        
        this.bankApiRes = res
        if(this.bankApiRes.error == false){
          //this.form.reset();
        }
      })
    }

  }
  
  handleAddressChange(address: any) {
    this.userAddress = address.formatted_address
    this.userLatitude = address.geometry.location.lat()
    this.userLongitude = address.geometry.location.lng()
    //console.log(this.userLatitude);
    //console.log(this.userLongitude);

    this.centerLatitude = parseFloat(address.geometry.location.lat());
    this.centerLongitude = parseFloat(address.geometry.location.lng());
    //console.log(this.centerLatitude);
    //console.log(this.centerLongitude);
    this.marker = {
      position: { lat: this.centerLatitude, lng: this.centerLongitude },
    }


    if(address)
    {
      if(address.address_components)
      {
        address.address_components.forEach((val:any)=>{
          if(val)
          {
            if(val.types)
            {
              if(val.types.length > 0)
              {
                for(let x=0; x<val.types.length; x++)
                {
                  console.log(val.types[x]);
                  if(val.types[x] == "postal_code")
                  {
                    this.google_zip_code = val.long_name;
                  }
                  if(val.types[x] == "country")
                  {
                    this.google_country = val.long_name;
                  }
                  if(val.types[x] == "locality")
                  {
                    this.google_city = val.long_name;
                  }
                }
              }
            }
          }
        })
      }
    }

  }
  showPaquests() {
    this.Paquets = false
    this.Produits = true
  }
  showProduits() {
    this.Paquets = true
    this.Produits = false
  }
  showPaypal(){
    this.paypal = false
    this.other = true
  }
  showOther(){
    this.paypal = true
    this.other = false
  }

  updatePaypal(){
    this.payPalparams = {
      "user_id": this.user_id,
      "paypal_email": this.payPalForm.value.paypal_email
    }
   console.log(this.payPalparams)

   this._http.post(this.payPalURL, this.payPalparams).subscribe(res=>{
    console.log(res)
    this.paypalApires = res 
     if(this.paypalApires.error == 'false'){
      
     }
   })
  }


  sendDetailProcess(eve:any, saleId:any){
    const codeID = eve.target.value

    if(codeID == 1){
      this._http.get(this.webShippingProcess+saleId).subscribe(res=>{
        this.webShippingRes = res
        this.errorShow = this.webShippingRes
        console.log(this.webShippingRes)
        if(this.webShippingRes.error == false){
          
          setTimeout(()=>{                          
            window.location.reload()
           }, 2000);
         
        }
      })
    }
    else if(codeID == 2){
      this._http.get(this.webShippingCancel+saleId).subscribe(res=>{
        this.webShippingRes = res
        this.errorShow = this.webShippingRes
        console.log(this.webShippingRes)
        if(this.webShippingRes.error == false){
          
          setTimeout(()=>{                          
            window.location.reload()
           }, 2000);
        }
        
      })
    }
    else if(codeID == 3){
      this._http.get(this.webShippingComplete+saleId).subscribe(res=>{
        this.webShippingRes = res
        this.errorShow = this.webShippingRes
        console.log(this.webShippingRes)
        if(this.webShippingRes.error == false){
         
          setTimeout(()=>{                          
            window.location.reload()
           }, 2000);
        } 
        
      })
    }
   

  }

  getallSales(item:any){
    this.webGetOwnSalesparams = {
      "from_date":this.form2.value.from_date,
      "to_date":this.form2.value.to_date,
      "search_text":this.form2.value.search_text,
      "user_id": this.user_id,
      "pageNo": item
    }

    this._http.post(this.webGetOwnSalesURL, this.webGetOwnSalesparams).subscribe(res=>{
      this.mysales = res
      this.sales = this.mysales.record
       
    })
  }


  getcompletedSales(item:any){
    this.webGetOwnSalesCompleteparams = {
      "from_date":this.form2.value.from_date,
      "to_date":this.form2.value.to_date,
      "search_text":this.form2.value.search_text,
      "user_id": this.user_id,
      "pageNo": item
    }
    this._http.post(this.webGetOwnSalesComplete, this.webGetOwnSalesCompleteparams).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesCompleteRes = this.apiResponse.record;
      
    })
  }

  getprocessSales(item:any){
    this.webGetOwnSalesProcessparams = {
      "from_date":this.form2.value.from_date,
      "to_date":this.form2.value.to_date,
      "search_text":this.form2.value.search_text,
      "user_id": this.user_id,
      "pageNo": item
    }
    this._http.post(this.webGetOwnSalesProcess, this.webGetOwnSalesProcessparams).subscribe(res=>{
      this.webGetOwnSalesProcessRes = res
      
    })
  }
  getcancelSales(item:any){
    this.webGetOwnSalesCancelparams = {
      "from_date":this.form2.value.from_date,
      "to_date":this.form2.value.to_date,
      "search_text":this.form2.value.search_text,
      "user_id": this.user_id,
      "pageNo": item
    }
    this._http.post(this.webGetOwnSalesCancel, this.webGetOwnSalesCancelparams).subscribe(res=>{
      this.webGetOwnSalesCancelRes = res
      
    })
  }
  form2 = new UntypedFormGroup({
    from_date: new UntypedFormControl('', [ ]),
    to_date: new UntypedFormControl('', [ ]),
    search_text: new UntypedFormControl('', [ ]),
  });
  get f(){
    return this.form2.controls;
  }
  submit()
  {
    //console.log(this.form2.value)
    this.getallSales(0)
    this.getcompletedSales(0)
    this.getprocessSales(0)
    this.getcancelSales(0)
  }
  resetForm()
  {
    this.form2.reset();
    this.getallSales(0)
    this.getcompletedSales(0)
    this.getprocessSales(0)
    this.getcancelSales(0)
  }


  openMyTransaction(trasId:any){
    this.mytransactiontable = false
    console.log(trasId)
    

    this._http.post(this.getSellerEarningMultipleOrder,{"id":trasId}).subscribe(res=>{
      this.getSellerEarningMultipleOrderRes = res
      if(this.getSellerEarningMultipleOrderRes.error == false){
        this.allTrasactions = this.getSellerEarningMultipleOrderRes.record[0].data

        console.log( this.allTrasactions, " this.allTrasactions " )
      }
    })

  }
}
