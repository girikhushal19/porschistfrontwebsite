import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router,NavigationStart, Event as NavigationEvent } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  base_url = "";base_url_node = "";base_url_node_only:any;webUserLoginSubmit:any;
  formData:any;formValue:any;apiResponse:any;currentRoute:any;
  callbackUrl:any;
  constructor(private router: Router,private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService)
  { 
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;
    this.webUserLoginSubmit = this.base_url_node+"webUserLoginSubmit";

    this.currentRoute = this.router.url.split('/');
    //console.log(this.currentRoute);
    //console.log(this.currentRoute.length);
    if(this.currentRoute.length === 3)
    {
      //console.log(this.currentRoute[2]);
      this.callbackUrl = this.currentRoute[2];
    }

  }

  ngOnInit(): void {
  }

  form = new UntypedFormGroup({
    //name: new FormControl('', [Validators.required, Validators.minLength(3)]),
    email: new UntypedFormControl('', [Validators.required, Validators.email]),
    password: new UntypedFormControl('',[Validators.required, Validators.minLength(6)])
  });
  
  get f(){
    return this.form.controls;
  }
  
  validateAllFormFields(formGroup: UntypedFormGroup) 
  {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof UntypedFormControl)
      {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) 
      {
        this.validateAllFormFields(control);
      }
    });
  }

  submit(){
    console.log("here");
    if (this.form.valid)
    {
      this.formValue = this.form.value;
      this._http.post(this.webUserLoginSubmit,this.formValue).subscribe((response:any)=>{
      this.apiResponse = response;
      console.log(this.apiResponse);
        if(this.apiResponse.error == false)
        {
           localStorage.setItem("token",this.apiResponse.userRecord.token);
           localStorage.setItem("_id",this.apiResponse.userRecord._id);
           localStorage.setItem("email",this.apiResponse.userRecord.email);
           localStorage.setItem("user_type",this.apiResponse.userRecord.user_type);
           localStorage.setItem("loggedin_time",this.apiResponse.userRecord.loggedin_time);
           
          if(this.callbackUrl != "" && this.callbackUrl != null)
          {
            window.location.href = this.base_url+this.callbackUrl;
          }else{
            window.location.href = this.base_url+"/userProfile/"+this.apiResponse.userRecord._id;
          }
           
        }
      });
    }else{
      //console.log('erro form submitted');
      this.validateAllFormFields(this.form); 
      // validate all form fields
    }
  }
}
