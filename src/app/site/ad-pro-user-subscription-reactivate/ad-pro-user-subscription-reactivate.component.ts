import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators,UntypedFormBuilder} from '@angular/forms';
import {LoginauthenticationService} from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { Router,ActivatedRoute} from '@angular/router';

 
@Component({
  selector: 'app-ad-pro-user-subscription-reactivate',
  templateUrl: './ad-pro-user-subscription-reactivate.component.html',
  styleUrls: ['./ad-pro-user-subscription-reactivate.component.css']
})
export class AdProUserSubscriptionReactivateComponent implements OnInit {
    user_error:number=0;
    base_url = "";base_url_node = "";ad_id:any; 
    token:any;user_type:any;apiResponse:any;apiResponseLast:any;formValue:any;record:any;totalPageNumber:any;
     queryParam:any;numbers:any; 
    base_url_node_only:any; formData:any;webStep2CarAdName:any;
    personalDetails!: UntypedFormGroup;addAdImageTesting:any;user_email:any;
    addressDetails!: UntypedFormGroup;user_id:any;lastName:any;user_mobileNumber:any;
    educationalDetails!: UntypedFormGroup;firstName:any;
     
     webStep6CarAdName:any;
     paymentTypeNew:any;
    step = 1;
    model_id_on_change:any;
    first_plan_price:number;second_plan_price:number;third_plan_price:number;final_plan_price:number;
    old_ad_type:any;
    allUserParticularPlan:any;allUserParticularPlanTopSearch:any;
    webGetUserProPlan:any;webGetUserProPlanTopSearch:any;
    resultCarOldPlan:any;resultAccessOldPlan:any;resultAllOldPlanCount:any;
    inactiveNumber:any;getAllOldPlanSubscriptionRec:any;
    selectedIndex2:any;
    webPaymentRenewalAdProfessional:any;user_id_second:any;
  constructor(private _http:HttpClient,private formBuilder: UntypedFormBuilder,private loginAuthObj:LoginauthenticationService,private router: Router,private actRoute: ActivatedRoute) 
  {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();
    
    // console.log(this.token);
    // console.log(this.user_id);
    // console.log(this.user_type);

    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }
    //this.user_id_second =  this.actRoute.snapshot.params['id'];
    //console.log("this.user_id_second "+this.user_id_second);
    this.first_plan_price = 0;this.second_plan_price = 0;this.third_plan_price = 0;
    this.final_plan_price = 0;

    this.webGetUserProPlan = this.base_url_node+"webGetUserProPlan";
    this.webGetUserProPlanTopSearch = this.base_url_node+"webGetUserProPlanTopSearch";
    this.webPaymentRenewalAdProfessional = this.base_url_node+"webPaymentRenewalAdProfessional";
    
    if(this.token === "" || this.user_id === "" || this.user_id === null || this.token === null)
    {
      window.location.href = this.base_url;
    }
    this.old_ad_type = "accessories";

    this.queryParam = {"user_id":this.user_id};
    this._http.post(this.base_url_node+"getActiveCarAccessPlan",this.queryParam).subscribe((response:any)=>{
      console.log("response of api"+response);
      this.apiResponse = response;
      this.resultCarOldPlan = this.apiResponse.resultCarOldPlan;
      this.resultAccessOldPlan = this.apiResponse.resultAccessOldPlan;
      this.resultAllOldPlanCount = this.apiResponse.resultAllOldPlanCount;

      const myNumber = this.resultAllOldPlanCount/10;

      this.inactiveNumber = Array(Math.ceil(myNumber)).fill(0).map((m,n)=>n);

      console.log(this.resultCarOldPlan);
      console.log(this.resultAccessOldPlan);
      //console.log(this.apiResponse);
      // if(this.apiResponse.error == false)
      // {
      //   this.allUserParticularPlan = this.apiResponse.record;
      // }
    });
    this.inactivePages(0)
  }
  inactivePages(item:any){
    this.selectedIndex2 = item
    this.queryParam = {
      "user_id": this.user_id,
      "pageNo": item
    };
    this._http.post(this.base_url_node+"getAllOldPlanSubscription",this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //
      this.getAllOldPlanSubscriptionRec = this.apiResponse.resultAllOldPlan;
    });
  }
  ngOnInit(): void {
    this.queryParam = {plan_type: 'car_plan'};
    this._http.post(this.webGetUserProPlan,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlan = this.apiResponse.record;
      }
    });

    this.queryParam = {plan_type: 'car_plan'};
    this._http.post(this.webGetUserProPlanTopSearch,this.queryParam).subscribe((response:any)=>{
      //console.log("response of api"+response);
      this.apiResponse = response;
      //console.log(this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.allUserParticularPlanTopSearch = this.apiResponse.record;
      }
    });
    
  }
 
  formSevenStep = new UntypedFormGroup({
    ad_type: new UntypedFormControl('', []),
    paymentType: new UntypedFormControl('', []),
    user_id: new UntypedFormControl('', []),
    proUserAd: new UntypedFormControl('', []),
    proUserAdTopSearch: new UntypedFormControl('', []),
  });
  get formSevenStepFun(){
    return this.formSevenStep.controls;
  }

  validateAllFormFields(formGroup: UntypedFormGroup)
  {         //{1}
    Object.keys(formGroup.controls).forEach(field => {  //{2}
      const control = formGroup.get(field);             //{3}
      if (control instanceof UntypedFormControl) {             //{4}
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof UntypedFormGroup) {        //{5}
        this.validateAllFormFields(control);            //{6}
      }
    });
  }

  next()
  {
    if (this.formSevenStep.invalid)
    {
      this.validateAllFormFields(this.formSevenStep);  return;
    }else{
      //console.log(this.formSevenStep.value);
      this._http.post(this.webPaymentRenewalAdProfessional,this.formSevenStep.value).subscribe((response:any)=>{
        this.apiResponseLast = response;
        console.log(this.apiResponseLast);
        //return false;
        //console.log("formData "+this.formData);
        if(this.apiResponseLast.error == false)
        {
          console.log(this.apiResponseLast);
          console.log(this.apiResponseLast.lastInsertId);
          console.log(this.apiResponseLast.price);
          
          
          
          if(this.formSevenStep.value.paymentType == "Paypal")
          {
            var redirect_url = this.base_url_node+"getProUsrCarAdPaymentRenewalPaypal?id="+this.apiResponseLast.lastInsertId+"&price="+this.apiResponseLast.price;
          }else{
            var redirect_url = this.base_url_node+"getProUsrCarAdPaymentRenewalStripe?id="+this.apiResponseLast.lastInsertId+"&price="+this.apiResponseLast.price;
          }
          
          //return false;
          //var redirect_url = this.apiResponseLast.redirect_url_web;
          //
          //this.ad_id = this.apiResponseLast.lastInsertId;
          //console.log("ad_id "+this.ad_id);
          //this.step++;

            setTimeout(() => {
              window.location.href = redirect_url;
            }, 2000);

        }
      });
    }
  }
  
   onItemChange(event:any)
   {
      this.first_plan_price = 0;
      this.second_plan_price = 0;
      this.first_plan_price = event;
 
      this.final_plan_price = this.first_plan_price +this.second_plan_price;
     console.log(event);
   }
   onItemChangeSec(event:any)
   {
    this.second_plan_price = 0;
    this.first_plan_price = 0;
    this.second_plan_price = event;
    this.final_plan_price = this.first_plan_price +this.second_plan_price;
    console.log(event);
   }
  
}
