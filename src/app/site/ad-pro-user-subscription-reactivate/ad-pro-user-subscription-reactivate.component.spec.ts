import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdProUserSubscriptionReactivateComponent } from './ad-pro-user-subscription-reactivate.component';

describe('AdProUserSubscriptionReactivateComponent', () => {
  let component: AdProUserSubscriptionReactivateComponent;
  let fixture: ComponentFixture<AdProUserSubscriptionReactivateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdProUserSubscriptionReactivateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdProUserSubscriptionReactivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
