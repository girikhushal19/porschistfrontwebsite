import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProUserProfileComponent } from './view-pro-user-profile.component';

describe('ViewProUserProfileComponent', () => {
  let component: ViewProUserProfileComponent;
  let fixture: ComponentFixture<ViewProUserProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewProUserProfileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewProUserProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
