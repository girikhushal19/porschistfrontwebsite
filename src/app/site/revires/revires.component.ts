import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginauthenticationService } from '../../siteservice/loginauthentication.service';
@Component({
  selector: 'app-revires',
  templateUrl: './revires.component.html',
  styleUrls: ['./revires.component.css']
})
export class ReviresComponent implements OnInit {
  base_url:any;apiResponse:any;allsales:any;
  record:any;user_id:any;page_no:Number=0;webGetOwnSalesCountrecord:any;user_type: any;
  constructor( private loginAuthObj: LoginauthenticationService, private http: HttpClient)
  {
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.base_url = loginAuthObj.base_url_node;
    

    if( this.user_id === "" || this.user_id === null )
    {
      window.location.href = this.loginAuthObj.base_url;
    }


    this.record = [];


    this.user_type = this.loginAuthObj.userLoggedInType();
    
    this.getallSales(0);
    this.http.get(this.base_url+"api/getAllSellerReviewCount/"+this.user_id).subscribe(res=>{
      this.apiResponse = res;
      this.webGetOwnSalesCountrecord = this.apiResponse.record;
      const myNumber = this.apiResponse.record/10
      this.allsales = Array(Math.ceil(myNumber)).fill(0).map((m,n)=>n);
      
    })
  }

  ngOnInit(): void {
  }
  getallSales(page_num:any)
  {
    console.log("page_num ", page_num);
    this.http.get(this.base_url+"api/getAllSellerReview/"+this.user_id+"/"+page_num).subscribe(res=>{
      this.apiResponse = res;
      console.log("res ", this.apiResponse);
      if(this.apiResponse.error == false)
      {
        this.record = this.apiResponse.record;
      }
    });
  }
}
