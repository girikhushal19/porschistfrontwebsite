import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReviresComponent } from './revires.component';

describe('ReviresComponent', () => {
  let component: ReviresComponent;
  let fixture: ComponentFixture<ReviresComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReviresComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ReviresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
