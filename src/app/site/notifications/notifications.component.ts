import { Component, OnInit } from '@angular/core';
import { LoginauthenticationService } from '../../siteservice/loginauthentication.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
  base_url: any; base_url_node: any; base_url_node_only: any
  token: any; user_id: any; user_type: any
  webGetHistoryList: any; queryParam: any
  webGetHistoryListRes: any; history: any
  webHistoryDelete: any; webHistoryDeleteRes: any
  webHistoryActiveDeactiveEmail: any; webHistoryActiveDeactiveEmailRes: any

  constructor(private _http: HttpClient, private loginAuthObj: LoginauthenticationService) {
    this.base_url = this.loginAuthObj.base_url;
    this.base_url_node = this.loginAuthObj.base_url_node_admin;
    this.base_url_node_only = this.loginAuthObj.base_url_node;

    this.token = this.loginAuthObj.userLogin();
    this.user_id = this.loginAuthObj.userLoggedInId();
    this.user_type = this.loginAuthObj.userLoggedInType();

    this.webGetHistoryList = this.base_url_node + "webGetHistoryList";
    this.webHistoryDelete = this.base_url_node + "webHistoryDelete/";
    this.webHistoryActiveDeactiveEmail = this.base_url_node + "webHistoryActiveDeactiveEmail";
  }

  ngOnInit(): void {
    this.queryParam = { "user_id": this.user_id };
    this._http.post(this.webGetHistoryList, this.queryParam).subscribe(res => {
      this.webGetHistoryListRes = res

      if (this.webGetHistoryListRes.error == false) {
        this.history = this.webGetHistoryListRes.record

      }
    })

  }

  deleteHis(id: any) {

    const askAlert = confirm("Voulez-vous supprimer l'historique ?")
    if (askAlert) {
      this._http.get(this.webHistoryDelete + id).subscribe(res => {
        this.webHistoryDeleteRes = res

        if (this.webHistoryDeleteRes.error == false) {
          window.location.reload()
        }
      })
    }

  }

  activeDeactiveEmail(id: any) {
    console.log(id)
    const emailParams =
    {
      "id": id,
      "status": 1
    }

    this._http.post(this.webHistoryActiveDeactiveEmail, emailParams).subscribe(res => {
      this.webHistoryActiveDeactiveEmailRes = res
      if (this.webHistoryActiveDeactiveEmailRes.error == false) {

        setInterval(() => {
          window.location.reload();
        }, 2000);
      }
    })

  }

}
